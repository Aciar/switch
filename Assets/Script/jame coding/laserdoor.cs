﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserdoor : MonoBehaviour
{
    private NewPlayerScript player;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("player").GetComponent<NewPlayerScript>();
    }
    private void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(player.PlayerDead());
    }

}
