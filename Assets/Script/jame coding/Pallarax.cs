﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pallarax : MonoBehaviour
{
    private Rigidbody2D target;
    public float speed;
    private float initPos;

   
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("player").GetComponent<Rigidbody2D>();
        initPos = transform.localPosition.x;
        GameObject objectCopy = GameObject.Instantiate(this.gameObject);
        Destroy(objectCopy.GetComponent<Pallarax>());
        objectCopy.transform.SetParent(this.transform);
        objectCopy.transform.localPosition = new Vector3(getWidth(), 0, 0);
    }

    void FixedUpdate()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        float targetVelocity = target.velocity.x;
        this.transform.Translate(new Vector3(-speed * targetVelocity, 0, 0) * Time.deltaTime);
        float width = getWidth();
        if (targetVelocity > 0)
        {
            if (initPos - this.transform.localPosition.x > width)
            {
                this.transform.Translate(new Vector3(width, 0, 0));
            }
        }
        else
        {
            if (initPos - this.transform.localPosition.x < 0)
            {
                this.transform.Translate(new Vector3(-width, 0, 0));
            }
        }
    }

    float getWidth()
    {
        return this.GetComponent<SpriteRenderer>().bounds.size.x;
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (target != null)
        {
            target = GameObject.FindGameObjectWithTag("player").GetComponent<Rigidbody2D>();
        }
    }


}
