﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walking : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed;
    public float jumpforce;
    private bool isgrounded;
    public Transform groundcheck;
    public float checkRadius;
    public LayerMask whatisGround;
    private int extraJump;
    public int extraJumpValue;
    private Vector3 InitialScale;
    //Plai add
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        InitialScale = transform.localScale;

        //Plai add
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        isgrounded = Physics2D.OverlapCircle(groundcheck.position, checkRadius, whatisGround);

        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");
        if (x > 0.5f || x < -0.5f)
        {
            rb.velocity = new Vector2(x * speed * Time.deltaTime, rb.velocity.y);

        }
        if (x == 0)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            anim.SetBool("isWalking", false);
        }
        if (x < -0.5)
        {
            anim.SetBool("isWalking", true);
            Vector3 scale = transform.localScale;
            scale.x = -InitialScale.x;
            scale.y = InitialScale.y;
            transform.localScale = scale;
        }
        else if (x > 0.5)
        {
            anim.SetBool("isWalking", true);
            Vector3 scale = transform.localScale;
            scale.x = InitialScale.x;
            scale.y = InitialScale.y;
            transform.localScale = scale;
        }

    }
    private void Update()
    {
        if(isgrounded == true)
        {
            extraJump = extraJumpValue; 
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && extraJump > 0)
        {
            //anim.SetTrigger("isJump");
            rb.velocity = Vector2.up * jumpforce;
            extraJump--;
        }
        else if(Input.GetKeyDown(KeyCode.UpArrow) && extraJump == 0 && isgrounded == true)
        {
            anim.SetTrigger("isJump");
            rb.velocity = Vector2.up * jumpforce;
        }

        if (rb.position.y < -6f) {
            FindObjectOfType<GameManager>().EndGame();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    { 
        if (collision.gameObject.tag == "Win") {
            FindObjectOfType<GameManager>().Win();
        }
    }
}
