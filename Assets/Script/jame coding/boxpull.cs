﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxpull : MonoBehaviour
{
    public bool beingPushed;
    public bool isDeviceAttached = false;
    float xPos;

    public bool isOverlapping = false;
    bool isDeviceShown = false;

    public GameObject device;

    public GameObject followTrail;
    GameObject trail;
    bool isQuitting = false;

    //For pop-up
    public GameObject popup;
    public bool popupOn = false;

    // Start is called before the first frame update
    void Start()
    {
        device.SetActive(false);
        xPos = transform.position.x;
        popup = GameObject.FindGameObjectWithTag("popup");
        //popup = GameObject.Find("popup");
    }

    // Update is called once per frame
    void Update()
    {
        if (popup == null)
        {
            Start();
            //popup = GameObject.Find("popup");
        }
        if (beingPushed == false)
        {
            transform.position = new Vector3(xPos,transform.position.y);
        }
        else
        {
            xPos = transform.position.x;
        }

        popupSet();
        DisplayDevice();

    }

    
    void DisplayDevice() {
        if (isDeviceAttached && !isDeviceShown)
        {
            device.SetActive(true);
            isDeviceShown = true;
        }
        else if (!isDeviceAttached && isDeviceShown) {
            device.SetActive(false);
            isDeviceShown = false;
        }
    }

    void OnTriggerStay2D(Collider2D collider) // when enter and stay in the trigger area
    {

        if (collider.gameObject.CompareTag("Trail")) // player position overlapping with Trail
        {
            isOverlapping = true; // set overlapping true
        }
    }

    void OnTriggerExit2D(Collider2D collider) // when exit the trigger
    {
        if (collider.gameObject.CompareTag("Trail")) // when player exit the overlapping area 
        {
            isOverlapping = false; // set overlapping false
        }
    }

    /*void OnApplicationQuit()
    {
        isQuitting = true;
    }

    private void OnDisable() {
        print("in");
        if (!isQuitting && !NewPlayerScript.instance.Dying)
        {
            if (!isDeviceAttached || (isDeviceAttached && isOverlapping))
            {

                trail = Instantiate(followTrail) as GameObject; // instantiate trail object
                trail.transform.position = new Vector2(transform.position.x, transform.position.y);

            }
        }
    }


    private void OnEnable()
    {
        if (trail != null)
        {
            Destroy(trail);
        }
    }

    private void OnDestroy()
    {
        if (trail != null)
        {
            Destroy(trail);
        }
    }*/

    public void popupSet()
    {
        if (popupOn == true)
        {
            PopupTrue();
        }
        else if (popupOn == false)
        {
            PopupFalse();
        }
    }

    public void PopupTrue()
    {
        popup.SetActive(true);
    }

    public void PopupFalse()
    {
        popup.SetActive(false);
    }
}