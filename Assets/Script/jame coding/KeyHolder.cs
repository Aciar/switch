﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KeyHolder : MonoBehaviour
{
    public event EventHandler OnkeysChanged;
    private List<KeyCard.KeyType> keylist;
    private static KeyHolder One;

    private void Awake()
    {

        if (One == null)
        {
            One = this;
            DontDestroyOnLoad(One);
        }
        else
        {
            Destroy(gameObject);
        }
        keylist = new List<KeyCard.KeyType>();
    }
    public List<KeyCard.KeyType> GetKeyList()
    {
        return keylist;
    }
    public void AddKey(KeyCard.KeyType keyType)
    {
        Debug.Log("Add" + keyType);
        keylist.Add(keyType);
        OnkeysChanged?.Invoke(this, EventArgs.Empty);
    }
    public void RemoveKey(KeyCard.KeyType keyType)
    {
        keylist.Remove(keyType);
        OnkeysChanged?.Invoke(this, EventArgs.Empty);
    }
    public bool ContainKey(KeyCard.KeyType keyType)
    {
        return keylist.Contains(keyType);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        KeyCard key = collision.GetComponent<KeyCard>();
        if(key != null)
        {
            AddKey(key.GetKeyType());
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.PickUpCard);
            Destroy(key.gameObject);
        }
        KeyDoor keyDoor = collision.GetComponent<KeyDoor>();
        if(keyDoor != null)
        {
            if (ContainKey(keyDoor.GetKeyType()))
            {
                RemoveKey(keyDoor.GetKeyType());
                SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.OpenDoor);
                keyDoor.OpenDoor();
            }
        }
    }
}
