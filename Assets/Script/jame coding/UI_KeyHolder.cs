﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_KeyHolder : MonoBehaviour
{
    [SerializeField] private KeyHolder keyHolder;
    [SerializeField] private RectTransform container;
    [SerializeField] private RectTransform keyTemplate;
 

    private void Awake()
    {
        keyTemplate.gameObject.SetActive(false);
    }
    private void Start()
    {
        keyHolder = GameObject.FindGameObjectWithTag("player").GetComponent<KeyHolder>();
        keyHolder.OnkeysChanged += keyHolder_OnKeysChanged;
    }
    private void OnDestroy()
    {
        keyHolder.OnkeysChanged -= keyHolder_OnKeysChanged;
    }
    public void keyHolder_OnKeysChanged(object sender, System.EventArgs e)
    {
        UpdateVisual();
    }
    private void UpdateVisual()
    {
        // Clean up old keys      
        foreach (RectTransform child in container)
        {
            if (child == keyTemplate) continue;
            Destroy(child.gameObject);
        }
        // Instantiate current key list
        List<KeyCard.KeyType> keyList = keyHolder.GetKeyList();
        container.GetComponent<RectTransform>().anchoredPosition = new Vector2(-(keyList.Count - 1) * 80 / 2f, 0);

            for (int i = 0; i < keyList.Count; i++)
            {
                KeyCard.KeyType keyType = keyList[i];
                Transform keyTransform = Instantiate(keyTemplate, container);
                keyTransform.gameObject.SetActive(true);
                keyTransform.GetComponent<RectTransform>().anchoredPosition = new Vector2(80 * i, 0);
                Image keyImage = keyTransform.Find("Image").GetComponent<Image>();
                switch (keyType)
                {
                    default:
                    case KeyCard.KeyType.red: keyImage.color = Color.red; break;
                    case KeyCard.KeyType.green: keyImage.color = Color.green; break;
                    case KeyCard.KeyType.blue: keyImage.color = Color.blue; break;
                }
           
            }
    }
}
