﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class LoadingScene : MonoBehaviour
{
    public GameObject loadingScreenObj;
    public GameObject SettingUI;
    public GameObject MenuUI;
    public Slider slider;
    public Button button;
    public GameObject levelselect;
    public GameObject control;
    AsyncOperation async;
    int firstplay = 0;

    private void Start()
    {
        firstplay = PlayerPrefs.GetInt("savedFirstRun");
        if (firstplay == 0) 
        {
            firstplay = 1;
            PlayerPrefs.SetInt("savedFirstRun",1);
        }
    }

    public void LoadScreenButton(int LV)
    {
        StartCoroutine(loadingscreendelay(LV));

    }
    public void restart(int LV)
    {
        StartCoroutine(loadingscreendelay(LV));
    }
    public void LevelSelect(int LV)
    {
        levelselect.SetActive(false);
        StartCoroutine(loadingscreendelay(LV));
    }
    IEnumerator loadingscreendelay(int lvl)
    {
        loadingScreenObj.SetActive(true);
        async = SceneManager.LoadSceneAsync(lvl);
        async.allowSceneActivation = false;

        while (async.isDone == false)
        {
            slider.value = async.progress;
            if (async.progress == 0.9f)
            {
                slider.value = 1f;
                Destroy(GameObject.FindWithTag("BGMenu"));
                async.allowSceneActivation = true;

            }
            yield return null;

        }
    }

    public void toSetting()
    {
        MenuUI.SetActive(false);
        SettingUI.SetActive(true);
        
    }

    public void toMenu()
    {
        SettingsController.GetAll();
        SettingUI.SetActive(false);
        MenuUI.SetActive(true);
    }

    public void QuitGame()
    {
        SettingsController.GetAll();
        Destroy(GameObject.FindWithTag("VolumeSound"));
        Application.Quit();
    }
   public void gotoscene(int lvl)
    {
        //Destroy(GameObject.FindWithTag("Settings"));
        SceneManager.LoadScene(lvl);
    }
    public void controller()
    {
        control.SetActive(true);
    }
    public void uncontroller()
    {
        control.SetActive(false);
    }
}
