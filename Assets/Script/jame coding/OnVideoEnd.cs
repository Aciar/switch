﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class OnVideoEnd : MonoBehaviour
{
    private int nextSceneToLoad;
    public int index;
    public VideoPlayer VideoPlayer; // Drag & Drop the GameObject holding the VideoPlayer component
    void Start()
    {
        VideoPlayer.loopPointReached += LoadScene;
        nextSceneToLoad = SceneManager.GetActiveScene().buildIndex + index;
    }
    void LoadScene(VideoPlayer vp)
    {
        SceneManager.LoadScene(nextSceneToLoad, LoadSceneMode.Single);
    }
}
