﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCard : MonoBehaviour
{
    [SerializeField] private KeyType keytype;
   public enum KeyType
    {
        red,
        green,
        blue,
        red1,
        green1,
        blue1

    }

    public KeyType GetKeyType()
    {
        return keytype;
    }
}