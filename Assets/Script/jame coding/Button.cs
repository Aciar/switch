﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public GameObject door;
    private bool activated;
    private List<Collision2D> collidings = new List<Collision2D>();
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    protected bool CheckCollisionCollideType(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<playerpush>())
        {
            return true;
        }
        if (collision.gameObject.GetComponent<boxpull>())
        {
            return true;
        }
        return false;
    }

    protected bool CheckDeactivationCondition(Collision2D collision)
    {
        return collidings.Count<=0 && activated;
    }

    protected bool CheckActivationCondition(Collision2D collision)
    {
        
        return collidings.Count > 0 && !activated;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (CheckCollisionCollideType(collision))
        {
            collidings.Add(collision);
        }
        if (!CheckActivationCondition(collision))
        {
            return;
        }
        CameraShaker.instance.CamShake();
        door.SetActive(false);
        Debug.Log(activated);
       // this.transform.position += new Vector3(0.0f, -0.01f, 0.0f);
        activated = true;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (CheckCollisionCollideType(collision))
        {
            collidings.Remove(collision);
        }
        if (!CheckDeactivationCondition(collision))
        {
            return;
        }
        door.SetActive(true);
        //door.transform.position += new Vector3(0.0f, -5.0f, 0.0f);
       // this.transform.position += new Vector3(0.0f, 0.01f, 0.0f);
        activated = false;
        Debug.Log(activated);
        
    }


}
