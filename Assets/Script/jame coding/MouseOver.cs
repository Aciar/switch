﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Vector3 cachedScale;
    Image current;
    Image old;
    public Sprite New;
    Sprite currents;

    void Start()
    {

        cachedScale = transform.localScale;
        current = this.GetComponent<Image>();
        old = this.GetComponent<Image>();
        currents = old.sprite;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.MenuButton);
        transform.localScale = transform.localScale * 1.1f;
        current.sprite = New;
    }

    public void OnPointerExit(PointerEventData eventData)
    {

        transform.localScale = cachedScale;
        current.sprite = currents;
    }

    public void ResetSprite()
    {
        transform.localScale = cachedScale;
        current.sprite = currents;
    }

}

