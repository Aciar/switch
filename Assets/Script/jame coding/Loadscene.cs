﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loadscene : MonoBehaviour
{
    private GameMaster gm;
    private int nextSceneToLoad;
    private int currentscene;
    public int index;
    public Vector2 newV;
    
    // Start is called before the first frame update
    void Start()
    {
        
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        nextSceneToLoad = SceneManager.GetActiveScene().buildIndex + index;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (nextSceneToLoad == 8|| nextSceneToLoad == 14 || nextSceneToLoad == 16 || nextSceneToLoad == 17)
        {
            Debug.Log("Destroy");
            Destroy(GameObject.FindWithTag("player"));
            Destroy(GameObject.FindWithTag("GM"));
            Destroy(GameObject.FindWithTag("BGsound"));
        }
        gm.LastCheckPointPos = newV;
        gm.scene++;
        Debug.Log("scene" + gm.scene);
        /*if (gm.scene == 2)
        {
            DimensionSwitch.instance.enabled = false;
        }*/
        SceneManager.LoadScene(nextSceneToLoad, LoadSceneMode.Single);
        
        
    }
}
