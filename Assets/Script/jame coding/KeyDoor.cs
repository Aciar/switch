﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : MonoBehaviour
{
    [SerializeField] private KeyCard.KeyType keytype;

    public KeyCard.KeyType GetKeyType()
    {
        return keytype;
    }
    public void OpenDoor()
    {
        CameraShaker.instance.CamShake();
        gameObject.SetActive(false);
    }
}
