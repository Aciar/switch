﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerpush : MonoBehaviour
{

    public float distance = 1f;
    public LayerMask boxMask;
    GameObject box;
    private NewPlayerScript player;
    RaycastHit2D hit;


    void Start()
    {
        player = GetComponent<NewPlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        Physics2D.queriesStartInColliders = false;
        if (player.side == 1)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.right, transform.localScale.x, boxMask);
        }
        else if (player.side == -1)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.left, transform.localScale.x , boxMask);
        }

        if(hit.collider != null && hit.collider.gameObject.tag == "Pushable" && (!Input.GetKey(InputManager.instance.pushBox) && !Input.GetKey(InputManager.instance.attachDev)))
        {
            box = hit.collider.gameObject;
            box.GetComponent<boxpull>().popupOn = true;
        }
        else /*if(hit.collider != null && hit.collider.gameObject.tag == "Pushable")*/
        {
            //box = hit.collider.gameObject;
            //GameObject.FindGameObjectWithTag("Pushable").GetComponent<boxpull>().popupOn = false;
            GameObject.Find("Box").GetComponent<boxpull>().popupOn = false;
        }


        if (hit.collider != null && hit.collider.gameObject.tag == "Pushable" &&Input.GetKeyDown(InputManager.instance.pushBox) && player.isGrounded == true)
        {
            box = hit.collider.gameObject;
            //NewPlayerScript.isPushing = true;
            //Debug.Log(this.GetComponent<Rigidbody2D>());
            box.GetComponent<FixedJoint2D>().connectedBody = this.GetComponent<Rigidbody2D>();
            box.GetComponent<FixedJoint2D>().enabled = true;
            box.GetComponent<boxpull>().beingPushed = true;
            box.GetComponent<boxpull>().popupOn = false;
            //isPushing = true;
            NewPlayerScript.isCanPushPull = true;
        }
        else if (Input.GetKeyUp(InputManager.instance.pushBox) || Input.GetKeyDown(InputManager.instance.jump) || Input.GetKeyDown(InputManager.instance.diSwitch))
        {
            if(box == null)
            {
                return;
            }
            //NewPlayerScript.isPushing = false;
            box.GetComponent<boxpull>().beingPushed = false;
            box.GetComponent<FixedJoint2D>().enabled = false;
            //isPushing = false;
            NewPlayerScript.isCanPushPull = false;
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        //Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.right * transform.localScale.x * distance);
        
         Gizmos.DrawLine(transform.position, (Vector2)transform.position + Vector2.right * transform.localScale.x * distance);
         
       
    }
}
