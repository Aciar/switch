﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPos : MonoBehaviour
{
    private GameMaster gm;
    private Transform checkpoint;
    private string sceneName;
    private Scene m_Scene;
    // Start is called before the first frame update
    void Start()
    {
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        transform.position = gm.LastCheckPointPos;
    }

    // Update is called once per frame
    void Update()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
            if (gm != null)
            {
                 gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
                 Debug.Log(gm.LastCheckPointPos);
                 transform.position = gm.LastCheckPointPos;
            }
           
    }
}
