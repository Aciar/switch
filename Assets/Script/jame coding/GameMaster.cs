﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{

    /*public SoundAudioClip[] soundAudioClipArray;
    [System.Serializable]
    public class SoundAudioClip
    {
        public Sound_manager.Sound sound;
        public AudioClip audioClip;
    }*/

    private static GameMaster instance;
    public Vector2 LastCheckPointPos;
    public int scene;
    private DimensionSwitch dimen;
  
    private void Awake()
    {
        dimen = GameObject.FindGameObjectWithTag("player").GetComponent<DimensionSwitch>();
        Sound_manager.Initialize();
        SoundManagerRandom.Initialize();
        //Destroy(GameObject.FindWithTag("VolumeSound"));
        //VolumeManager.Initialize();

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
       
    }
}
