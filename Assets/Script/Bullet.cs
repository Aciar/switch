﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update

    float moveSpeed = 20f;

    public Rigidbody2D rb;

    GameObject target;
    Vector2 moveDirection;
    void Start()
    {
        
        rb = this.GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("player");
        moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
        rb.velocity = new Vector2(moveDirection.x, moveDirection.y);
        
        //rb.velocity = transform.right * moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(this.gameObject, 5f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Ontrigger");
        if (other.gameObject.tag == "player")
        {
            Debug.Log("Bullet hit Player");
            other.GetComponent<NewPlayerScript>().InvokeDead();
            Destroy(this.gameObject);
        }
        
    }
}
