﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    public Animator camAnim;
    public static CameraShaker instance;
    private void Awake()
    {
        if (instance != null) {
            Destroy(this);
        }
        instance = this;
    }
    public void CamShake() {
        camAnim.SetTrigger("Shake");
    }
        
        
}
