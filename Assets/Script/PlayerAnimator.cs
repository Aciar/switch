﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Running
        if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetBool("isRunning",true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }

        //Walking
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }

        //Pulling
        if(Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.X))
        {
            //this.transform.localScale = new Vector3(-1.5f, 1.5f, 1.0f);
            anim.SetBool("isPulling", true);
        }
        else
        {
            //this.transform.localScale = new Vector3(1.5f, 1.5f, 1.0f);
            anim.SetBool("isPulling", false);
        }

        //Pushing
        if (Input.GetKey(KeyCode.C) && Input.GetKey(KeyCode.V))
        {
            anim.SetBool("isPushing", true);
        }
        else
        {
            anim.SetBool("isPushing", false);
        }

        //Jump
        if(Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetTrigger("isJump");
        }
        
        //Dead
        if (Input.GetKeyDown(KeyCode.R))
        {
            anim.SetTrigger("isDead");
        }

        //Break
        if (Input.GetKeyDown(KeyCode.B))
        {
            anim.SetTrigger("isBreak");
        }
    }
}
