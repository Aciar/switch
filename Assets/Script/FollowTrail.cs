﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTrail : MonoBehaviour
{
    public GameObject followObj;
    GameObject pastObject, futureObject;
    public bool isEnemy = false;
    void Start()
    {
        //followObj = followObj.GetComponent<GameObject>();
        pastObject = LevelController.instance.pastObject;
        futureObject = LevelController.instance.futureObject;
    }

    private void OnEnable()
    {
        if (followObj != null)
        {
            if (isEnemy) {
                transform.localScale = followObj.transform.localScale;
            }
            transform.position = followObj.transform.position;
        }
    }

    private void Update()
    {
        if (followObj == null) { Destroy(this.gameObject); }

        if (followObj != null)
        {
            DiCheck();
        }
        
    }

    void DiCheck() {
        if (transform.parent == followObj.transform.parent)
        {
            if (transform.parent.gameObject == pastObject)
            {
                transform.parent = futureObject.transform;
            }
            else
            {
                transform.parent = pastObject.transform;
            }
        }
    }
}
