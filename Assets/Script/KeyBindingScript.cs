﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBindingScript : MonoBehaviour
{
    Transform menuPanel;
    Event keyEvent;
    Text buttonText;
    KeyCode newKey;
    bool waitingForKey;
    public Color defaultColor;
    public Color selectedColor;

    void Start()
    {
        menuPanel = transform.Find("Panel");
        menuPanel.gameObject.SetActive(false);
        waitingForKey = false;

        for (int i = 0; i < 6; i++) {
            if (menuPanel.GetChild(i).name == "leftKey") 
            {
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = InputManager.instance.left.ToString();
            }
            else if (menuPanel.GetChild(i).name == "rightKey")
            {
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = InputManager.instance.right.ToString();
            }
            else if (menuPanel.GetChild(i).name == "jumpKey")
            {
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = InputManager.instance.jump.ToString();
            }
            else if (menuPanel.GetChild(i).name == "diSwitchKey")
            {
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = InputManager.instance.diSwitch.ToString();
            }
            else if (menuPanel.GetChild(i).name == "attachDevKey")
            {
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = InputManager.instance.attachDev.ToString();
            }
            else if (menuPanel.GetChild(i).name == "pushBoxKey")
            {
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = InputManager.instance.pushBox.ToString();
            }
        }
    }

    
    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Escape)&& !menuPanel.gameObject.activeSelf ){
            menuPanel.gameObject.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && menuPanel.gameObject.activeSelf)
        {
            menuPanel.gameObject.SetActive(false);
        }*/
    }

    public void OpenPanel() {
        if (menuPanel.gameObject.activeSelf)
        {
            menuPanel.gameObject.SetActive(false);
        }
        else {
            menuPanel.gameObject.SetActive(true);
        }
    }

    /*public void ClosePanel()
    {
        menuPanel.gameObject.SetActive(false);
    }*/

    void OnGUI() {
        keyEvent = Event.current;

        if (keyEvent.isKey && waitingForKey) {
            newKey = keyEvent.keyCode;
            waitingForKey = false;
        }
    }

    public void StartAssignment(string keyName) {
        if (!waitingForKey) {
            StartCoroutine(AssignKey(keyName));
        }
        
    }

    public void SendText(Text text) {
        buttonText = text;
    }

    IEnumerator WaitForKey() {
        buttonText.color = selectedColor;
        while (!keyEvent.isKey)
           yield return null;
        
    }

    public IEnumerator AssignKey(string keyName) {
        waitingForKey = true;

        yield return WaitForKey();
        buttonText.color = defaultColor;
        switch (keyName) {

            case "left":
                InputManager.instance.left = newKey;
                buttonText.text = InputManager.instance.left.ToString();
                PlayerPrefs.SetString("leftKey", InputManager.instance.left.ToString());
                break;

            case "right":
                InputManager.instance.right = newKey;
                buttonText.text = InputManager.instance.right.ToString();
                PlayerPrefs.SetString("rightKey", InputManager.instance.right.ToString());
                break;

            case "jump":
                InputManager.instance.jump = newKey;
                buttonText.text = InputManager.instance.jump.ToString();
                PlayerPrefs.SetString("jumpKey", InputManager.instance.jump.ToString());
                break;

            case "diSwitch":
                InputManager.instance.diSwitch = newKey;
                buttonText.text = InputManager.instance.diSwitch.ToString();
                PlayerPrefs.SetString("diSwitchKey", InputManager.instance.diSwitch.ToString());
                break;

            case "attachDev":
                InputManager.instance.attachDev = newKey;
                buttonText.text = InputManager.instance.attachDev.ToString();
                PlayerPrefs.SetString("attachDevKey", InputManager.instance.attachDev.ToString());
                break;

            case "pushBox":
                InputManager.instance.pushBox = newKey;
                buttonText.text = InputManager.instance.pushBox.ToString();
                PlayerPrefs.SetString("pushBoxKey", InputManager.instance.pushBox.ToString());
                break;
        }
        yield return null;
    }

    public void ResetDefault()
    {
        for (int i = 0; i < 6; i++)
        {
            if (menuPanel.GetChild(i).name == "leftKey")
            {
                InputManager.instance.left = KeyCode.LeftArrow;
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = "LeftArrow";
                PlayerPrefs.SetString("leftKey", "LeftArrow");
            }
            else if (menuPanel.GetChild(i).name == "rightKey")
            {
                InputManager.instance.right = KeyCode.RightArrow;
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = "RightArrow";
                PlayerPrefs.SetString("rightKey", "RightArrow");
            }
            else if (menuPanel.GetChild(i).name == "jumpKey")
            {
                InputManager.instance.jump = KeyCode.Z;
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = "Z";
                PlayerPrefs.SetString("jumpKey", "Z");
            }
            else if (menuPanel.GetChild(i).name == "diSwitchKey")
            {
                InputManager.instance.diSwitch = KeyCode.C;
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = "C";
                PlayerPrefs.SetString("diSwitchKey", "C");
            }
            else if (menuPanel.GetChild(i).name == "attachDevKey")
            {
                InputManager.instance.attachDev = KeyCode.X;
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = "X";
                PlayerPrefs.SetString("attachDevKey", "X");
            }
            else if (menuPanel.GetChild(i).name == "pushBoxKey")
            {
                InputManager.instance.pushBox = KeyCode.Space;
                menuPanel.GetChild(i).GetComponentInChildren<Text>().text = "Space";
                PlayerPrefs.SetString("pushBoxKey", "Space");
            }
        }
    }
}
