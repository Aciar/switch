﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Controller2D))] // automatically add Controller2D Script
public class PlayerScript : MonoBehaviour
{
	Vector2 input; // value of the input

	public float moveSpeed = 6; // player speed
	public float timeToMaxGround = .1f; // time to get max speed on ground
	public float timeToStopGround = .5f; // time to stop from max speed on ground
	public float timeToMaxAir = .2f; // time to get max speed on air
	public float timeToStopAir = .7f; // time to stop from max speed on air
    public playerpush playerpush;
    public float jumpHeight = 4; // height of the jump
	public float timeToJumpApex = .4f; // time to reach max height
	public float timeToReachGround = .27f; // time from max height to reach ground
    //Plai adding
    public float maxValueToWalk;

	float upGravity; // gravity when player is going up.
	float downGravity; // gravity when player is going down.
	float jumpVelocity; // inital force of the jump.

	Vector3 velocity; // object to store x & y axis of player velocity
	float velocityXSmoothing; // object for actual current velocity in smoothDamp

	//DimensionSwitch dimensionSwitch; // object for DimensionSwitch reference
	bool isSwitchTrigger; // state of dimension
	bool isOverLapping; // Check if player collide with another dimension object

	Controller2D controller; // object for accessing Controller2D

    Animator anim; //Animation
    private Vector3 InitialScale; //From Jame's script

	int isRevert = 1;
	float additionGravity = 0;

    void Start()
	{
		controller = GetComponent<Controller2D>();  // get reference from Controller2D

		upGravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2); // equation to find gravity when going up
		downGravity = -(2 * jumpHeight) / Mathf.Pow(timeToReachGround, 2); // equation to find gravity when going down
		jumpVelocity = Mathf.Abs(upGravity) * timeToJumpApex;   // equation to find jumpVelocity

		//dimensionSwitch = DimensionSwitch.instance; // get reference from DimensionSwitch from any different object in the scene
		isSwitchTrigger = false; // set state to past(default)
		isOverLapping = false; // set overLapping default;

        anim = GetComponent<Animator>();//Animation adding
        InitialScale = transform.localScale; //from Jame's script
    }

	void Update()
	{
		VelocityReset(); // reset veloctiy when hit something in that axis

		input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));  // get input from the player

		//HorizontalControl(); // manage the horizontal movement

		VerticalControl(); // manage the vertical movement

        HorizontalControl(); // manage the horizontal movement

        controller.Move(velocity * Time.deltaTime); // send to function for movement

        SwitchCheck(); // check if the Switch key is pressed



	}

	void VelocityReset() // reset veloctiy when hit something in that axis
	{
		if (controller.collisions.above || controller.collisions.below) // check if detect collision at above or below
		{
			velocity.y = 0;  // reset y-axis velocity 
		}
		if (controller.collisions.left || controller.collisions.right) // check if detect collision at left or right
		{
			velocity.x = 0;  // reset x-axis velocity 
		}
	}

	void HorizontalControl() // manage the horizontal movement
	{
		float targetVelocityX = input.x * moveSpeed * isRevert; // set the speed according to move 

		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)) // player hold the movement key the same way as the velocity
		{
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? timeToMaxGround : timeToMaxAir); // imply the SmoothDamp to x axis when acceleration
		}
		else
		{   // player let go of the key
            velocity.x = Mathf.SmoothDamp(velocity.x, 0, ref velocityXSmoothing, (controller.collisions.below) ? timeToStopGround : timeToStopAir); // imply the SmoothDamp to x axis when deceleration
        }

        //Only for turn left and right
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            Vector3 scale = transform.localScale;
            scale.x = -InitialScale.x;
            scale.y = InitialScale.y;
            transform.localScale = scale;
        }
        else if(Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 scale = transform.localScale;
            scale.x = InitialScale.x;
            scale.y = InitialScale.y;
            transform.localScale = scale;
        }

		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)) // if movemont key is pressed
		{
            anim.SetBool("isWalking", true);
			if (isRevert == 1)
			{
				if (Mathf.Sign(Input.GetAxisRaw("Horizontal")) != Mathf.Sign(velocity.x) || (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.RightArrow)))
				{ // hold the key opposite to the velocity or hold both keys
					velocity.x = Mathf.SmoothDamp(velocity.x, 0, ref velocityXSmoothing, (controller.collisions.below) ? timeToStopGround : timeToStopAir); // imply the SmoothDamp to x axis when deceleration
                    
                }
                if (Mathf.Abs(velocity.x) > maxValueToWalk)
                {
                    anim.SetBool("isWalking", false);
                    anim.SetBool("isRunning", true);
                    //anim.SetBool("isShouldBreak", true);
                }
                else
                {
                    //anim.SetBool("isShouldBreak", false);
                }
                anim.SetBool("isShouldBreak", true);
            }
        }
        else
        {
            if (controller.collisions.below)
            {
                //anim.SetBool("isJumping", false);
                anim.SetBool("isShouldBreak", true);
            }
            else
            {
                //anim.SetBool("isJumping", true);
                anim.SetBool("isShouldBreak", false);
            }

            if ((anim.GetBool("isWalking") == true || anim.GetBool("isRunning") == true) && (anim.GetBool("isShouldBreak") == true && anim.GetBool("isjumping") == false))
                anim.SetTrigger("isBreak");
            anim.SetBool("isWalking", false);
            anim.SetBool("isRunning",false);
            
        }
	}
	void VerticalControl() // manage the vertical movement
	{
		if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)) && controller.collisions.below) // check key input
		{
            velocity.y = jumpVelocity; // jump according to junpVelocity
		}

		if (velocity.y >= 0) // if player moving upward
		{
			velocity.y += (upGravity - additionGravity) * Time.deltaTime; // velocity y effect by the gravity whlie moving up
		}
		else if (velocity.y < 0) // if player moving downward
		{
			velocity.y += (downGravity - additionGravity) * Time.deltaTime; // velocity y effect by the gravity whlie moving down
		}
        
    }

	public void AxisSwitch(){

		isRevert = -1;
	}

	public void GravityPull(float add) {
		additionGravity = add;
	}

	void OnTriggerEnter2D(Collider2D collider) // when enter the trigger
	{
		
		if (collider.gameObject.CompareTag("Trail")) // player position overlapping with Trail
		{
			isOverLapping = true; // set overlapping true
		}
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag("Trail")) // when player exit the overlapping area 
		{
			isOverLapping = false; // set overlapping false
		}
	}
	void SwitchCheck() // check if the Switch key is pressed
	{
		if (Input.GetKeyDown(KeyCode.X) && !isOverLapping) // check key and overlapping condition
		{
			isSwitchTrigger = !isSwitchTrigger; // set state to it's opposite
			//dimensionSwitch.Switch(isSwitchTrigger); // send the condition to update the level render
			//controller.UpdateLayerMask(isSwitchTrigger); // // send the condition to update the level collider

		}
	} 
}