﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderOut : MonoBehaviour
{
    public GameObject ThunderCall;
    public float projectileSpeed = 10f;
    private Rigidbody2D rb;
    private Vector2 screenBounds;
    void Start()
    {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        rb = this.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -projectileSpeed);
    }


    void Update()
    {
        if (transform.position.y < -screenBounds.y * 2)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(ThunderCall);
    }

}
