﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DimensionSwitch : MonoBehaviour
{
    // public static DimensionSwitch instance; // for being called outside the GameObject
    private GameMaster gm;
    Renderer pastBase;
    GameObject pastObject;

    Renderer futureBase;
    GameObject futureObject;

    bool isSceneLoaded = false;
    public static DimensionSwitch instance;

    public bool isSwitchTrigger = false; // state of dimension
    bool isOverLapping = false; // Check if player collide with another dimension object
    public bool isPlayerDie;

    public static bool isEnemy;
    public static bool isMonster;
    public static bool isGuard;
    public static bool isTurret;
    public GameObject monfov;       //Simple fixing in Monster have more than 1 so should using tag to pass it
    //EnemyShortState[] MonScipt;
    public EnemyList enemylist;
    public bool isSwitching = false;


    public GameObject ScreenEffect;
    GameObject ScrEff;
    public GameObject playerEffect;
    GameObject playerEff;
    GameObject cam;
    public float SwitchDelay = .4f;
    private void Awake()
    {
        if (instance == null)
            Destroy(instance);

        instance = this; // create a pointer and point to itself 

    }

    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
        pastBase = GameObject.FindGameObjectWithTag("Past").GetComponent<Renderer>(); // Need to tag the object "Past" too
        pastObject = GameObject.Find("PastObject");
        futureBase = GameObject.FindGameObjectWithTag("Future").GetComponent<Renderer>(); // Need to tag the object "Future" too
        futureObject = GameObject.Find("FutureObject");
        ResetDimension();
        SceneManager.sceneLoaded += OnSceneLoaded;
        cam = GameObject.FindWithTag("MainCamera");
        enemylist = GameObject.FindGameObjectWithTag("Enemylist").GetComponent<EnemyList>();
    }

    private void Update()
    {
        isPlayerDie = GameObject.FindGameObjectWithTag("player").GetComponent<NewPlayerScript>().Dying;
        if (DimensionSwitch.isEnemy == true)
        {
            enemylist = GameObject.FindGameObjectWithTag("Enemylist").GetComponent<EnemyList>();
        }
        SwitchCheck(); // check the dimensionSwitch

        if (isSceneLoaded) {
            //Debug.Log("ChangeScene");
            ResetDimension();
            isSceneLoaded = false;
        }
    }

    void SwitchCheck() // check if the Switch key is pressed
    {
        if (Input.GetKeyDown(InputManager.instance.diSwitch) && !isOverLapping && isSwitching == false && isPlayerDie == false) // check key and overlapping condition
        {
            StartCoroutine(SwitchingStr());
        }
    }

    IEnumerator SwitchingStr() {
        isSwitching = true;
        //print("inSwitching");
        playerEff = Instantiate(playerEffect) as GameObject;
        playerEff.transform.position = new Vector3(transform.position.x, transform.position.y, playerEff.transform.position.z);
        ScrEff = Instantiate(ScreenEffect) as GameObject;
        ScrEff.transform.parent = cam.transform;
        ScrEff.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, ScrEff.transform.position.z);

        isSwitchTrigger = !isSwitchTrigger; // set state to it's opposite
        Switch(isSwitchTrigger); // send the condition to update the level 
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.DiSwitch);
        BoxTransferDevice.instance.BoxTransferCheck(isSwitchTrigger);
        //print("Boxtransfer.Instance : " + BoxTransferDevice.instance);
        /*if (BoxTransferDevice.instance.isDeviceAvailable == false) {
            while (BoxTransferDevice.instance.isTransfered == false) { 
                
            }
        }*/
        yield return new WaitForSeconds(SwitchDelay);
        Destroy(playerEff);
        Destroy(ScrEff);
        isSwitching = false;
        yield return null;
    }

    public void Switch(bool isSwitchTrigger){
        if (isSwitchTrigger == true)
        {
            if (isEnemy)
            {
                enemylist.setTrueMonFOV();
                enemylist.setFalseGuardFOV();
                enemylist.setFalseTurretFOV();
            }
            BGSound.isPast = false;
            BGSound.isFuture = true;
            pastBase.enabled = false;
            pastObject.SetActive(false);
            futureBase.enabled = true;
            futureObject.SetActive(true);
        }
        else {
            if (isEnemy)
            {
                enemylist.setTrueGuardFOV();
                enemylist.setFalseMonFOV();
                enemylist.setTrueTurretFOV();
            }
            BGSound.isPast = true;
            BGSound.isFuture = false;
            pastBase.enabled = true;
            pastObject.SetActive(true);
            futureBase.enabled = false;
            futureObject.SetActive(false);
        }
    }

    void OnTriggerStay2D(Collider2D collider) // when enter and stay in the trigger area
    {

        if (collider.gameObject.CompareTag("Trail")) // player position overlapping with Trail
        {
            isOverLapping = true; // set overlapping true
        }
    }

    void OnTriggerExit2D(Collider2D collider) // when exit the trigger
    {
        if (collider.gameObject.CompareTag("Trail")) // when player exit the overlapping area 
        {
            isOverLapping = false; // set overlapping false
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (GameObject.Find("Disaster") != null) { }
        else
        {
            isSwitchTrigger = false;
            pastBase = GameObject.FindGameObjectWithTag("Past").GetComponent<Renderer>(); // Need to tag the object "Past" too
            pastObject = LevelController.instance.pastObject;
            futureBase = GameObject.FindGameObjectWithTag("Future").GetComponent<Renderer>(); // Need to tag the object "Future" too
            futureObject = LevelController.instance.futureObject;
            cam = GameObject.FindWithTag("MainCamera");
        }

        monfov = GameObject.FindGameObjectWithTag("MonFOV");


        isSceneLoaded = true;
    }

    private void ResetDimension()
    {
        isOverLapping = false;
        isSwitchTrigger = false;
        pastBase.enabled = true;
        pastObject.SetActive(true);
        futureBase.enabled = false;
        futureObject.SetActive(false);
        
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}