﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sawblade : MonoBehaviour
{
    public float speedRotate;
    private NewPlayerScript player;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("player").GetComponent<NewPlayerScript>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * speedRotate * Time.deltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(player.PlayerDead());
    }
}
