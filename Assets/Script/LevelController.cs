﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public static LevelController instance;
    public GameObject pastObject;
    public GameObject futureObject;
    GameObject player;

    private void Awake()
    {
        if (instance != null) {
            Destroy(this);
        }

        instance = this;
        Destroy(GameObject.FindWithTag("BGsound2"));
        Destroy(GameObject.FindWithTag("Settings"));
        //Destroy(GameObject.FindWithTag("VolumeSound"));
        //VolumeManager.FindSlider();
        player = GameObject.FindGameObjectWithTag("player");

        if (GameObject.FindGameObjectWithTag("PastObject") != null && GameObject.FindGameObjectWithTag("FutureObject") != null)
        {
            player.GetComponent<DimensionSwitch>().enabled = true;
            BGSound.isDimensionScene = true;
        }
        else
        {
            player.GetComponent<DimensionSwitch>().enabled = false;
            BGSound.isDimensionScene = false;
        }

        if(GameObject.FindGameObjectWithTag("Monster") == null && GameObject.FindGameObjectWithTag("Guard") == null && GameObject.FindGameObjectWithTag("Turret") == null)
        {
            Destroy(GameObject.FindWithTag("Enemylist"));
            DimensionSwitch.isEnemy = false;
        }
        else
        {
            DimensionSwitch.isEnemy = true;
        }

        /*if (GameObject.FindGameObjectWithTag("Enemy") != null)
        {
            DimensionSwitch.isEnemy = true;
        }
        else
        {
            DimensionSwitch.isEnemy = false;
        }

        if (GameObject.FindGameObjectWithTag("Monster") != null)
        {
            DimensionSwitch.isMonster = true;
        }
        else
        {
            DimensionSwitch.isMonster = false;
        }

        if (GameObject.FindGameObjectWithTag("Guard") != null)
        {
            DimensionSwitch.isGuard = true;
        }
        else
        {
            DimensionSwitch.isGuard = false;
        }

        if (GameObject.FindGameObjectWithTag("Turret") != null)
        {
            DimensionSwitch.isTurret = true;
        }
        else
        {
            DimensionSwitch.isTurret = false;
        }*/
    }
}
