﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraControl : MonoBehaviour
{
    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    GameObject cam;
    GameObject player;
    Transform target;
    Vector3 velocity;
    float setVal;
    Vector3 desiredPosition;

    [Space]
    public float minXPos;
    public float maxXPos;


    private void Awake()
    {
        cam = this.gameObject;

        player = GameObject.FindWithTag("player");
        if (player != null)
        {
            target = player.transform;
            setVal = target.position.x;
        }
        setVal = Mathf.Clamp(target.position.x, minXPos, maxXPos);
        transform.position = new Vector3(setVal, 0, cam.transform.position.z) + offset;
    }

    private void FixedUpdate()
    {
        desiredPosition = new Vector3(target.position.x, 0, cam.transform.position.z) + offset;

        desiredPosition.x = Mathf.Clamp(target.position.x, minXPos, maxXPos);

        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        /*Debug.Log("Level Loaded");
        Debug.Log(scene.name);
        Debug.Log(mode);*/
        transform.position = new Vector3(desiredPosition.x, desiredPosition.y, cam.transform.position.z);
    }
}