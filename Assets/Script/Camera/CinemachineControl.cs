﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineControl : MonoBehaviour
{
    public GameObject Player;
    public Transform FollowTarget;
    private CinemachineVirtualCamera vcam;

   
    void Start()
    {
        vcam = GetComponent<CinemachineVirtualCamera>();

        Player = GameObject.FindWithTag("player");
        if (Player != null)
        {
            FollowTarget = Player.transform;
            vcam.LookAt = FollowTarget;
            vcam.Follow = FollowTarget;

        }
        else
        {
            Debug.Log("Can't find");
        }
    }

    private void Update()
    {
        //vcam.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }
}
