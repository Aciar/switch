﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseUIController : MonoBehaviour
{
    private static PauseUIController instance;
    public GameObject PauseMenu;
    public GameObject SettingUI;
    //public GameObject Logo;
    //public GameObject bg;
    public GameObject FilterConfig;
    public GameObject config;
    public bool isPause = false;

    public static PauseUIController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new PauseUIController();
            }

            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.transform.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
        //SettingUI = GameObject.FindGameObjectWithTag("Settings");
        if (SettingUI == null)
        {
            Debug.Log("SettingUI : " + SettingUI);
            SettingUI = GameObject.FindGameObjectWithTag("Settings2");
            
            SettingUI.SetActive(true);
        }
       /*if(config == null)
        {
            config = GameObject.FindGameObjectWithTag("config");
            config.SetActive(false);
        }*/
            
            
           
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SettingUI == null)
        {
            Debug.Log("SettingUI : "+ SettingUI);
            SettingUI = GameObject.FindGameObjectWithTag("Settings2");
            SettingUI.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Escape) && isPause == false)
        {
            PauseMenu.SetActive(true);
            //Logo.SetActive(true);
            //bg.SetActive(true);
            Time.timeScale = 0f;
            isPause = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isPause == true)
        {
            PauseMenu.SetActive(false);
            //Logo.SetActive(false);
            //bg.SetActive(false);
            SettingUI.SetActive(false);
            config.SetActive(false);
            FilterConfig.SetActive(false);
            Time.timeScale = 1f;
            isPause = false;
        }
    }
    public void toScene()
    {
        PauseMenu.SetActive(false);
        //Logo.SetActive(false);
        //bg.SetActive(false);
        Time.timeScale = 1f;
        isPause = false;
    }

    public void toSetting()
    {
        PauseMenu.SetActive(false);
        //Logo.SetActive(false);
        SettingUI.SetActive(true);
        isPause = true;
    }
    public void toConfig()
    {
        PauseMenu.SetActive(false);
        //Logo.SetActive(false);
        config.SetActive(true);
        FilterConfig.SetActive(true);
        isPause = true;
    }

    public void toPauseMenu()
    {
        SettingsController.GetAll();
        SettingUI.SetActive(false);
        PauseMenu.SetActive(true);
        //Logo.SetActive(true);
    }
    public void toPauseMenu2()
    {
        config.SetActive(false);
        FilterConfig.SetActive(false);
        PauseMenu.SetActive(true);
        //Logo.SetActive(true);
    }
    public void backtomainmenu()
    {
        SettingsController.GetAll();
        Time.timeScale = 1f;
        Destroy(GameObject.FindWithTag("GM"));
        Destroy(GameObject.FindWithTag("player"));
        Destroy(GameObject.FindWithTag("BGsound"));
        Destroy(this.gameObject);
        SceneManager.LoadScene("StartScreen");
    }

    public void toRestart()
    {
        Destroy(GameObject.FindWithTag("player"));
        Destroy(GameObject.FindWithTag("GM"));
        config.SetActive(false);
        FilterConfig.SetActive(false);
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
        PauseMenu.SetActive(false);
        //Logo.SetActive(false);
        //bg.SetActive(false);
        Time.timeScale = 1f;
        isPause = false;
    }

    public void toMainMenu()
    {
        //Application.LoadLevel(0);
        SceneManager.LoadScene("StartScreen");
    }

    public void QuitGame()
    {
        Destroy(GameObject.FindWithTag("VolumeSound"));
        Application.Quit();
    }

}
