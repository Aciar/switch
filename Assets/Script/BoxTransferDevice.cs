﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BoxTransferDevice : MonoBehaviour
{
    public bool isDeviceAvailable = true;
    public float distance = 1f;
    public LayerMask boxMask;
    public GameObject box;
    public GameObject attachedBox;
    private NewPlayerScript player;
    RaycastHit2D hit;
    GameObject pastObject, futureObject;

    public static BoxTransferDevice instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        instance = this;
    }

    void Start()
    {
        player = GetComponent<NewPlayerScript>();
        pastObject = LevelController.instance.pastObject;
        futureObject = LevelController.instance.futureObject;
    }

    void Update()
    {
        DeviceCheck();
        //BoxTransferCheck();
    }

    void DeviceCheck()
    {
        Physics2D.queriesStartInColliders = false;
        if (player.side == 1)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.right, transform.localScale.x, boxMask);
            //Debug.DrawLine(transform.position, transform.position + Vector3.right,Color.green ,transform.localScale.x);
        }
        else if (player.side == -1)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.left, transform.localScale.x, boxMask);
            //Debug.DrawLine(transform.position, transform.position + Vector3.left, Color.green, transform.localScale.x);
        }


        if (hit.collider != null && hit.collider.gameObject.tag == "Pushable" && Input.GetKeyDown(InputManager.instance.attachDev) && player.isGrounded == true)
        {
            
            box = hit.collider.gameObject;
            if (isDeviceAvailable)
            {
                isDeviceAvailable = false;
                box.GetComponent<boxpull>().isDeviceAttached = true;
                attachedBox = box;
            }
            else if (!isDeviceAvailable && box.GetComponent<boxpull>().isDeviceAttached == true)
            {
                box.GetComponent<boxpull>().isDeviceAttached = false;
                isDeviceAvailable = true;
            }
        }
    }

    public void BoxTransferCheck(bool dimensionCheck)
    {
        /*if (attachedBox != null) {
            print(isDeviceAvailable + " " + DimensionSwitch.instance.isSwitching + " " + attachedBox.GetComponent<boxpull>().isOverlapping);
        }*/

        if (!isDeviceAvailable && attachedBox.GetComponent<boxpull>().isOverlapping == false)
        {
            //Debug.Log("inTransfer");
            //Debug.Log(DimensionSwitch.instance.isSwitchTrigger);
            if (dimensionCheck)
            {
                attachedBox.gameObject.transform.parent = futureObject.transform;
                if (futureObject.transform.parent != null)
                {
                    Debug.Log("box's parent: " + attachedBox.gameObject.transform.parent.parent.name + "future");
                }
            }
            else
            {
                attachedBox.gameObject.transform.parent = pastObject.transform;
                if (pastObject.transform.parent != null)
                {
                    Debug.Log("box's parent: " + attachedBox.gameObject.transform.parent.parent.name + "past");
                }
            }
        }
        
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        /*Debug.Log("Level Loaded");
        Debug.Log(scene.name);
        Debug.Log(mode);*/
        isDeviceAvailable = true;
        box = attachedBox = null;
        pastObject = LevelController.instance.pastObject;
        futureObject = LevelController.instance.futureObject;

        instance = this;
    }

   

}