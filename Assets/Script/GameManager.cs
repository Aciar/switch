﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class GameManager : MonoBehaviour {

    bool gameHasEnded = false;
    public float restartDelay = 1.0f;
    public Text log;

    public void EndGame() {
        if (gameHasEnded == false) {
            gameHasEnded = true;
            log.text = "Retry";
            Invoke("Restart", restartDelay);
        }
        
    }
    void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Win() {
        FindObjectOfType<Walking>().enabled = false;
        log.text = "Win";
        Debug.Log("Win");
    }
}
