﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Collision coll;         //Collision for checking onGround isPushing isPulling
    [HideInInspector]
    public Rigidbody2D rb;
    private AnimationScript anim;   //AnimationScript for control animation

    [Space]
    [Header("Stats")]
    public float speed = 10;
    public float jumpForce = 50;

    [Space]
    [Header("Booleans")]
    public bool canMove = true;
    public bool groundTouch;
    public bool Pushing;
    public bool Pulling;
    public bool gonnaBreak;

    public int side = 1;


    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<Collision>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<AnimationScript>();

    }

    // Update is called once per frame
    void Update()
    {
        canMove = true;
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector2 dir = new Vector2(x, y);

        Walk(dir);

        if(Mathf.Abs(x) > 0.95)
        {
            gonnaBreak = true;
            //Sound_manager.PlaySound(Sound_manager.Sound.Break,transform.position);
            //SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Break, transform.position);
        }
        else
        {
            gonnaBreak = false;
        }

        
        if((x > 0.1 || x < -0.1) && (coll.onGround == true && rb.velocity.y == 0))
        {
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Walk, transform.position);
        }

        anim.SetHorizontalMovement(x, y, rb.velocity.y);

        if(gonnaBreak == true && (x < 0.1 || x > -0.1))
        {
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Break);
        }

        if(Input.GetButtonDown("Jump"))
        {
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Jump);
        }

        if (Input.GetButtonDown("Jump"))
        {
            //Sound_manager.PlaySound(Sound_manager.Sound.Jump, transform.position);
            //SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Jump, transform.position);
            anim.SetTrigger("Jump");

            if (coll.onGround) 
                Jump(Vector2.up, false);

        }

        if (coll.onGround && !groundTouch)
        {
            GroundTouch();
            groundTouch = true;
        }

        if (!coll.onGround && groundTouch)
        {
            groundTouch = false;
        }

        if (x > 0 && (!coll.boxLeftside && !coll.boxRightside) && (!Pushing && !Pulling) && !gonnaBreak)
        {
            side = 1;
            anim.Flip(side);
        }
        if (x < 0 && (!coll.boxLeftside && !coll.boxRightside) && (!Pushing && !Pulling) && !gonnaBreak)
        {
            side = -1;
            anim.Flip(side);
        }

        if(coll.boxLeftside && side == 1 && Input.GetKey(KeyCode.C))
        {
            if(x > 0.1)
            {
                Debug.Log("Pushing");
                Pushing = true;
                Pulling = false;
            }
            else if(x < -0.1)
            {
                Debug.Log("Pulling");
                Pushing = false;
                Pulling = true;
            }
        }
        else if (coll.boxRightside && side == -1 && Input.GetKey(KeyCode.C))
        {
            if (x > 0.1)
            {
                Debug.Log("Pulling");
                Pushing = false;
                Pulling = true;
            }
            else if (x < -0.1)
            {
                Debug.Log("Pushing");
                Pushing = true;
                Pulling = false;
            }
        }
        else
        {
            Pushing = false;
            Pulling = false;
        }

    }

    private void Walk(Vector2 dir)
    {
        if (!canMove)
            return;

        //Sound_manager.PlaySound(Sound_manager.Sound.Walk, transform.position);
        //SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Walk, transform.position);
        rb.velocity = new Vector2(dir.x * speed, rb.velocity.y);
        
        
    }

    private void Jump(Vector2 dir, bool wall)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;
    }

    void GroundTouch()
    {
        side = anim.sr.flipX ? -1 : 1;

    }

}
