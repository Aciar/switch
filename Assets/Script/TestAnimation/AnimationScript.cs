﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScript : MonoBehaviour
{
    private Animator anim;
    private NewPlayerScript player;
    private playerpush pushpull;
    private Collision coll;
    [HideInInspector]
    public SpriteRenderer sr;
    public bool isRun = false;
    public bool isJump = false;
    public bool gonnaBreak = false;
    public bool isDoneBrake = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        coll = GetComponentInParent<Collision>();
        player = GetComponentInParent<NewPlayerScript>();
        sr = GetComponent<SpriteRenderer>();
        pushpull = GetComponentInParent<playerpush>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("isDie", player.Dying);
        if (anim.GetBool("isDie") == false)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsTag("1"))      //run
            {
                //check that run animation is run, so let brake true
                isRun = true;
            }
            if (anim.GetCurrentAnimatorStateInfo(0).IsTag("3"))      //jump
            {
                //check that jump animation is run, so let brake false
                isJump = true;
            }
            if (anim.GetFloat("HorizontalAxis") == 0.0f)
            {
                isJump = false;
            }

            if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("1") && isRun == true && (anim.GetFloat("VerticalVelocity") < 0.1 && anim.GetFloat("VerticalVelocity") > -0.1) && isJump == false && !anim.GetCurrentAnimatorStateInfo(0).IsName("DeadColor"))
            {
                //check that run animation is done and going to do brake animation
                gonnaBreak = true;
                anim.SetTrigger("Brake");
                isRun = false;
                isJump = false;
            }

            if (gonnaBreak == true)
            {
                NewPlayerScript.gonnaBreak = true;
            }
            else if (gonnaBreak == false)
            {
                NewPlayerScript.gonnaBreak = false;
            }

            if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("1") || !anim.GetCurrentAnimatorStateInfo(0).IsTag("0"))
            {
                gonnaBreak = false;
            }

            //Debug.Log("on ground : " + coll.onGround);
            anim.SetBool("onGround", player.isGrounded);
            //anim.SetBool("onGround", coll.onGround);
            anim.SetBool("isPushing", player.Pushing);
            anim.SetBool("isPulling", player.Pulling);
            anim.SetBool("gonnaBreak", gonnaBreak);
        }
    }

    public void SetHorizontalMovement(float x, float y, float yVel)
    {
        anim.SetFloat("HorizontalAxis", x);
        anim.SetFloat("VerticalAxis", y);
        anim.SetFloat("VerticalVelocity", yVel);
    }

    public void SetTrigger(string trigger)
    {
        anim.SetTrigger(trigger);
    }

    public void Flip(int side)
    {
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("BreakColor") && !anim.GetCurrentAnimatorStateInfo(0).IsTag("1") && NewPlayerScript.isCanPushPull == false/*&& !anim.GetAnimatorTransitionInfo(0).IsName("fall -> IdleColor")*/)
        {
            bool state = (side == 1) ? false : true;
            sr.flipX = state;
        }
    }

    bool AnimatorIsPlaying(string stateName)
    {
        return AnimatorIsPlaying() && anim.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }

    bool AnimatorIsPlaying()
    {
        return anim.GetCurrentAnimatorStateInfo(0).length > anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }
}
