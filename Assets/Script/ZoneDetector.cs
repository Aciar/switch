﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneDetector : MonoBehaviour
{
    public bool isWall = false;
    public bool isPlayer = false;
    public static bool WallWantCheck = true;
    public Collider2D me;

    private void Start()
    {
        //Collider = this.GetComponent<Collider>();
    }

    private void Update()
    {
        /*if(isWall == true && WallWantCheck == true)
        {
            EnemyBehaviour.isWallDetect = true;
        }
        else if(isWall == false && WallWantCheck == true)
        {
            EnemyBehaviour.isWallDetect = false;
        }
        else
        {
            Debug.Log("Wall want check : " + WallWantCheck);
        }*/

        if (isPlayer == true)
        {
            EnemyBehaviour.isPlayerDetect = true;
        }
        else
        {
            EnemyBehaviour.isPlayerDetect = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        /*if (other.gameObject.tag == "Wall")
        {
            isWall = true;
            Debug.Log("Wall Detected");
        }
        else
        {
            isWall = false;
        }
        */
        if (other.gameObject.tag == "Player")
        {
            isPlayer = true;
            Debug.Log("Player Detected");
        }
        else
        {
            isPlayer = false;
        }
    }
}
