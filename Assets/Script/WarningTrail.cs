﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningTrail : MonoBehaviour
{

    public float delayTime = 2.5f;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(delayTime);

        Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
