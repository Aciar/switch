﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArea : MonoBehaviour
{
    [Header("Stats")]
    public int speed = 5;
    public int walkvalue = 1;
    public bool isRight = true;
    public bool isFollow = false;
    public bool isFindPlayer = true;

    [Header("FollowRetreatShooting")]
    public float Followspeed;
    public float stoppingDistance;
    public float nearDistance;
    public float startTimeBTWShot;
    private float timeBTWShots;

    public Vector3 initialPoint;
    public bool isBackToInitialPoint = false;
    public float distanceFromFirst = 0.0f;

    [Header("References")]
    public GameObject shot;
    public GameObject shootingPoint;
    private Transform player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if(player == null)
        {
            isFindPlayer = false;
            //Debug.Log("Can't find player");
        }
        initialPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            isFindPlayer = false;
            //Debug.Log("Can't find player");
        }
        Vector3 position = transform.position;
        if (isRight == true && isFollow == false && isBackToInitialPoint == false)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            shootingPoint.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * speed * Time.deltaTime);
            transform.position = position;
        }
        else if (isRight == false && isFollow == false && isBackToInitialPoint == false)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            shootingPoint.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * -speed * Time.deltaTime);
            transform.position = position;
        }

        distanceFromFirst = (position.x - initialPoint.x);
        isFollow = false;
        if (isFindPlayer)
        {
            float distance = (player.transform.position - transform.position).magnitude;
            if (distance < stoppingDistance + 10f && distance == stoppingDistance)
            {
                isFollow = false;
                transform.position = Vector2.MoveTowards(transform.position, player.position, Followspeed * Time.deltaTime);
            }
            else if (distance < stoppingDistance && distance > nearDistance)
            {
                isFollow = true;
                transform.position = this.transform.position;
            }
            else if (distance <= nearDistance)
            {
                isFollow = false;
                transform.position = Vector2.MoveTowards(transform.position, player.position, -Followspeed * Time.deltaTime);
            }

            if (timeBTWShots <= 0 && isFollow == true)
            {

                Instantiate(shot, shootingPoint.transform.position, shootingPoint.transform.rotation);
                //Instantiate(shot, transform.position, Quaternion.identity);
                timeBTWShots = startTimeBTWShot;
            }
            else
            {
                timeBTWShots -= Time.deltaTime;
            }
        }
        else if (isFindPlayer == false)
        {
            isFollow = false;
            if (distanceFromFirst > 0.1)
            {
                isBackToInitialPoint = true;
                this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * -speed * Time.deltaTime);
                transform.position = position;
            }
            else if (distanceFromFirst < -0.1)
            {
                isBackToInitialPoint = true;
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * speed * Time.deltaTime);
                transform.position = position;
            }
            else
            {
                initialPoint = transform.position;
                isBackToInitialPoint = false;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall")
        {
            if (isRight == true)
            {
                isRight = false;
                //Debug.Log("Is right false");
            }
            else
            {
                isRight = true;
                //Debug.Log("Is right true");
            }
        }
    }
}
