﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyList : MonoBehaviour
{
    public static EnemyList _instance;
    public List<GameObject> enemies;
    public List<GameObject> monster;
    public List<GameObject> guard;
    public List<GameObject> turret;
    public List<GameObject> FOVmon;
    public List<GameObject> FOVguard;
    public List<GameObject> FOVtur;
    public static EnemyList Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new EnemyList();
            }

            return _instance;
        }
    }
    private void Awake()
    {
        /*if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }*/
        _instance = this;
        //DontDestroyOnLoad(gameObject);
        enemies = new List<GameObject>();
        monster = new List<GameObject>();
        guard = new List<GameObject>();
        turret = new List<GameObject>();
        FOVmon = new List<GameObject>();
        FOVguard = new List<GameObject>();
        FOVtur = new List<GameObject>();
    }

    public void AddList(GameObject go)
    {
        //Debug.Log("Adding : " + go);
        enemies.Add(go);
        Monsterfind(go);
        Turretfind(go);
        Guardfind(go);
    }

    public void AddMonFOV(GameObject monfov)
    {
        //Debug.Log("Adding monfov : " + monfov);
        FOVmon.Add(monfov);
    }

    public void setTrueMonFOV()
    {
        if (FOVmon != null)
        {
            foreach (GameObject monfov in FOVmon)
            {
                if (monfov != null)
                {
                    monfov.SetActive(true);
                }
            }
        }
    }

    public void setFalseMonFOV()
    {
        if (FOVmon != null)
        {
            foreach (GameObject monfov in FOVmon)
            {
                if (monfov != null)
                {
                    monfov.SetActive(false);
                }
            }
        }
    }

    public void AddGuardFOV(GameObject guardfov)
    {
        //Debug.Log("Adding monfov : " + guardfov);
        FOVguard.Add(guardfov);
    }

    public void setTrueGuardFOV()
    {
        if (FOVguard != null)
        {
            foreach (GameObject guardfov in FOVguard)
            {
                if (guardfov != null)
                {
                    guardfov.SetActive(true);
                }
            }
        }
    }

    public void setFalseGuardFOV()
    {
        if (FOVguard != null)
        {
            foreach (GameObject guardfov in FOVguard)
            {
                if (guardfov != null)
                {
                    guardfov.SetActive(false);
                }
            }
        }
    }

    public void AddTurretFOV(GameObject turfov)
    {
        //Debug.Log("Adding monfov : " + turfov);
        FOVtur.Add(turfov);
    }

    public void setTrueTurretFOV()
    {
        if (FOVtur != null)
        {
            foreach (GameObject turfov in FOVtur)
            {
                if (turfov != null)
                {
                    turfov.SetActive(true);
                }
            }
        }
    }

    public void setFalseTurretFOV()
    {
        if (FOVtur != null)
        {
            foreach (GameObject turfov in FOVtur)
            {
                if (turfov != null)
                {
                    turfov.SetActive(false);
                }
            }
        }
    }

    public void Monsterfind(GameObject go)
    {
        if(go.GetComponent<EnemyShortState>() != null)
        {
            monster.Add(go);
            //Debug.Log("monster : " + go);
        }
    }

    public void Turretfind(GameObject go)
    {
        if (go.GetComponent<TurretController>() != null)
        {
            turret.Add(go);
            //Debug.Log("turret : " + go);
        }
        else
        {
            //Debug.Log("turret no script");
        }
    }

    public void Guardfind(GameObject go)
    {
        if (go.GetComponent<EnemyRaycastState>() != null)
        {
            guard.Add(go);
            //Debug.Log("guard : " + go);
        }
    }
}
