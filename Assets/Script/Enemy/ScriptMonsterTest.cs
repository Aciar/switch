﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptMonsterTest : MonoBehaviour
{
    public enum State
    {
        Walking,
        Waiting,
        ChaseTarget,
        GoingBackToStart,
    }

    [Header("State")]
    public State state;
    [SerializeField] public float waitTimer;
    [SerializeField] public float walkTimer;
    private float waitCountTimer;
    private float walkCountTimer;
    private bool WalkOrWait;

    [Header("Raycast")]
    [SerializeField] public float speed;
    [SerializeField] public float followSpeed;

    [Header("Enemy status")]
    [SerializeField] public GameObject player;
    private Vector3 initialPoint;
    public Rigidbody2D rb;
    private bool isRight;
    [SerializeField] private float walkvalue;
    private Vector3 target;
    private bool isBackToInitialPoint;
    private bool isFindPlayer = false;
    private bool isFollow = false;
    private bool isPlayer = true;
    private float distanceFromFirst = 0.0f;
    public NewPlayerScript playerscript;
    public Vector3 position;

    [Header("FieldOfView")]
    [SerializeField] private Vector3 aimDirection;
    [SerializeField] private Transform pfFieldOfView;
    [SerializeField] private float fov = 45f;
    [SerializeField] private float viewDistance = 5f;
    [SerializeField] private GameObject prefabFieldOfView;
    public static GameObject cloneFOV1;
    private FieldOfView fieldOfView;
    private Vector3 lastMoveDir;
    private bool isHitPlayer = false;
    [SerializeField] private float startTimeBTW;
    private float timeBTW;
    public static bool isClosed = false;
    public Transform damagingPoint;
    /*[Header("References")]
    public Transform damagingPoint;
    public LayerMask whatIsEnemies;
    public float attackRange;
    //public int damage;
    public float startTimeBTW;
    public float timeBTW;
    private NewPlayerScript playerscript;
    RaycastHit2D hit;
    public static bool isPlayerDead = false;
    public bool isFirstBite = false;*/
    [SerializeField] public float attackRange;
    private bool cooling;
    private bool attackMode;
    public static bool isPlayerDead = false;


    private Animator anim;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("player");
        playerscript = GameObject.FindGameObjectWithTag("player").GetComponent<NewPlayerScript>();
        if (player == null)
            isPlayer = false;

        initialPoint = transform.position;
        rb = GetComponent<Rigidbody2D>();

        lastMoveDir = aimDirection;
        cloneFOV1 = (GameObject)Instantiate(prefabFieldOfView);
        fieldOfView = cloneFOV1.GetComponent<FieldOfView>();
        fieldOfView.SetFoV(fov);
        fieldOfView.SetViewDistance(viewDistance);

        anim = GetComponent<Animator>();
        waitCountTimer = waitTimer;
        walkCountTimer = walkTimer;
    }

    private void Update()
    {
        position = transform.position;

        if (cloneFOV1 == null)
        {
            Debug.Log("Clone FOV null");
        }

        switch (state)
        {
            default:
            case State.Waiting:
            case State.Walking:
                HandleMovement();
                FindPlayer();
                timeBTW = startTimeBTW;
                break;

            case State.ChaseTarget:
                FindPlayer();
                break;

            case State.GoingBackToStart:
                distanceFromFirst = (transform.position.x - initialPoint.x);
                isFollow = false;
                if (distanceFromFirst > 1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                }
                else if (distanceFromFirst < -1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                }
                else
                {
                    initialPoint = transform.position;
                    isBackToInitialPoint = false;
                    state = State.Walking;
                }
                break;
        }

        if (fieldOfView != null)
        {
            fieldOfView.SetOrigin(transform.position);
            fieldOfView.SetAimDirection(GetAimDir());
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            setActive();
        }

        Debug.Log("is closed short: " + isClosed);
        if (isClosed == false)
        {
            cloneFOV1.SetActive(true);
        }
    }

    public bool setActive()
    {
        return !isClosed;
    }

    private void HandleMovement()
    {
        switch (state)
        {
            case State.Waiting:
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", false);
                waitCountTimer -= Time.deltaTime;
                anim.SetBool("isWalk", false);
                if (waitCountTimer <= 0f)
                {
                    walkCountTimer = walkTimer;
                    state = State.Walking;
                    WalkOrWait = true;          //Walk = true
                }
                break;
            case State.Walking:
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", false);
                anim.SetBool("isWalk", true);
                if (isRight == true)
                {
                    Debug.Log("is Right");
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(1.0f, 0f, 0f);
                }
                else if (isRight == false)
                {
                    Debug.Log("is left");
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(-1.0f, 0f, 0f);
                }

                walkCountTimer -= Time.deltaTime;
                if (walkCountTimer <= 0f && isHitPlayer == false)
                {
                    Debug.Log("In gonna waiting");
                    state = State.Waiting;
                    WalkOrWait = false;
                    waitCountTimer = waitTimer;
                }
                break;
        }
    }

    private void FindPlayer()
    {
        if (Vector3.Distance(GetPosition(), player.transform.position) < viewDistance)
        {
            // Player inside viewDistance
            Vector3 dirToPlayer = (player.transform.position - GetPosition()).normalized;
            if (Vector3.Angle(GetAimDir(), dirToPlayer) < fov / 2f)
            {
                // Player inside Field of View
                Debug.Log("in field of view");
                RaycastHit2D raycastHit2D = Physics2D.Raycast(GetPosition(), dirToPlayer, viewDistance);
                if (raycastHit2D.collider != null)
                {
                    Debug.Log("Hit Something");
                    // Hit something
                    if (raycastHit2D.collider.gameObject.GetComponent<NewPlayerScript>() != null && isPlayerDead == false)
                    {
                        // Hit Player
                        Debug.Log("Hit player");
                        isHitPlayer = true;
                        StartAttackingPlayer();
                    }
                    else
                    {
                        Debug.Log("Hit something else");
                        // Hit something else
                    }
                }
            }
        }
        else
        {
            if (WalkOrWait == false)
            {
                state = State.Waiting;
            }
            else if (WalkOrWait == true)
            {
                state = State.Walking;
            }
            isHitPlayer = false;
        }
    }

    public void StartAttackingPlayer()
    {
        state = State.ChaseTarget;
        Vector3 dirToTarget = (player.transform.position - GetPosition()).normalized;
        lastMoveDir = dirToTarget;
        if (isRight == true && player.transform.position.x < transform.position.x)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * -speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(-1.0f, 0f, 0f);
            isRight = false;
        }
        else if (isRight == false && player.transform.position.x > transform.position.x)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(1.0f, 0f, 0f);
            isRight = true;
        }


        float distance = (player.transform.position - transform.position).magnitude;
        if (distance > attackRange)
        {
            Move();
            StopAttack();
        }
        else if (attackRange >= distance && cooling == false)
        {
            AttackPlayer();
        }
        
        if(cooling)
        {
            CoolDown();
            anim.SetBool("isClose", false);
        }
    }

    void Move()
    {
        anim.SetBool("isChasing", true);
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Bitev2"))
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), followSpeed * Time.deltaTime);
            isFollow = true;
        }
    }

    void CoolDown()
    {
        timeBTW -= Time.deltaTime;

        if(timeBTW <= 0 && cooling && attackMode)
        {
            cooling = false;
            timeBTW = startTimeBTW;
        }
    }

    void StopAttack()
    {
        cooling = false;
        attackMode = false;
        anim.SetBool("isClose", false);
        anim.SetBool("isChasing", false);
    }

    private void AttackPlayer()
    {
        attackMode = true;
        anim.SetBool("isChasing", false);
        anim.SetBool("isClose", true);
        anim.SetTrigger("isBite");

        /*Collider2D[] playertoDamage = Physics2D.OverlapCircleAll(damagingPoint.position, attackRange, whatIsEnemies);

        for (int i = 0; i < playertoDamage.Length; i++)
        {
            Debug.Log("Damaging");
            anim.SetBool("isClose", false);
            if (playertoDamage[i].CompareTag("player") == true)
            {
                playertoDamage[i].GetComponent<NewPlayerScript>().InvokeDead();
            }
        }*/
    }

    public void TriggerCooling()
    {
        cooling = true;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(damagingPoint.position, attackRange);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall" && state != State.ChaseTarget)
        {
            if (isRight == true && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = false;
                //Debug.Log("Is right false");
            }
            else if (isRight == false && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = true;
                //Debug.Log("Is right true");
            }
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetAimDir()
    {
        return lastMoveDir;
    }
}
