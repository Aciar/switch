﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public enum State
    {
        Patrol,
        ChaseTarget,
        GoingBackToStart,
    }

    [Header("State")]
    public State state;

    [Header("Reference")]
    public Transform damagingPoint;
    private Transform player;
    public Vector3 initialPoint;
    public bool isBackToInitialPoint = false;
    public float distanceFromFirst = 0.0f;

    [Header("Stats")]
    public int speed = 5;
    public int walkvalue = 1;
    public bool isRight = true;
    public bool isFollow = false;
    public bool isFindPlayer = true;

    [Header("FollowRetreatDamaging")]
    public float Followspeed;
    public float stoppingDistance;
    public float nearDistance;
    public float startTimeBTW;
    private float timeBTW;
    public float attackRange;
    public int damage;
    public float distance;
    public LayerMask whatIsEnemies;

    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (player == null)
        {
            isFindPlayer = false;
            //Debug.Log("Can't find player");
        }
        initialPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            isFindPlayer = false;
            //Debug.Log("Can't find player");
        }
        Vector3 position = transform.position;
        //Debug.Log("Player " + isFindPlayer);
        switch (state)
        {
            default:
            case State.Patrol:
                //Vector3 position = transform.position;
                if (isRight == true && isFollow == false /*&& isBackToInitialPoint == false*/)
                {
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    damagingPoint.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                }
                else if (isRight == false && isFollow == false /*&& isBackToInitialPoint == false*/)
                {
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    damagingPoint.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                }
                if (isFindPlayer)
                {
                    FindPlayer(isFindPlayer);
                }
                break;
            case State.ChaseTarget:
                
                if (isFindPlayer)
                {
                    distance = (player.transform.position - transform.position).magnitude;
                    if (distance < stoppingDistance)
                    {
                        if (distance > nearDistance)
                        {
                            transform.position = Vector2.MoveTowards(transform.position, player.position, Followspeed * Time.deltaTime);
                            isFollow = true;
                        }
                        else if (distance <= nearDistance)
                        {
                            transform.position = this.transform.position;
                        }
                    }
                }
                else
                {
                    state = State.GoingBackToStart;
                }
                //FindPlayer(isFindPlayer);
                AttackPlayer(distance);
                FindPlayer(isFindPlayer);
                break;
            case State.GoingBackToStart:
                isFollow = false;
                distanceFromFirst = (position.x - initialPoint.x);
                if (distanceFromFirst > 0.2)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    damagingPoint.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                }
                else if (distanceFromFirst < -0.2)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    damagingPoint.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                }
                else
                {
                    initialPoint = transform.position;
                    isBackToInitialPoint = false;
                    state = State.Patrol;
                }
                //FindPlayer(isFindPlayer);
                break;
        }
        //Debug.Log("State : " + state);
    }

    private void FindPlayer(bool isFindPlayer)
    {
        //Debug.Log("State in find player: " + state);
        if (isFindPlayer)
        {
            distance = (player.transform.position - transform.position).magnitude;
            if (distance < stoppingDistance)
            {
                state = State.ChaseTarget;
                isFollow = true;
            }
            else
            {
                state = State.Patrol;
                isFollow = false;
            }
        }
        else if(isFindPlayer == false)
        {
            //Debug.Log("State in find player false: " + state);
            state = State.GoingBackToStart;
            isBackToInitialPoint = true;
        }

    }

    private void AttackPlayer(float distance)
    {
        if (timeBTW <= 0 && distance <= 1.6)
        {
            Collider2D[] playertoDamage = Physics2D.OverlapCircleAll(damagingPoint.position, attackRange, whatIsEnemies);
            Debug.Log(playertoDamage);
            for (int i = 0; i < playertoDamage.Length; i++)
            {
                Debug.Log("Damaging");
                playertoDamage[i].GetComponent<Player>().TakeDamage(damage);
            }
            timeBTW = startTimeBTW;
        }
        else
        {
            timeBTW -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall")
        {
            if (isRight == true && isBackToInitialPoint == false && isFollow == false)
            {
                isRight = false;
                //Debug.Log("Is right false");
            }
            else if (isRight == false && isBackToInitialPoint == false && isFollow == false)
            {
                isRight = true;
                //Debug.Log("Is right true");
            }
        }
    }
}
