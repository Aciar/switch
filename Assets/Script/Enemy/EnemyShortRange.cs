﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyShortRange : MonoBehaviour
{
    [Header("Stats")]
    public int speed = 5;
    public int walkvalue = 1;
    public bool isRight = true;
    public bool isFollow = false;
    public bool isFindPlayer = true;

    [Header("FollowRetreatDamaging")]
    public float Followspeed;
    public float stoppingDistance;
    public float nearDistance;
    public float startTimeBTW;
    private float timeBTW;

    public Vector3 initialPoint;
    public bool isBackToInitialPoint = false;
    public float distanceFromFirst = 0.0f;

    [Header("References")]
    //public GameObject Weapon;
    public Transform damagingPoint;
    public LayerMask whatIsEnemies;
    public float attackRange;
    public int damage;
    private Transform player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (player == null)
        {
            isFindPlayer = false;
            //Debug.Log("Can't find player");
        }
        initialPoint = transform.position;
        //initialPoint.Set(transform.position.x, transform.position.y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            isFindPlayer = false;
            //Debug.Log("Can't find player");
        }
        Vector3 position = transform.position;
        if (isRight == true && isFollow == false && isBackToInitialPoint == false)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            damagingPoint.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * speed * Time.deltaTime);
            transform.position = position;
        }
        else if (isRight == false && isFollow == false && isBackToInitialPoint == false)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            damagingPoint.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * -speed * Time.deltaTime);
            transform.position = position;
        }

        distanceFromFirst = (position.x - initialPoint.x);

        if (isFindPlayer)
        {
            float distance = (player.transform.position - transform.position).magnitude;

            if (distance < stoppingDistance)
            {
                if (distance > nearDistance)
                {
                    transform.position = Vector2.MoveTowards(transform.position, player.position, Followspeed * Time.deltaTime);
                    isFollow = true;
                }
                else if (distance <= nearDistance)
                {
                    transform.position = this.transform.position;
                }
            }
            if (timeBTW <= 0 && distance <= 1.6)
            {
                Collider2D[] playertoDamage = Physics2D.OverlapCircleAll(damagingPoint.position, attackRange, whatIsEnemies);
                Debug.Log(playertoDamage);
                for (int i = 0; i < playertoDamage.Length; i++)
                {
                    Debug.Log("Damaging");
                    playertoDamage[i].GetComponent<Player>().TakeDamage(damage);
                }
                timeBTW = startTimeBTW;
            }
            else
            {
                timeBTW -= Time.deltaTime;
            }
        }
        else if(isFindPlayer == false)
        {
            isFollow = false;
            if (distanceFromFirst > 0.6)
            {
                isBackToInitialPoint = true;
                this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                damagingPoint.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * -speed * Time.deltaTime);
                transform.position = position;
            }
            else if (distanceFromFirst < -0.6)
            {
                isBackToInitialPoint = true;
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                damagingPoint.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * speed * Time.deltaTime);
                transform.position = position;
            }
            else
            {
                initialPoint = transform.position;
                isBackToInitialPoint = false;
            }
        }

        //float distanceFromFirst = (position.x - initialPoint.x);
        //ทำให้เป็น 0-1 แล้วเอาไปคูณ Speed = direction

        //Debug.Log("distanceFromFirst : " + distanceFromFirst);
        /*if (isBackToInitialPoint == true)
        {
            Debug.Log("distanceFromFirst : " + distanceFromFirst);
            if (distanceFromFirst > 0.6)
            {
                this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                damagingPoint.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * -speed * Time.deltaTime);
                transform.position = position;
            }
            else if (distanceFromFirst < -0.6)
            {
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                damagingPoint.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * speed * Time.deltaTime);
                transform.position = position;
            }
            else 
            {
                distanceFromFirst = position.x;
                isBackToInitialPoint = false;
            }
        }*/
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(damagingPoint.position, attackRange);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall")
        {
            if (isRight == true && isBackToInitialPoint == false)
            {
                isRight = false;
                //Debug.Log("Is right false");
            }
            else if (isRight == false && isBackToInitialPoint == false)
            {
                isRight = true;
                //Debug.Log("Is right true");
            }
        }
    }
}
