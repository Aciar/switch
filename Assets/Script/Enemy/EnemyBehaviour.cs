﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    [Header("Stats")]
    public int speed = 5;
    public int walkvalue = 1;
    public bool isRight = true;
    public static bool isWallDetect = false;
    public static bool isPlayerDetect = false;
    public Rigidbody2D EnemyRigid;
    public GameObject Detector;

    [Header("FollowRetreatShooting")]
    public float Followspeed;
    public float stoppingDistance;
    public float nearDistance;
    public float startTimeBTWShort;
    private float timeBTWShots;

    [Header("References")]
    public GameObject shot;
    private Transform player;
    //public Animation EnemyAnim;

    // Start is called before the first frame update
    void Start()
    {
        EnemyRigid = this.GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //EnemyAnim = this.GetComponent<Animation>();

    }

    void Update()
    {
        //Walk between 2 walls with no Player
        Vector3 position = transform.position;
        /*if (isRight == true && isPlayerDetect == false)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * speed * Time.deltaTime);
            transform.position = position;
        }
        else if (isRight == false && isPlayerDetect == false)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            position.x -= (walkvalue * speed * Time.deltaTime);
            transform.position = position;
        }*/
        /*else if (isPlayerDetect == true)
        {
            position.x -= 0;
            transform.position = position;
        }*/

        //Walk to Player / Retreat
        //1.Stay 
        /*if (isPlayerDetect == true && Vector2.Distance(transform.position,player.position) > stoppingDistance)
        {
            
        }*/


        //If Detection check wall
        /*if (isPlayerDetect == true && isWallDetect == true)
        {
            Detector.SetActive(true);
        }
        else if (isWallDetect == false && isPlayerDetect == false)
        {
            Detector.SetActive(true);
        }
        else
        {
            Detector.SetActive(false);
        }

        if (isPlayerDetect == true)
        {
            if (isWallDetect == false)
            {
                Detector.SetActive(true);
            }
            else
            {
                Detector.SetActive(false);
            }
        }
        else if (isPlayerDetect == false)
        {
            if (isWallDetect == false)
            {
                Detector.SetActive(true);
            }
            else
            {
                Detector.SetActive(false);
            }
        }
        */
        //Detector
        ZoneDetector.WallWantCheck = true;
        float distance = (player.position - transform.position).magnitude;
        //Debug.Log("Distance : " + distance);
        if (isPlayerDetect == true && isWallDetect == false)
        {
            //ZoneDetector.WallWantCheck = true;
            Detector.SetActive(true);
            position.x -= 0;
            transform.position = position;
            Shoot();
            //Debug.Log("Distance : " + distance);
        }
        else if (isPlayerDetect == true && isWallDetect == true)
        {
            ZoneDetector.WallWantCheck = false;
            Detector.SetActive(true);
            position.x -= 0;
            transform.position = position;
            Shoot();
            //Debug.Log("Distance : " + distance);
        }
        else if (isPlayerDetect == false && isWallDetect == false)
        {
            //ZoneDetector.WallWantCheck = true;
            Detector.SetActive(true);
            if (isRight == true )
            {
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * speed * Time.deltaTime);
                transform.position = position;
            }
            else if (isRight == false)
            {
                this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                position.x -= (walkvalue * speed * Time.deltaTime);
                transform.position = position;
            }
        }
        else if (isPlayerDetect == false && isWallDetect == true)
        {
            //ZoneDetector.WallWantCheck = true;
            Detector.SetActive(false);
            if (isRight == true)
            {
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * speed * Time.deltaTime);
                transform.position = position;
            }
            else if (isRight == false)
            {
                this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                position.x -= (walkvalue * speed * Time.deltaTime);
                transform.position = position;
            }
        }



            //Debug.Log("Player Detected : " + isPlayerDetect);
        //Enemy shooting
        if (isPlayerDetect == true && timeBTWShots <=0)
        {
            speed = 0;
            Debug.Log("Player Detected in loop : " + isPlayerDetect);
            Instantiate(shot, transform.position, Quaternion.identity);
            timeBTWShots = startTimeBTWShort;
        }
        else
        {
            speed = 5;
            timeBTWShots -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall")
        {
            if (isRight == true)
            {
                isRight = false;
            }
            else
            {
                isRight = true;
            }
        }
    }

    public void Shoot()
    {
        Instantiate(shot, transform.position, Quaternion.identity);
        timeBTWShots = startTimeBTWShort;
    }
}
