﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TurretController : MonoBehaviour
{
    public GameObject Player;
    public float speed = 1.0f;
    public GameObject bulletPoint;
    public GameObject shot;
    public GameObject origin;
    public Vector3 Originpoint;
    public Vector3 buletDir;
    public static bool isClosed = false;

    [Header("FieldOfView")]
    [SerializeField] private Vector3 aimDirection;
    [SerializeField] private Transform pfFieldOfView;
    [SerializeField] private float fov = 90f;
    [SerializeField] private float viewDistance = 50f;
    private FieldOfView fieldOfView;
    [SerializeField] private GameObject prefabFieldOfView;
    public static GameObject cloneFOV;
    private Vector3 lastMoveDir;
    public float startTimeBTWShot;
    public float timeBTWShots;

    //For finding player again
    bool isSceneLoaded = false;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("player");
        EnemyList._instance.AddList(this.gameObject);
        //Originpoint = origin.transform.position;
        lastMoveDir = aimDirection;
        cloneFOV = (GameObject)Instantiate(prefabFieldOfView);
        //fieldOfView = Instantiate(pfFieldOfView, null).GetComponent<FieldOfView>();
        EnemyList._instance.AddTurretFOV(cloneFOV);
        fieldOfView = cloneFOV.GetComponent<FieldOfView>();
        fieldOfView.SetFoV(fov);
        fieldOfView.SetViewDistance(viewDistance);
        timeBTWShots = startTimeBTWShot;

    }

    // Update is called once per frame
    void Update()
    {
        Player = GameObject.FindGameObjectWithTag("player");
        if(Player == null)
        {
            Start();
        }
        Originpoint = origin.transform.position;
        Vector3 moveDirection = Player.transform.position - transform.position;
        if (moveDirection != Vector3.zero)
        {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle - 90.0f, Vector3.forward);
        }
        buletDir = bulletPoint.transform.position;
        lastMoveDir = moveDirection;
        if (fieldOfView != null)
        {
            fieldOfView.SetOrigin(Originpoint);
            fieldOfView.SetAimDirection(GetAimDir());
        }

        if (Vector3.Distance(GetPosition(), Player.transform.position) < viewDistance)
        {
            Vector3 dirToPlayer = (Player.transform.position - GetPosition()).normalized;
            if (Vector3.Angle(GetAimDir(), dirToPlayer) < fov / 2f)
            {
                RaycastHit2D raycastHit2D = Physics2D.Raycast(GetPosition(), dirToPlayer, viewDistance);
                if (raycastHit2D.collider != null)
                {
                    // Hit something
                    if (raycastHit2D.collider.gameObject.GetComponent<NewPlayerScript>() != null)
                    {
                        // Hit Player
                        Shoot();
                    }
                    else
                    {
                        // Hit something else
                    }
                }
                else
                {
                    timeBTWShots = startTimeBTWShot;
                }
            }
        }

        if (Input.GetKeyDown(InputManager.instance.diSwitch))
        {
            setActive();
        }

        if (isClosed == false)
        {
            cloneFOV.SetActive(true);
        }

        if (isSceneLoaded)
        {
            isSceneLoaded = false;
        }
    }

    public bool setActive()
    {
        return !isClosed;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        isSceneLoaded = true;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void Shoot()
    {
        if (timeBTWShots <= 0 && isClosed == false)
        {
            Instantiate(shot, bulletPoint.transform.position, bulletPoint.transform.rotation);
            timeBTWShots = startTimeBTWShot;
        }
        else if(timeBTWShots <= 0 && isClosed == true)
        {
            timeBTWShots = startTimeBTWShot;
        }
        else
        {
            timeBTWShots -= Time.deltaTime;
        }
        
    }


    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetAimDir()
    {
        return lastMoveDir;
    }
}
