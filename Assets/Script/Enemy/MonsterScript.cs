﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterScript : MonoBehaviour
{
    public enum State
    {
        Walking,
        Waiting,
        ChaseTarget,
        GoingBackToStart,
    }

    [Header("State")]
    public static State state;
    public static float waitTimer = 2f;
    public static float walkTimer = 15f;
    public static float waitCountTimer;
    public static float walkCountTimer;
    public static bool WalkOrWait;

    [Header("Raycast")]
    public static float speed = 1f;
    public static float followSpeed = 2f;

    [Header("Enemy status")]
    GameObject player;
    Vector3 initialPoint;
    public Rigidbody2D rb;
    public bool isRight;
    public float walkvalue;
    public Vector3 target;
    private bool isBackToInitialPoint;
    private bool isFindPlayer = false;
    private bool isFollow = false;
    public bool isPlayer = true;
    float distanceFromFirst = 0.0f;

    [Header("FieldOfView")]
    [SerializeField] private Vector3 aimDirection;
    [SerializeField] private Transform pfFieldOfView;
    [SerializeField] private float fov = 90f;
    [SerializeField] private float viewDistance = 50f;
    [SerializeField] private GameObject prefabFieldOfView;
    public static GameObject cloneFOV1;
    private FieldOfView fieldOfView;
    private Vector3 lastMoveDir;
    public bool isHitPlayer = false;

    [Header("References")]
    public Transform damagingPoint;
    public LayerMask whatIsEnemies;
    public float attackRange;
    public float startTimeBTW;
    public float timeBTW;
    public float delayTimeBTW;
    public float time;
    private NewPlayerScript playerscript;
    RaycastHit2D hit;
    public Vector3 position;
    public static bool isPlayerDead = false;
    public bool isFirstBite = false;
    public static bool isClosed = false;
    public bool cooling = false;
    public bool attackMode = false;

    public Animator anim;
    //For finding player again
    bool isSceneLoaded = false;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("player");
        playerscript = GameObject.FindGameObjectWithTag("player").GetComponent<NewPlayerScript>();
        if (player == null)
            isPlayer = false;

        initialPoint = transform.position;
        rb = GetComponent<Rigidbody2D>();

        lastMoveDir = aimDirection;
        cloneFOV1 = (GameObject)Instantiate(prefabFieldOfView);
        cloneFOV1.tag = "MonFOV";
        cloneFOV1.transform.parent = transform;
        fieldOfView = cloneFOV1.GetComponent<FieldOfView>();
        fieldOfView.SetFoV(fov);
        fieldOfView.SetViewDistance(viewDistance);
        //SetParent(cloneFOV1);

        anim = GetComponent<Animator>();
        waitCountTimer = waitTimer;
        walkCountTimer = walkTimer;
    }

    private void Update()
    {
        position = transform.position;
        //SetParent(cloneFOV1);
        if (cloneFOV1 == null)
        {
            Debug.Log("Clone FOV null");
        }

        switch (state)
        {
            default:
            case State.Waiting:
            case State.Walking:
                HandleMovement();
                FindPlayer();
                timeBTW = startTimeBTW;
                break;


            case State.ChaseTarget:
                FindPlayer();
                break;

            case State.GoingBackToStart:
                distanceFromFirst = (transform.position.x - initialPoint.x);
                isFollow = false;
                if (distanceFromFirst > 1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                }
                else if (distanceFromFirst < -1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                }
                else
                {
                    initialPoint = transform.position;
                    isBackToInitialPoint = false;
                    state = State.Walking;
                }
                break;
        }

        if (fieldOfView != null)
        {
            fieldOfView.SetOrigin(transform.position);
            fieldOfView.SetAimDirection(GetAimDir());
        }

    }

    void OnDisable()
    {
        Debug.Log("PrintOnDisable: script was disabled");
        DetachFromParent(cloneFOV1);
    }

    void OnEnable()
    {
        Debug.Log("PrintOnEnable: script was enabled");
        SetParent(cloneFOV1);
    }

    public void SetParent(GameObject cloneFOV)
    {
        //Makes the GameObject "newParent" the parent of the GameObject "player".
        //cloneFOV.transform.parent = this.transform;

        //Display the parent's name in the console.
        //Debug.Log("Player's Parent: " + cloneFOV.transform.parent.name);
    }

    public void DetachFromParent(GameObject cloneFOV)
    {
        cloneFOV.transform.parent = null;
    }

    public bool setActive()
    {
        return !isClosed;
    }

    private bool CheckMovementPermitted()
    {
        return state != State.ChaseTarget;
    }

    private void HandleMovement()
    {
        if (CheckMovementPermitted() == false)
        {
            return;
        }
        switch (state)
        {
            case State.Waiting:
                isFirstBite = false;
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", false);
                waitCountTimer -= Time.deltaTime;
                anim.SetBool("isWalk", false);
                if (waitCountTimer <= 0f)
                {
                    walkCountTimer = walkTimer;
                    state = State.Walking;
                    WalkOrWait = true;          //Walk = true
                }
                break;
            case State.Walking:
                isFirstBite = false;
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", false);
                anim.SetBool("isWalk", true);
                if (isRight == true)
                {
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(1.0f, 0f, 0f);
                }
                else if (isRight == false)
                {
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(-1.0f, 0f, 0f);
                }

                walkCountTimer -= Time.deltaTime;
                if (walkCountTimer <= 0f && isHitPlayer == false)
                {
                    state = State.Waiting;
                    WalkOrWait = false;
                    waitCountTimer = waitTimer;
                }
                break;
        }
    }

    private void FindPlayer()
    {
        if (Vector3.Distance(GetPosition(), player.transform.position) < viewDistance)
        {
            // Player inside viewDistance
            Vector3 dirToPlayer = (player.transform.position - GetPosition()).normalized;
            if (Vector3.Angle(GetAimDir(), dirToPlayer) < fov / 2f)
            {
                // Player inside Field of View
                RaycastHit2D raycastHit2D = Physics2D.Raycast(GetPosition(), dirToPlayer, viewDistance);
                if (raycastHit2D.collider != null)
                {
                    // Hit something
                    if (raycastHit2D.collider.gameObject.GetComponent<NewPlayerScript>() != null && isPlayerDead == false)
                    {
                        // Hit Player
                        isHitPlayer = true;
                        StartAttackingPlayer();
                    }
                    else
                    {
                        // Hit something else
                    }
                }
            }
        }
        else
        {

            if (WalkOrWait == false)
            {
                state = State.Waiting;
            }
            else if (WalkOrWait == true)
            {
                state = State.Walking;
            }
            isFirstBite = false;
            isHitPlayer = false;
        }
    }

    public void StartAttackingPlayer()
    {
        state = State.ChaseTarget;
        Vector3 dirToTarget = (player.transform.position - GetPosition()).normalized;
        lastMoveDir = dirToTarget;
        if (isRight == true && player.transform.position.x < transform.position.x)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            lastMoveDir = new Vector3(-1.0f, 0f, 0f);
            isRight = false;
        }
        else if (isRight == false && player.transform.position.x > transform.position.x)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            lastMoveDir = new Vector3(1.0f, 0f, 0f);
            isRight = true;
        }


        float distance = (player.transform.position - transform.position).magnitude;
        if (distance > attackRange)
        {
            time -= Time.deltaTime;
            if (time <= 0)
            {
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", true);
                attackMode = true;
                AttackPlayer();
                time = delayTimeBTW;
            }
            else
            {
                if (anim.GetCurrentAnimatorStateInfo(0).IsTag("1"))
                {
                    transform.position += Vector3.zero;
                }
                else
                {
                    Move();
                }
                StopAttack();
            }
        }
        else if (attackRange >= distance && cooling == false && isFirstBite == false)
        {
            anim.SetBool("isChasing", false);
            anim.SetBool("isClose", true);
            attackMode = true;
            AttackPlayer();
        }
        else if (attackRange >= distance && cooling == false && isFirstBite == true)
        {
            anim.SetBool("isChasing", false);
            anim.SetBool("isClose", true);
            attackMode = true;
            time -= Time.deltaTime;
            if (time <= 0)
            {
                AttackPlayer();
                time = delayTimeBTW;
            }
        }


        if (cooling)
        {
            anim.SetBool("isClose", false);
            CoolDown();

        }
    }

    void Move()
    {
        anim.SetBool("isChasing", true);
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("BITE2") && CheckMovementPermitted() == false && attackMode == false)
        {
            FollowPlayer();
            isFollow = true;
        }
        else if (attackMode == true)
        {
            rb.velocity = Vector2.zero;
            transform.position += Vector3.zero;
        }
    }

    void FollowPlayer()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), followSpeed * Time.deltaTime);
    }

    void CoolDown()
    {
        timeBTW -= Time.deltaTime;

        if (timeBTW <= 0 && cooling && attackMode)
        {
            cooling = false;
            timeBTW = startTimeBTW;
            time = delayTimeBTW;
        }
    }

    void StopAttack()
    {
        cooling = false;
        attackMode = false;
        anim.SetBool("isClose", false);
    }

    private void AttackPlayer()
    {
        rb.velocity = Vector2.zero;
        transform.position += Vector3.zero;
        attackMode = true;
        isFirstBite = true;

        anim.SetBool("isWalk", false);
        anim.SetBool("isChasing", false);
        anim.SetBool("isClose", true);
        anim.SetTrigger("isBite");

    }

    public void TriggerCooling()
    {
        cooling = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall" && state != State.ChaseTarget)
        {
            if (isRight == true && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = false;
            }
            else if (isRight == false && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = true;
            }
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetAimDir()
    {
        return lastMoveDir;
    }


}
