﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EnemyRaycastState : MonoBehaviour
{
    public enum State
    {
        Walking,
        Waiting,
        ChaseTarget,
        GoingBackToStart,
    }

    [Header("State")]
    public State state;
    public float waitTimer;
    public float walkTimer;
    public float waitCountTimer;
    public float walkCountTimer;
    public bool WalkOrWait;

    [Header("Raycast")]
    //public float visionRadius;
    public float attackRadius;
    public float speed;

    [Header("Enemy status")]
    public GameObject player;
    public Vector3 initialPoint;
    public Rigidbody2D rb;
    private bool isRight;
    public float walkvalue;
    public Vector3 target;
    private bool isBackToInitialPoint;
    private bool isFindPlayer = false;
    private bool isFollow = false;
    public bool isPlayer = true;
    float distanceFromFirst = 0.0f;

    [Header("FieldOfView")]
    [SerializeField] private Vector3 aimDirection;
    [SerializeField] private Transform pfFieldOfView;
    [SerializeField] private float fov = 90f;
    [SerializeField] private float viewDistance = 50f;
    [SerializeField] private GameObject prefabFieldOfView;
    public static GameObject cloneFOV;
    private FieldOfView fieldOfView;
    private Vector3 lastMoveDir;
    public bool isHitPlayer = false;

    [Header("Attack_Player")]
    public GameObject shot;
    public GameObject shootingPoint;
    public float startTimeBTWShot;
    public float timeBTWShots;
    public Vector3 position;
    public static bool isPlayerDead = false;
    public static bool isClosed = false;
    public static bool playerGotStun = false;

    public Animator anim;
    //For finding player again
    bool isSceneLoaded = false;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("player");
        if (player == null)
            isPlayer = false;
        EnemyList._instance.AddList(this.gameObject);
        initialPoint = transform.position;
        rb = GetComponent<Rigidbody2D>();

        lastMoveDir = aimDirection;
        cloneFOV = (GameObject)Instantiate(prefabFieldOfView);
        //fieldOfView = Instantiate(pfFieldOfView, null).GetComponent<FieldOfView>();
        EnemyList._instance.AddGuardFOV(cloneFOV);
        fieldOfView = cloneFOV.GetComponent<FieldOfView>();
        fieldOfView.SetFoV(fov);
        fieldOfView.SetViewDistance(viewDistance);

        anim = GetComponent<Animator>();

        waitCountTimer = waitTimer;
        walkCountTimer = walkTimer;
    }

    private void Update()
    {
        player = GameObject.FindGameObjectWithTag("player");
        if (player == null)
        {
            isPlayer = false;
            Start();
        }
        else
        {
            isPlayer = true;
            isPlayerDead = false;
        }
        position = transform.position;
        if (cloneFOV == null)
        {
            Debug.Log("Clone FOV null");
        }

        switch (state)
        {
            default:
            case State.Waiting:
                HandleMovement();
                break;

            case State.Walking:
                HandleMovement();
                FindPlayer();
                timeBTWShots = startTimeBTWShot;
                break;

            case State.ChaseTarget:
                FindPlayer();
                break;

            case State.GoingBackToStart:
                distanceFromFirst = (transform.position.x - initialPoint.x);
                isFollow = false;
                if (distanceFromFirst > 1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                }
                else if (distanceFromFirst < -1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                }
                else
                {
                    initialPoint = transform.position;
                    isBackToInitialPoint = false;
                    state = State.Walking;
                }
                break;
        }

        if(playerGotStun == true)
        {
            Vector3 moveDirection = player.transform.position - transform.position;
            lastMoveDir = moveDirection;
            state = State.ChaseTarget;
            StartAttackingPlayer();
        }

        if (fieldOfView != null)
        {
            fieldOfView.SetOrigin(transform.position);
            fieldOfView.SetAimDirection(GetAimDir());
        }

        if (Input.GetKeyDown(InputManager.instance.diSwitch))
        {
            setActive();
        }

        if (isClosed == false)
        {
            cloneFOV.SetActive(true);
        }

        if (isSceneLoaded)
        {
            isSceneLoaded = false;
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        isSceneLoaded = true;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public bool setActive()
    {
        return !isClosed;
    }

    private void HandleMovement()
    {
        switch (state)
        {
            case State.Waiting:
                anim.SetBool("isAiming", false);
                waitCountTimer -= Time.deltaTime;
                anim.SetBool("isWalk", false);
                if (waitCountTimer <= 0f)
                {
                    walkCountTimer = walkTimer;
                    state = State.Walking;
                    WalkOrWait = true;          //Walk = true
                }
                break;
            case State.Walking:
                anim.SetBool("isAiming", false);
                anim.SetBool("isWalk", true);
                if (isRight == true)
                {
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(1.0f, 0f, 0f);
                }
                else if (isRight == false)
                {
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(-1.0f, 0f, 0f);
                }
                
                walkCountTimer -= Time.deltaTime;
                if (walkCountTimer <= 0f && isHitPlayer == false)
                {
                    state = State.Waiting;
                    WalkOrWait = false;
                    waitCountTimer = waitTimer;
                }
                break;
        }
    }

    private void FindPlayer()
    {
        if (Vector3.Distance(GetPosition(), player.transform.position) < viewDistance && playerGotStun == false)
        {
            // Player inside viewDistance
            Vector3 dirToPlayer = (player.transform.position - GetPosition()).normalized;
            if (Vector3.Angle(GetAimDir(), dirToPlayer) < fov / 2f)
            {
                // Player inside Field of View
                Debug.Log("in field of view guard");
                RaycastHit2D raycastHit2D = Physics2D.Raycast(GetPosition(), dirToPlayer, viewDistance);
                if (raycastHit2D.collider != null)
                {
                    Debug.Log("Hit Something guard");
                    // Hit something
                    if (raycastHit2D.collider.gameObject.GetComponent<NewPlayerScript>() != null && isPlayerDead == false)
                    {
                        // Hit Player
                        Debug.Log("Hit player guard");
                        isHitPlayer = true;
                        StartAttackingPlayer();
                    }
                    else
                    {
                        Debug.Log("Hit something else guard");
                        Debug.Log("what face : "+ raycastHit2D.collider.gameObject);
                        // Hit something else
                    }
                }
                //StartAttackingPlayer();
            }
            else
            {
                if (WalkOrWait == false)
                {
                    state = State.Waiting;
                }
                else if (WalkOrWait == true)
                {
                    state = State.Walking;
                }
                isHitPlayer = false;
            }
        }
        else
        {
            if (WalkOrWait == false)
            {
                state = State.Waiting;
            }
            else if(WalkOrWait == true)
            {
                state = State.Walking;
            }
            isHitPlayer = false;
        }
    }

    public void StartAttackingPlayer()
    {
        //AttackPlayer();

        StartCoroutine(AttackPlayerIE());
    }


    private void AttackPlayer()
    {
        state = State.ChaseTarget;
        Vector3 dirToTarget = (player.transform.position - GetPosition()).normalized;
        lastMoveDir = dirToTarget;

        transform.position = this.transform.position;
        //transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        if (isRight == true && player.transform.position.x < transform.position.x)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * -speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(-1.0f, 0f, 0f);
            isRight = false;
        }
        else if (isRight == false && player.transform.position.x > transform.position.x)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(1.0f, 0f, 0f);
            isRight = true;
        }
        if (timeBTWShots <= 0)
        {
            anim.SetTrigger("isShoot");
            Vector3 moveDirection = player.transform.position - transform.position;
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            Instantiate(shot, shootingPoint.transform.position, Quaternion.Euler(0f,0f,angle));
            timeBTWShots = startTimeBTWShot;
        }
        else
        {
            timeBTWShots -= Time.deltaTime;
            anim.SetBool("isAiming", true);
        }
    }

    IEnumerator AttackPlayerIE()
    {
        state = State.ChaseTarget;
        Vector3 dirToTarget = (player.transform.position - GetPosition()).normalized;
        lastMoveDir = dirToTarget;

        transform.position = this.transform.position;
        //transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        if (isRight == true && player.transform.position.x < transform.position.x)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * -speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(-1.0f, 0f, 0f);
            isRight = false;
        }
        else if (isRight == false && player.transform.position.x > transform.position.x)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(1.0f, 0f, 0f);
            isRight = true;
        }
        if (timeBTWShots <= 0)
        {
            anim.SetTrigger("isShoot");
            Vector3 moveDirection = player.transform.position - transform.position;
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            Instantiate(shot, shootingPoint.transform.position, Quaternion.Euler(0f, 0f, angle));
            timeBTWShots = startTimeBTWShot;
        }
        else
        {
            timeBTWShots -= Time.deltaTime;
            anim.SetBool("isAiming", true);
        }
        yield return new WaitForSeconds(0.5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall" && state != State.ChaseTarget)
        {
            if (isRight == true && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = false;
                //Debug.Log("Is right false");
            }
            else if (isRight == false && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = true;
                //Debug.Log("Is right true");
            }
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetAimDir()
    {
        return lastMoveDir;
    }
}
