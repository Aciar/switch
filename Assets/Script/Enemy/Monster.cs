﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    public enum State
    {
        Walking,
        Waiting,
        ChaseTarget,
        GoingBackToStart,
    }

    [Header("State")]
    public State state;
    public float waitTimer;
    public float walkTimer;
    public float waitCountTimer;
    public float walkCountTimer;
    public bool WalkOrWait;

    [Header("Raycast")]
    public float speed;
    public float followSpeed;

    [Header("Enemy status")]
    GameObject player;
    Vector3 initialPoint;
    public Rigidbody2D rb;
    public bool isRight;
    public float walkvalue;
    public Vector3 target;
    private bool isBackToInitialPoint;
    private bool isFindPlayer = false;
    private bool isFollow = false;
    public bool isPlayer = true;
    float distanceFromFirst = 0.0f;

    [Header("FieldOfView")]
    [SerializeField] private Vector3 aimDirection;
    [SerializeField] private Transform pfFieldOfView;
    [SerializeField] private float fov = 90f;
    [SerializeField] private float viewDistance = 50f;
    [SerializeField] private GameObject prefabFieldOfView;
    public static GameObject cloneFOV1;
    private FieldOfView fieldOfView;
    private Vector3 lastMoveDir;
    public bool isHitPlayer = false;

    [Header("References")]
    //public GameObject Weapon;
    public Transform damagingPoint;
    public LayerMask whatIsEnemies;
    public float attackRange;
    //public int damage;
    public float startTimeBTW;
    public float timeBTW;
    public float delayTimeBTW;
    public float time;
    private NewPlayerScript playerscript;
    RaycastHit2D hit;
    public Vector3 position;
    public static bool isPlayerDead = false;
    public bool isFirstBite = false;
    public static bool isClosed = false;
    public bool cooling = false;
    public bool attackMode = false;

    public Animator anim;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("player");
        playerscript = GameObject.FindGameObjectWithTag("player").GetComponent<NewPlayerScript>();
        if (player == null)
            isPlayer = false;

        initialPoint = transform.position;
        rb = GetComponent<Rigidbody2D>();

        lastMoveDir = aimDirection;
        cloneFOV1 = (GameObject)Instantiate(prefabFieldOfView);
        fieldOfView = cloneFOV1.GetComponent<FieldOfView>();
        fieldOfView.SetFoV(fov);
        fieldOfView.SetViewDistance(viewDistance);

        anim = GetComponent<Animator>();
        waitCountTimer = waitTimer;
        walkCountTimer = walkTimer;
    }

    private void Update()
    {
        position = transform.position;

        if (cloneFOV1 == null)
        {
            Debug.Log("Clone FOV null");
        }

        switch (state)
        {
            default:
            case State.Waiting:

            case State.Walking:
                HandleMovement();
                FindPlayer();
                timeBTW = startTimeBTW;
                break;


            case State.ChaseTarget:
                FindPlayer();
                break;

            case State.GoingBackToStart:
                distanceFromFirst = (transform.position.x - initialPoint.x);
                isFollow = false;
                if (distanceFromFirst > 1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                }
                else if (distanceFromFirst < -1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                }
                else
                {
                    initialPoint = transform.position;
                    isBackToInitialPoint = false;
                    state = State.Walking;
                }
                break;
        }

        if (fieldOfView != null)
        {
            fieldOfView.SetOrigin(transform.position);
            fieldOfView.SetAimDirection(GetAimDir());
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            setActive();
        }

        Debug.Log("is closed short: " + isClosed);
        if (isClosed == false)
        {
            cloneFOV1.SetActive(true);
        }
    }
    
    public bool setActive()
    {
        return !isClosed;
    }

    private void HandleMovement()
    {
        switch (state)
        {
            case State.Waiting:
                isFirstBite = false;

                //Animation
                anim.SetBool("isWalk", false);
                anim.SetBool("isChase", false);

                waitCountTimer -= Time.deltaTime;
                if (waitCountTimer <= 0f)
                {
                    walkCountTimer = walkTimer;
                    state = State.Walking;
                    WalkOrWait = true;          //Walk = true
                }
                break;
            case State.Walking:
                isFirstBite = false;

                //Animation
                anim.SetBool("isWalk", true);
                anim.SetBool("isChase", false);

                if (isRight == true)
                {
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(1.0f, 0f, 0f);
                }
                else if (isRight == false)
                {
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(-1.0f, 0f, 0f);
                }

                walkCountTimer -= Time.deltaTime;
                if (walkCountTimer <= 0f && isHitPlayer == false)
                {
                    Debug.Log("In gonna waiting");
                    state = State.Waiting;
                    WalkOrWait = false;
                    waitCountTimer = waitTimer;
                }
                break;
        }
    }

    private void FindPlayer()
    {
        if (Vector3.Distance(GetPosition(), player.transform.position) < viewDistance)
        {
            // Player inside viewDistance
            Vector3 dirToPlayer = (player.transform.position - GetPosition()).normalized;
            if (Vector3.Angle(GetAimDir(), dirToPlayer) < fov / 2f)
            {
                // Player inside Field of View
                Debug.Log("in field of view");
                RaycastHit2D raycastHit2D = Physics2D.Raycast(GetPosition(), dirToPlayer, viewDistance);
                if (raycastHit2D.collider != null)
                {
                    Debug.Log("Hit Something");
                    // Hit something
                    if (raycastHit2D.collider.gameObject.GetComponent<NewPlayerScript>() != null && isPlayerDead == false)
                    {
                        // Hit Player
                        Debug.Log("Hit player");
                        isHitPlayer = true;
                        anim.SetBool("isChase", true);
                        StartAttackingPlayer();
                    }
                    else
                    {
                        Debug.Log("Hit something else");
                        anim.SetBool("isChase", false);
                    }
                }
            }
        }
        else
        {
            anim.SetBool("isChase", false);
            if (WalkOrWait == false)
            {
                state = State.Waiting;
            }
            else if (WalkOrWait == true)
            {
                state = State.Walking;
            }
            isFirstBite = false;
            isHitPlayer = false;
        }
    }

    public void StartAttackingPlayer()
    {
        //Only for follow the direction of player
        state = State.ChaseTarget;
        Vector3 dirToTarget = (player.transform.position - GetPosition()).normalized;
        lastMoveDir = dirToTarget;
        if (isRight == true && player.transform.position.x < transform.position.x)
        {
            this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * -speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(-1.0f, 0f, 0f);
            isRight = false;
        }
        else if (isRight == false && player.transform.position.x > transform.position.x)
        {
            this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            position.x += (walkvalue * speed * Time.deltaTime);
            transform.position = position;
            lastMoveDir = new Vector3(1.0f, 0f, 0f);
            isRight = true;
        }
        
        //Chasing state
        float distance = (player.transform.position - transform.position).magnitude;
        /*if (distance > attackRange && isFirstBite == false)
        {
            anim.SetBool("isChase", true);
            Move();
            StopAttack();
        }
        else if (distance > attackRange && isFirstBite == true)
        {
            time -= Time.deltaTime;
            if (time <= 0)
            {
                AttackPlayer();
                time = delayTimeBTW;
            }
            else
            {
                anim.SetBool("isChase", true);
                Move();
                StopAttack();
            }
        }
        else if (attackRange >= distance && cooling == false && isFirstBite == false)
        {
            AttackPlayer();
        }
        else if (attackRange >= distance && cooling == false && isFirstBite == true)
        {
            //Animation
            anim.SetBool("isClose", true);
            anim.SetBool("isChase", false);

            time -= Time.deltaTime;

            if (time <= 0)
            {
                AttackPlayer();
                time = delayTimeBTW;
            }
        }
        */
        if(distance <= attackRange && isFirstBite == false && cooling == false)
        {
            AttackPlayer();
        }
        else if(distance <= attackRange && isFirstBite == true && cooling == false)
        {
            time -= Time.deltaTime;

            if (time <= 0)
            {
                AttackPlayer();
                time = delayTimeBTW;
            }
        }
        else if(distance > attackRange)
        {
            anim.SetBool("isClose", false);
            time -= Time.deltaTime;
            if (time <= 0)
            {
                AttackPlayer();
                time = delayTimeBTW;
            }
            else
            {
                Move();
                StopAttack();
            }
        }

        if (cooling)
        {
            anim.SetBool("isClose", false);
            CoolDown();
        }
    }

    void Move()
    {
        anim.SetBool("isChase", true);
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Bitev2"))
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), followSpeed * Time.deltaTime);
            isFollow = true;
        }
    }

    void CoolDown()
    {
        timeBTW -= Time.deltaTime;

        if (timeBTW <= 0 && cooling && attackMode)
        {
            cooling = false;
            timeBTW = startTimeBTW;
            time = delayTimeBTW;
        }
    }

    void StopAttack()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), 0 * Time.deltaTime);
        cooling = false;
        attackMode = false;
    }

    private void AttackPlayer()
    {
        anim.SetBool("isChase", false);
        anim.SetTrigger("isBite");
        anim.SetBool("isClose", true);
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.transform.position.x, transform.position.y), 0 * Time.deltaTime);
        timeBTW = startTimeBTW;
        time = delayTimeBTW;
        attackMode = true;
        isFirstBite = true;
    }

    public void TriggerCooling()
    {
        cooling = true;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(damagingPoint.position, attackRange);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall" && state != State.ChaseTarget)
        {
            if (isRight == true && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = false;
            }
            else if (isRight == false && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = true;
            }
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetAimDir()
    {
        return lastMoveDir;
    }
}
