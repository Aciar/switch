﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMonsterNew : MonoBehaviour
{
    public enum State
    {
        Walking,
        Waiting,
        ChaseTarget,
        GoingBackToStart,
    }

    [Header("State")]
    public State state;
    public float waitTimer;
    public float walkTimer;
    public float waitCountTimer;
    public float walkCountTimer;
    public bool WalkOrWait;

    [Header("Raycast")]
    //public float visionRadius;
    //public float attackRadius;
    public float speed;
    public float followSpeed;

    [Header("Enemy status")]
    GameObject player;
    Vector3 initialPoint;
    public Rigidbody2D rb;
    public bool isRight;
    public float walkvalue;
    public Vector3 target;
    private bool isBackToInitialPoint;
    private bool isFindPlayer = false;
    private bool isFollow = false;
    public bool isPlayer = true;
    float distanceFromFirst = 0.0f;

    [Header("FieldOfView")]
    [SerializeField] private Vector3 aimDirection;
    [SerializeField] private Transform pfFieldOfView;
    [SerializeField] private float fov = 90f;
    [SerializeField] private float viewDistance = 50f;
    [SerializeField] private GameObject prefabFieldOfView;
    public GameObject cloneFOV;
    private FieldOfView fieldOfView;
    private Vector3 lastMoveDir;
    public bool isHitPlayer = false;

    [Header("References")]
    //public GameObject Weapon;
    public Transform damagingPoint;
    public Transform AttackRange;
    public LayerMask whatIsEnemies;
    public float attackRange;
    //public int damage;
    public float startTimeBTW;
    public float timeBTW;
    private NewPlayerScript playerscript;
    RaycastHit2D hit;
    public Vector3 position;

    public static bool isPlayerDead = false;
    public bool isFirstBite = false;
    public bool isClosed = false;

    public Animator anim;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("player");
        playerscript = GameObject.FindGameObjectWithTag("player").GetComponent<NewPlayerScript>();
        if (player == null)
            isPlayer = false;

        initialPoint = transform.position;
        rb = GetComponent<Rigidbody2D>();

        lastMoveDir = aimDirection;
        cloneFOV = (GameObject)Instantiate(prefabFieldOfView);
        //fieldOfView = Instantiate(pfFieldOfView, null).GetComponent<FieldOfView>();
        fieldOfView = cloneFOV.GetComponent<FieldOfView>();
        fieldOfView.SetFoV(fov);
        fieldOfView.SetViewDistance(viewDistance);

        anim = GetComponent<Animator>();
        waitCountTimer = waitTimer;
        walkCountTimer = walkTimer;
    }

    private void Update()
    {
        position = transform.position;


        switch (state)
        {
            default:
            case State.Waiting:
            case State.Walking:
                HandleMovement();
                FindPlayer();
                timeBTW = startTimeBTW;
                break;


            case State.ChaseTarget:
                //AttackPlayer(isFollow);
                FindPlayer();
                break;

            case State.GoingBackToStart:
                distanceFromFirst = (transform.position.x - initialPoint.x);
                isFollow = false;
                if (distanceFromFirst > 1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                }
                else if (distanceFromFirst < -1)
                {
                    isBackToInitialPoint = true;
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                }
                else
                {
                    initialPoint = transform.position;
                    isBackToInitialPoint = false;
                    state = State.Walking;
                }
                break;
        }

        if (fieldOfView != null)
        {
            fieldOfView.SetOrigin(transform.position);
            fieldOfView.SetAimDirection(GetAimDir());
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            isClosed = !isClosed;
        }

        if (isClosed == true)
        {
            cloneFOV.SetActive(false);
        }
        else if (isClosed == false)
        {
            cloneFOV.SetActive(true);
        }
    }

    private void HandleMovement()
    {
        switch (state)
        {
            case State.Waiting:
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", false);
                waitCountTimer -= Time.deltaTime;
                anim.SetBool("isWalk", false);
                if (waitCountTimer <= 0f)
                {
                    walkCountTimer = walkTimer;
                    state = State.Walking;
                    WalkOrWait = true;          //Walk = true
                }
                break;
            case State.Walking:
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", false);
                anim.SetBool("isWalk", true);
                if (isRight == true)
                {
                    Debug.Log("is Right");
                    this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(1.0f, 0f, 0f);
                }
                else if (isRight == false)
                {
                    Debug.Log("is left");
                    this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                    position.x += (walkvalue * -speed * Time.deltaTime);
                    transform.position = position;
                    lastMoveDir = new Vector3(-1.0f, 0f, 0f);
                }

                walkCountTimer -= Time.deltaTime;
                if (walkCountTimer <= 0f && isHitPlayer == false)
                {
                    Debug.Log("In gonna waiting");
                    state = State.Waiting;
                    WalkOrWait = false;
                    waitCountTimer = waitTimer;
                }
                break;
        }
    }

    private void FindPlayer()
    {
        if (Vector3.Distance(GetPosition(), player.transform.position) < viewDistance)
        {
            // Player inside viewDistance
            Vector3 dirToPlayer = (player.transform.position - GetPosition()).normalized;
            if (Vector3.Angle(GetAimDir(), dirToPlayer) < fov / 2f)
            {
                // Player inside Field of View
                Debug.Log("in field of view");
                RaycastHit2D raycastHit2D = Physics2D.Raycast(GetPosition(), dirToPlayer, viewDistance);
                if (raycastHit2D.collider != null)
                {
                    Debug.Log("Hit Something");
                    // Hit something
                    if (raycastHit2D.collider.gameObject.GetComponent<NewPlayerScript>() != null && isPlayerDead == false)
                    {
                        // Hit Player
                        Debug.Log("Hit player");
                        isHitPlayer = true;
                        isFirstBite = false;
                        StartAttackingPlayer();
                    }
                    else
                    {
                        isHitPlayer = false;
                        Debug.Log("Hit something else");
                        // Hit something else
                    }
                }
            }
        }
        else
        {
            isHitPlayer = false;
            if (WalkOrWait == false)
            {
                state = State.Waiting;
            }
            else if (WalkOrWait == true)
            {
                state = State.Walking;
            }
            
        }
    }

    public void StartAttackingPlayer()
    {
        AttackPlayer();
    }

    private void AttackPlayer()
    {
        state = State.ChaseTarget;
        Vector3 dirToTarget = (player.transform.position - GetPosition()).normalized;
        lastMoveDir = dirToTarget;

        RaycastHit2D hit = Physics2D.Linecast(damagingPoint.position, AttackRange.position, 1 << LayerMask.NameToLayer("Player"));
        Debug.Log("in Hit player");
        if (hit.collider != null)
        {
            Debug.Log("Hit player != null");
            if (hit.collider.gameObject.CompareTag("player") && timeBTW <= 0)
            {
                Debug.Log("Hit player tag");
                transform.position = this.transform.position;
                if (isFirstBite == false)
                {
                    Debug.Log("bite player");
                    anim.SetBool("isChasing", false);
                    anim.SetBool("isClose", true);
                    anim.SetTrigger("isBite");
                    hit.collider.gameObject.GetComponent<NewPlayerScript>().InvokeDead();
                    isFirstBite = true;
                }
                else
                {
                    anim.SetBool("isClose", false);
                }
                timeBTW = startTimeBTW;
            }
            else if (hit.collider.gameObject.CompareTag("player") && timeBTW > 0)
            {
                timeBTW -= Time.deltaTime;
                anim.SetBool("isChasing", false);
                anim.SetBool("isClose", true);
            }
            else
            {
                timeBTW -= Time.deltaTime;
                anim.SetBool("isChasing", true);
                anim.SetBool("isClose", false);
            }
            Debug.DrawLine(damagingPoint.position, hit.point, Color.red);
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, followSpeed * Time.deltaTime);
            if (isRight == true && player.transform.position.x < transform.position.x)
            {
                this.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * -speed * Time.deltaTime);
                transform.position = position;
                lastMoveDir = new Vector3(-1.0f, 0f, 0f);
                isRight = false;
            }
            else if (isRight == false && player.transform.position.x > transform.position.x)
            {
                this.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                position.x += (walkvalue * speed * Time.deltaTime);
                transform.position = position;
                lastMoveDir = new Vector3(1.0f, 0f, 0f);
                isRight = true;
            }
            isFollow = true;
            anim.SetBool("isChasing", true);
            Debug.DrawLine(damagingPoint.position, AttackRange.position, Color.red);
        }


    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(damagingPoint.position, AttackRange.position);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall" && state != State.ChaseTarget)
        {
            if (isRight == true && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = false;
                //Debug.Log("Is right false");
            }
            else if (isRight == false && state != State.ChaseTarget && state != State.GoingBackToStart)
            {
                isRight = true;
                //Debug.Log("Is right true");
            }
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public Vector3 GetAimDir()
    {
        return lastMoveDir;
    }
}
