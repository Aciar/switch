﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRayCast : MonoBehaviour
{
    [SerializeField]
    GameObject Enemy;
    //Animator enemyAnimator;
    [SerializeField]
    Transform CastPoint;
    [SerializeField]
    Transform player;
    [SerializeField]
    float agroRange;
    [SerializeField]
    float moveSpeed;
    [SerializeField]
    float visionRadius;

    public Rigidbody2D rb;
    public bool isFacingLeft;
    public bool isAgro = false;
    public bool isSearching;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
       //enemyAnimator = Enemy.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(CanSeePlayer(agroRange))
        {
            isAgro = true;
            ChasePlayer();
        }
        else
        {
            if(isAgro)
            {
                if(!isSearching)
                {
                    isSearching = true;
                    Invoke("StopChasingPlayer", 3);
                }
            }
        }

        if(isAgro)
        {
            ChasePlayer();
        }
    }

    bool CanSeePlayer(float distance)
    {
        bool val = false;

        float castDist = distance;

        if(isFacingLeft)
        {
            castDist = -distance;
        }

        Vector2 endPos = CastPoint.position + Vector3.right * castDist;
        RaycastHit2D hit = Physics2D.Linecast(CastPoint.position, endPos, 1 << LayerMask.NameToLayer("Player"));
        //RaycastHit2D hit = Physics2D.Raycast(CastPoint.position, endPos, visionRadius, 1 << LayerMask.NameToLayer("Player"));

        if (hit.collider != null)
        {
            if(hit.collider.gameObject.CompareTag("Player"))
            {
                val = true;
            }
            else
            {
                val = false;
            }
            Debug.DrawLine(CastPoint.position, hit.point, Color.red);
            //Debug.DrawRay(CastPoint.position, hit.point, Color.red);
        }
        else
        {
            Debug.DrawLine(CastPoint.position, endPos, Color.red);
            //Debug.DrawRay(CastPoint.position, endPos, Color.red);
        }

        return val;
    }

    void ChasePlayer()
    {
        if(transform.position.x < player.position.x)
        {
            rb.velocity = new Vector2(moveSpeed, 0);
            transform.localScale = new Vector3(1, 1);
            isFacingLeft = false;
        }
        else
        {
            rb.velocity = new Vector2(-moveSpeed, 0);
            transform.localScale = new Vector3(-1, 1);
            isFacingLeft = true;
        }

        //enemyAnimator.Play();
    }

    void StopChasingPlayer()
    {
        isAgro = false;
        isSearching = false;
        rb.velocity = new Vector2(0, 0);
    }
}
