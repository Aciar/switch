﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))] // automatically add BoxCollider2D component 
public class Controller2D : MonoBehaviour
{
	//public LayerMask pastLayer; // layer for past object
	//public LayerMask futureLayer; // layer for future object
	public LayerMask collisionMask; // create which layer the object will collide

	/*public Collider2D pastBaseCollider; // collider object for pastBase
	public Collider2D pastDiffCollider; // collider object for pastDiff
	public Collider2D futureBaseCollider; // collider object for futureBase
	public Collider2D futureDiffCollider; // collider object for futureDiff */

	const float skinWidth = .015f;	// declare the depth of the starting point of the ray in the object collider
	public int horizontalRayCount = 4;	// total ray in horiontal axis
	public int verticalRayCount = 4; // total ray in vectical axis

	float horizontalRaySpacing; // space between each ray in horiontal axis
	float verticalRaySpacing; // space between each ray in horiontal axis

	BoxCollider2D boxCollider;	// object for BoxCollider2D
	RaycastOrigins raycastOrigins; // create object from class RayCastOrigins
	public CollisionInfo collisions; // create object from class CollisionInfo

	void Start()
	{
		boxCollider = GetComponent<BoxCollider2D>(); // get reference from BoxCollider2D
		CalculateRaySpacing(); // calculate the space between each ray

		/*collisionMask = pastLayer; // set default layer
		pastBaseCollider.isTrigger = false; // set trigger default
		pastDiffCollider.isTrigger = false; // set trigger default
		futureBaseCollider.isTrigger = true; // set trigger default
		futureDiffCollider.isTrigger = true; // set trigger default */
	}

	public void Move(Vector3 velocity) // function for movement
	{
		UpdateRaycastOrigins(); // find each edge of the collider
		collisions.Reset();

		if (velocity.x != 0) // moving in x-axis
		{
			HorizontalCollisions(ref velocity); // check collision in horizontal axis
		}
		if (velocity.y != 0) // moving in y-axis
		{
			VerticalCollisions(ref velocity); // check collision in vertical axis
		}

		transform.Translate(velocity); // move the object
	}

	/*public void UpdateLayerMask(bool timeState) { // change layer that player will collide according to the time state
		if (timeState == true) // future state
		{
			collisionMask = futureLayer; // set enable future collision and past trigger
			pastDiffCollider.isTrigger = true;
			futureDiffCollider.isTrigger = false; 

		}
		else // past state
		{
			collisionMask = pastLayer; // set enable past collision and future trigger
			pastDiffCollider.isTrigger = false;
			futureDiffCollider.isTrigger = true; 
		}
	} */
	void HorizontalCollisions(ref Vector3 velocity) // check collision in horizontal axis
	{
		float directionX = Mathf.Sign(velocity.x); // return sign of the velocity, so we know if it's going left or right
		float rayLength = Mathf.Abs(velocity.x) + skinWidth; // calculate the length of the ray

		for (int i = 0; i < horizontalRayCount; i++) // for each ray
		{
			Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight; // check if the object is going left or right, then set the raycast that will be used 
			rayOrigin += Vector2.up * (horizontalRaySpacing * i); // set the raycast
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask); // set raycast hit condition

			Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red); // draw ray

			if (hit) // check if the ray hit
			{
				velocity.x = (hit.distance - skinWidth) * directionX; // when distance equal 0, the object will stop
				rayLength = hit.distance; // Update the length the closest distance

				collisions.left = directionX == -1; // check if the collision occurs at left 
				collisions.right = directionX == 1; // check if the collision occurs at right
			}
		}
	}

	void VerticalCollisions(ref Vector3 velocity) // check collision in vertical axis
	{
		float directionY = Mathf.Sign(velocity.y); // return sign of the velocity, so we know if it's going up or down
		float rayLength = Mathf.Abs(velocity.y) + skinWidth; // calculate the length of the ray

		for (int i = 0; i < verticalRayCount; i++) // for each ray
		{
			Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft; // check if the object is going up or down, then set the raycast that will be used 
			rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x); // set the raycast and update when the object move (velocity.x)
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask); // set raycast hit condition

			Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red); // draw ray

			if (hit) // check if the ray hit
			{
				velocity.y = (hit.distance - skinWidth) * directionY; // when distance equal 0, the object will stop
				rayLength = hit.distance; // Update the length the closest distance

				collisions.below = directionY == -1; // check if the collision occurs at below
				collisions.above = directionY == 1; // check if the collision occurs at above
			}
		}
	}

	void UpdateRaycastOrigins() // find each edge of the collider
	{
		Bounds bounds = boxCollider.bounds; // create bound that has the same size as the collider
		bounds.Expand(skinWidth * -2); // decrease the size of the bound by skinWidth

		raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y); // set each edge
		raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
		raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
	}

	void CalculateRaySpacing() // calculate the space between each ray
	{
		Bounds bounds = boxCollider.bounds; // create bound that has the same size as the collider
		bounds.Expand(skinWidth * -2); // decrease the size of the bound by skinWidth

		horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue); // state the minimum & maximum value of total ray in horizontal axis
		verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue); // state the minimum & maximum value of total ray in vertical axis

		horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1); // equation to find the space between ray in horizontal axis
		verticalRaySpacing = bounds.size.x / (verticalRayCount - 1); // equation to find the space between ray in vertical axis
	}

	struct RaycastOrigins // struct for each edge of raycast
	{
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}

	public struct CollisionInfo // check where the collision is occured
	{
		public bool above, below;
		public bool left, right;

		public void Reset() // reset the value to false
		{
			above = below = false;
			left = right = false;
		}
	}
	
}