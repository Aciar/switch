﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewPlayerScript : MonoBehaviour
{
    [SerializeField] private UI_KeyHolder UI_KeyHolder;
    [SerializeField] private KeyHolder keyHolder;
    // each object decription in inspector is in tooltip
    [Header("Movement")]
    [Tooltip("player speed value")]
    public float moveSpeed;
    [Tooltip("player jump force value")]
    public float jumpForce;

    [Header("Acceleration")]
    [Tooltip("time from stop to reach max speed")]
    public float timeToMax;
    [Tooltip("time to stop from max speed")]
    public float timeToStop;

    [Header("Gravity")]
    [Tooltip("Gravity multiplier when player fall down")]
    public float fallMultiplier;
    [Tooltip("Gravity multiplier when player let go of the key while jumping")]
    public float lowJumpMultiplier;

    [Header("GroundCheck")]
    [Tooltip("Tranform component of groundCheck object")]
    public Transform groundCheck;
    [Tooltip("radius of groundCheck")]
    public float checkRadius;
    [Tooltip("Choose what layer is ground")]
    public LayerMask whatIsGround;
    public bool isGrounded; // for store value if player touch ground  
    //public bool groundTouch;

    float xInput; // input in x-axis
    [HideInInspector]
    public float velocityX; // value to store velocity in x-axis
    float velocityXSmoothing; //  value to store velocity in smoothdamp method

    bool disEffectOn = false;
    [HideInInspector]
    public bool isInvert = false;

    [Header("Booleans")]
    [Tooltip("Check if player can walk")]
    public bool canMove = true;
    [Tooltip("Check if player is gonna pushing")]
    public bool Pushing;
    [Tooltip("Check if player is gonna pulling")]
    public bool Pulling;
    [Tooltip("Check if player is gonna break after run")]
    public static bool gonnaBreak;
    private AnimationScript anim;
    public int side = 1;
    float x;
    float xCheck = 0;
    int sideCheck;
    public bool Dying;
    public bool Jumping;
    public bool Braking = false;
    public bool isDoneSetBrake = false;
    public bool isStun = false;
    public static bool isMagneticField = false;
    public static bool isCanPushPull;
    Rigidbody2D rb2D; // object for Rigidbody2D
    public GameObject ShockEffect;
    public GameObject StunEffect;
    public GameObject video;

    public static NewPlayerScript instance;
    float xSmoothing;
    private void Awake()
    {
        if (instance != null) {
            Destroy(this);
        }
        instance = this;
        Dying = false;
    }

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>(); // get reference for Rigidbody2D
        anim = GetComponentInChildren<AnimationScript>();
        
    }

    private void Update()
    {
        if (isStun == false && Dying == false)
        {
            if (Input.GetKey(InputManager.instance.left))
            {
                xInput = -1;
                x = Mathf.SmoothDamp(x, -1, ref xSmoothing, .1f);
            }
            else if (Input.GetKey(InputManager.instance.right)) {
                xInput = 1;
                x = Mathf.SmoothDamp(x, 1, ref xSmoothing, .1f);
            }
            else {
                xInput = 0;
                x = Mathf.SmoothDamp(x, 0, ref xSmoothing, .1f);
            }
            VerticalControl(); // control velocity in y-axis
        }
        else // Check if stun by jumping on guard
        {
            xCheck = 0;
            x = 0;
            rb2D.velocity.Set(0f, rb2D.velocity.y);
        }

        if (gonnaBreak == true)
        {
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Break);
        }

        /*if (Dying == true)
        {
            xCheck = 0;
            x = 0;
            rb2D.velocity.Set(0f, rb2D.velocity.y);
        }*/

        if ((x > 0.1 || x < -0.1) && (isGrounded == true && rb2D.velocity.y == 0))
        {
            if (x >= 0.99 || x <= -0.99)
            {
                SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Run, transform.position);
            }
            else
            {
                SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Walk, transform.position);
            }
        }

        if (isInvert == true)   //For animation setting
        {
            xCheck *= -1;
            x *= -1;
        }

        anim.SetHorizontalMovement(x, 0, rb2D.velocity.y);

        if (xInput > 0.001 && gonnaBreak == false && Pushing == false && Pulling == false && isCanPushPull == false && Dying == false && isInvert == false)
        {
            side = 1;
            anim.Flip(side);
        }
        if (xInput < -0.001 && gonnaBreak == false && Pushing == false && Pulling == false && isCanPushPull == false && Dying == false && isInvert == false)
        {
            side = -1;
            anim.Flip(side);
        }
    }

    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround); // check if player touch ground

        //HorizontalControl(); // control velocity in x-axis

        if (isStun == false && Dying == false)
        {
            HorizontalControl(); // control velocity in x-axis
        }
        else 
        {
            velocityX = Mathf.SmoothDamp(velocityX, 0.0f, ref velocityXSmoothing, timeToStop);
        }
        rb2D.velocity = new Vector2(velocityX, rb2D.velocity.y); // executing velocity in x-axis
  
    }

    void HorizontalControl() // control velocity in x-axis
    {
        if (isInvert)   // invert the force
        {
            xInput *= -1;
        }
        
        float targetVelocityX = xInput * moveSpeed; // set target speed


        if ((Input.GetKey(InputManager.instance.left) || Input.GetKey(InputManager.instance.right)) && Dying != true) // key is pressed
        {
            velocityX = Mathf.SmoothDamp(velocityX, targetVelocityX, ref velocityXSmoothing, timeToMax); // apply the SmoothDamp to x axis when acceleration 

            //Flip side of player when isInvert
            if (velocityX > 0.1 && isInvert)
            {
                side = 1;
            }
            else if (velocityX < -0.1 && isInvert)
            {
                side = -1;
            }
            if (isInvert || gonnaBreak)
            { 
                anim.Flip(side); // Update side to animation
            }

            if (!isInvert)
            {

                if (Mathf.Sign(xInput) != Mathf.Sign(velocityX) || (Input.GetKey(InputManager.instance.left) && Input.GetKey(InputManager.instance.right))) // key press oposite side of velocity || press both key
                {
                    velocityX = Mathf.SmoothDamp(velocityX, 0.0f, ref velocityXSmoothing, timeToStop); // apply the SmoothDamp to x axis when deceleration
                }
            }

            if(isCanPushPull == true)
            {
                if (velocityX > 0.5 && side == 1)
                {
                    Pushing = true;
                    Pulling = false;
                }
                else if (velocityX < -0.5 && side == 1)
                {
                    Pulling = true;
                    Pushing = false;
                }
                else if (velocityX > 0.5 && side == -1)
                {
                    Pulling = true;
                    Pushing = false;
                }
                else if (velocityX < -0.5 && side == -1)
                {
                    Pulling = false;
                    Pushing = true;
                }
                else
                {
                    Pulling = false;
                    Pushing = false;
                }
            }
            else
            {
                Pulling = false;
                Pushing = false;
            }
            
        }
        else if((!Input.GetKey(InputManager.instance.left) && !Input.GetKey(InputManager.instance.right)))// no key is pressed
        {
            velocityX = Mathf.SmoothDamp(velocityX, 0.0f, ref velocityXSmoothing, timeToStop); // apply the SmoothDamp to x axis when deceleration
        }
        else
        {
            velocityX = 0;
        }
    }

    void VerticalControl() // control velocity in y-axis
    {
        if (Input.GetKeyDown(InputManager.instance.jump) && isGrounded) // condition for allow jumping
        {
            Jumping = true;
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Jump);
            rb2D.velocity = Vector2.up * jumpForce; // executing velocity in y-axis
            if (Dying == false)
            {
                anim.SetTrigger("Jump");
            }
        }

        if (rb2D.velocity.y < 0) // falling down
        {
            rb2D.velocity += Vector2.up * Physics2D.gravity.y * fallMultiplier * Time.deltaTime; // calculate gravity change
        }
        else if (rb2D.velocity.y > 0 && !Input.GetKey(InputManager.instance.jump)) // going up but let go of the button
        {
            rb2D.velocity += Vector2.up * Physics2D.gravity.y * lowJumpMultiplier * Time.deltaTime; // calculate gravity change
        }

        if(rb2D.velocity.y <= 0.2  && Jumping)
        {
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Landing);
            Jumping = false;
        }
    }

    public void HitByDisaster(float xSpeed, float yGravity, float duration) 
    {
        if (!disEffectOn)
        {
            StartCoroutine(ModifyVelocity(xSpeed, yGravity, duration));
        }
    }

    public void gotShocked()
    {
        GameObject shock = Instantiate(ShockEffect, this.transform.position, Quaternion.identity);
        shock.transform.parent = this.gameObject.transform;
        Destroy(shock, 5.15f);
    }

    IEnumerator ModifyVelocity(float xSpeed, float yGravity, float duration) {
        disEffectOn = true;
        moveSpeed *= xSpeed;
        rb2D.gravityScale *= yGravity;
        yield return new WaitForSeconds(duration);
        moveSpeed /= xSpeed;
        rb2D.gravityScale /= yGravity;
        disEffectOn = false;
    }

    private void OnCollisionEnter2D(Collision2D collision) // when collider with others
    {
        if (collision.gameObject.CompareTag("spike")) // hit spike
        {
            StartCoroutine(PlayerDead()); // start Player dead function
        }
       /* if (collision.gameObject.CompareTag("spike2")) // hit spike
        {
            StartCoroutine(PlayerDead2()); // start Player dead function
        }*/
    }

    public void DeadByDis() {
        StartCoroutine(PlayerDead());
    }

    public IEnumerator PlayerDead() // when player is dead
    {
        Dying = true;
        anim.SetTrigger("Dead");
        CameraShaker.instance.CamShake();
        EnemyRaycastState.isPlayerDead = true;
        EnemyShortState.isPlayerDead = true;
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Dead);
        Debug.Log("Play animation Dead");
        yield return new WaitForSeconds(1.25f); // delay for 2 second before destroy the player
        Destroy(this.gameObject); // destroy
        Debug.Log("Destroy Player");
        Destroy(GameObject.FindWithTag("GM")); // destroy game manager to reset all level related value
        //keyHolder.OnkeysChanged -= UI_KeyHolder.keyHolder_OnKeysChanged;
        //Destroy(GameObject.FindWithTag("BGsound")); //destroy Background sound
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
        Debug.Log("Dead and restart");
    }
    /*public IEnumerator PlayerDead2() // when player is dead
    {
        anim.SetTrigger("Dead");
        Dying = true;
        EnemyRaycastState.isPlayerDead = true;
        EnemyShortState.isPlayerDead = true;
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Dead);
        //anim.SetTrigger("Dead");
        yield return new WaitForSeconds(2f); // delay for 2 second before destroy the player
        Destroy(this.gameObject); // destroy
        Destroy(GameObject.FindWithTag("GM")); // destroy game manager to reset all level related value
        Destroy(GameObject.FindWithTag("BGsound")); //destroy Background sound
        SceneManager.LoadScene("RestartScreen2"); // load restart screen
    }*/

    public void InvokeDead()
    {
        Debug.Log("Dead");
        StartCoroutine(PlayerDead());
    }

    public void Stunning()  //Stun from jumping on guard
    {
        if(isStun == false)
        {
            StartCoroutine(PlayerStun());
            
        }
    }

    public IEnumerator PlayerStun()
    {
        isStun = true;
        GameObject stun = Instantiate(StunEffect, this.transform.position, Quaternion.identity);
        stun.transform.parent = this.gameObject.transform;
        Destroy(stun, 5f);
        Debug.Log("In stunning");
        yield return new WaitForSeconds(5f);        //Stun for 5 seconds
        isStun = false;
    }

}