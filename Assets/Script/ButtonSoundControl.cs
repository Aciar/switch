﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ButtonSoundControl : MonoBehaviour
{
    static public float volume = 0.0f;
    public AudioSource audioSource;
    public AudioClip audioClip;
    //public AudioMixer audioMixer;
    public Slider sliderMaster;
    public Slider sliderMusic;
    public Slider sliderSFX;
    float volumeMaster = 0;
    float volumeMusic = 0;
    float volumeSFX = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(sliderMusic == null)
        {
            volumeMusic = 1;
        }
        if(sliderSFX == null)
        {
            volumeSFX = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        audioSource.volume = volumeMaster * volumeMusic * volumeSFX;
    }

    public void Play()
    {
        volumeMaster = sliderMaster.value;
        if (sliderMusic == null)
        {
            volumeMusic = 1;
        }
        else
        {
            volumeMusic = sliderMusic.value;
        }
        if (sliderSFX == null)
        {
            volumeSFX = 1;
        }
        else
        {
            volumeSFX = sliderSFX.value;
        }
        audioSource.volume = volumeMaster*volumeMusic * volumeSFX;
        audioSource.Stop();
        audioSource.PlayOneShot(audioClip);
    }


    /*public float GetVolume(float vol)
    {
        vol = audioSource.volume;
    }*/
   
}
