﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Sound_manager 
{
    public enum Sound
    {
        Walk,
        Run,
        Jump,
        Landing,
        Break,
        DiSwitch,
    }

    //perfect for storing timers
    private static Dictionary<Sound, float> soundTimerDictionary;
    private static GameObject oneShotGameObject;
    private static AudioSource oneShotAudioSource;

    public static void Initialize()
    {
        soundTimerDictionary = new Dictionary<Sound, float>();
        soundTimerDictionary[Sound.Walk] = 0f;
    }

    public static void PlaySound(Sound sound, Vector3 position)
    {
        if (CanPlaySound(sound))
        {
            GameObject soundGameObject = new GameObject("Sound");
            soundGameObject.transform.position = position;
            AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();

            Object.Destroy(soundGameObject, audioSource.clip.length);
        }
    }

    public static void PlaySound(Sound sound)
    {
        if (CanPlaySound(sound))
        {
            if (oneShotGameObject == null)
            {
                oneShotGameObject = new GameObject("Sound");
                oneShotAudioSource = oneShotGameObject.AddComponent<AudioSource>();
            }
            oneShotAudioSource.PlayOneShot(GetAudioClip(sound));
        }
    }
    
    private static bool CanPlaySound(Sound sound)
    {
        switch(sound)
        {
            default:
                return true;
            case Sound.Walk:
                if (soundTimerDictionary.ContainsKey(sound))
                {
                    float lastTimePlayed = soundTimerDictionary[sound];
                    float playerMoveTimerMax = 0.55f;
                    if((lastTimePlayed + playerMoveTimerMax) < Time.time)
                    {
                        soundTimerDictionary[sound] = Time.time;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
                    //break;
        }
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        foreach (SFXasset.SoundAudioClip soundAudioClip in SFXasset.i.soundAudioClipArray)
        {
            if (soundAudioClip.sound == sound)
            {
                return soundAudioClip.audioClip;
            }
        }
        Debug.LogError("Sound " + sound + "not found");
        return null;

    }


    /*
    /// <summary>
    /// Plays a sound
    /// </summary>
    /// <returns>An audiosource</returns>
    /// <param name="sfx">The sound clip you want to play.</param>
    /// <param name="location">The location of the sound.</param>
    /// <param name="loop">If set to true, the sound will loop.</param>
    public virtual AudioSource PlaySound(AudioClip sfx, Vector3 location, bool loop = false, float spartialBlend = 1f, string saveKey = null)
    {
        if (sfx == null)
        {
            return null;
        }
        if (!Settings.SfxOn)
            return null;
        // we create a temporary game object to host our audio source
        GameObject temporaryAudioHost = new GameObject("TempAudio");
        // we set the temp audio's position
        temporaryAudioHost.transform.position = location;
        // we add an audio source to that host
        AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource;
        // make the sound 3D
        //audioSource.spatialBlend = spartialBlend;
        // we set that audio source clip to the one in paramaters
        audioSource.clip = sfx;
        // we set the audio source volume to the one in parameters
        //audioSource.volume = SettingsManager.Instance.GetSfxVolume();

        //audioSource.volume = sfx.volume;
        // we set our loop setting
        audioSource.loop = loop;
        // we start playing the sound
        audioSource.Play();

        if (!loop)
        {
            // we destroy the host after the clip has played
            Destroy(temporaryAudioHost, sfx.length);
        }
        /*else
        {
            _loopingSounds.Add(audioSource);
            if (!string.IsNullOrEmpty(saveKey))
            {
                _loopingSoundsDict[saveKey] = audioSource;
            }
        }

        // we return the audiosource reference
        return audioSource;
    }*/
}
