﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSound : MonoBehaviour
{
    //private NewPlayerScript player;
    // Start is called before the first frame update

    //
    public AudioClip[] clips;
    private AudioSource[] audioSrc;
    int numArray = 0;
    double initLatency = .1d;
    float differenceTime = 0;
    public static bool isCutscene = false;
    public static bool isDimensionScene = false;
    public static bool isPast = true;
    public static bool isFuture = false;


    //private float musicVolume = 1f;
    void Start()
    {
        //player = GetComponent<NewPlayerScript>();
        audioSrc = GetComponents<AudioSource>();
        numArray = audioSrc.Length;
        for(int i = 0; i < numArray; i++)
        {
            audioSrc[i].clip = clips[i];
            //audioSrc[i].PlayOneShot(clips[i], audioSrc[i].volume);
        }
        for (int i = 0; i < numArray; i++)
        {
            //audioSrc[i].volume = VolumeManager.Instance.getMusicVolume();
            audioSrc[i].volume = SettingsController.Instance.getMusicVolume();
            //audioSrc[i].PlayOneShot(clips[i], audioSrc[i].volume);
        }
        differenceTime = clips[1].length - clips[0].length;
        //Debug.Log("Diff : "+differenceTime);
        PlayBG1();
        //PlayBG2();
    }

    private static BGSound instance = null;
    public static BGSound Instance
    {
        get { return instance; }
    }



    void Awake()
    {
        if(instance != null && instance != this) //If this object is on but not this scene   
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);

    }

    /*public void DestroyWhenDie(bool isDying)
    {
        if (isDying)
        {
            Destroy(this);
        }
    }*/

    void Update()
    {
        //to check Background sound
        /*if(Input.GetKeyDown(KeyCode.K))
        {
            isDimensionScene = !isDimensionScene;
            Debug.Log("isDimensionScene : "+ isDimensionScene);
        }
        if(isDimensionScene)
        {
            if(Input.GetKeyDown(KeyCode.H))
            {
                isPast = true;
                isFuture = false;
                Debug.Log("isPast : " + isPast);
                Debug.Log("isFuture : " + isFuture);
            }
            if(Input.GetKeyDown(KeyCode.N))
            {
                isPast = false;
                isFuture = true;
                Debug.Log("isPast : " + isPast);
                Debug.Log("isFuture : " + isFuture);
            }
        }*/

        if(isCutscene == true)
        {
            audioSrc[0].volume = 0;
            audioSrc[0].volume = 0;
        }


        if(isDimensionScene && isCutscene == false)
        {
            if(isPast == true && isFuture == false)
            {
                audioSrc[0].volume = 0;                                         //Past
                //audioSrc[1].volume = VolumeManager.Instance.getMusicVolume();   //Future
                audioSrc[1].volume = SettingsController.Instance.getMusicVolume();
            }
            else if (isPast == false && isFuture == true)
            {
                //audioSrc[0].volume = VolumeManager.Instance.getMusicVolume();   //Past
                audioSrc[0].volume = SettingsController.Instance.getMusicVolume();
                audioSrc[0].volume = 0;                                           //Future
            }
        }
        else
        {
            //audioSrc.volume = VolumeManager.Instance.getMusicVolume();
            for (int i = 0; i < numArray; i++)
            {
                //audioSrc[i].volume = VolumeManager.Instance.getMusicVolume();
                audioSrc[i].volume = SettingsController.Instance.getMusicVolume();
                //audioSrc[i].PlayOneShot(clips[i], audioSrc[i].volume);
            }
        }

    }
    
    public void PlayBG1()   //Past
    {
        audioSrc[0].PlayOneShot(clips[0], audioSrc[0].volume);
        Invoke("PlayBG1", clips[0].length +8.844894f);
        PlayBG2();
    }

    public void PlayBG2()   //Future
    {
        audioSrc[1].PlayOneShot(clips[1], audioSrc[1].volume);
        //Invoke("PlayBG2", clips[1].length);
    }
}
