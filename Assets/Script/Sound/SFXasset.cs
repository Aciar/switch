﻿using UnityEngine;

public class SFXasset : MonoBehaviour
{
    private static SFXasset _i;

    public static SFXasset i
    {
        get {
            //if (_i == null) _i = (Instantiate(Resources.Load("sfxasset")) as GameObject).GetComponent<SFXasset>();
            if (_i == null) //_i = Instantiate(Resources.Load<SFXasset>("SFXasset"));
                _i = Instantiate(Resources.Load("SFXasset") as GameObject).GetComponent<SFXasset>();
            return _i;
        }
    }

    public SoundAudioClip[] soundAudioClipArray;

    [System.Serializable]
    public class SoundAudioClip
    {
        public Sound_manager.Sound sound;
        public AudioClip audioClip;
    }
}
