﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteController : MonoBehaviour
{
    public Toggle mute;
    public GameObject soundSettings;
    public SettingsController settings;
    public static bool isMute;
    public static bool MasMute;
    public static bool MuMute;
    public static bool SFXMute;
    public static bool AllMute;

    // Start is called before the first frame update
    void Start()
    {
        soundSettings = GameObject.Find("SoundVolume");
        settings = soundSettings.GetComponent<SettingsController>();
        if (mute.name == "MasterMute")
        { 
            mute.isOn = settings.getMasterMute();
        }
        else if (mute.name == "MusicMute")
        {
            mute.isOn = settings.getMusicMute();
        }
        else if (mute.name == "SFXMute")
        {
            mute.isOn = settings.getSFXMute();
        }
        else if (mute.name == "AllMute")
        {
            mute.isOn = settings.getAllMute();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (mute.name == "MasterMute")
        {
            if (settings.getAllMute() == false)
            {
                mute.isOn = settings.getMasterMute();
            }
            else if (settings.getAllMute() == true)
            {
                mute.isOn = settings.getAllMute();
            }
            mute.onValueChanged.AddListener(MasterMuteChange);
        }
        else if (mute.name == "MusicMute")
        {
            if (settings.getAllMute() == false)
            {
                mute.isOn = settings.getMusicMute();
            }
            else if (settings.getAllMute() == true)
            {
                mute.isOn = settings.getAllMute();
            }
            mute.onValueChanged.AddListener(MusicMuteChange);
        }
        else if (mute.name == "SFXMute")
        {
            if (settings.getAllMute() == false)
            {
                mute.isOn = settings.getSFXMute();
            }
            else if (settings.getAllMute() == true)
            {
                mute.isOn = settings.getAllMute();
            }
            mute.onValueChanged.AddListener(SFXMuteChange);
        }
        else if (mute.name == "AllMute")
        {
            mute.onValueChanged.AddListener(AllMuteChange);
        }


    }

    void MasterMuteChange(bool value)
    {
        settings.setMasterMute(value);
    }

    void MusicMuteChange(bool value)
    {
        settings.setMusicMute(value);
    }

    void SFXMuteChange(bool value)
    {
        settings.setSFXMute(value);
    }

    void AllMuteChange(bool value)
    {
        settings.setAllMute(value);
        mute.isOn = settings.getAllMute();
    }
}
