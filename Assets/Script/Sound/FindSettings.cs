﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FindSettings : MonoBehaviour 
{
    public Slider slider;
    public GameObject soundSettings;
    public SettingsController settings;

    // Start is called before the first frame update
    void Start()
    {
        soundSettings = GameObject.Find("SoundVolume");
        settings = soundSettings.GetComponent<SettingsController>();
        if (slider.name == "MasterSlider")
        {
            slider.value = settings.getMaster();
        }
        else if (slider.name == "MusicSlider")
        {
            slider.value = settings.getMusic();
        }
        else if (slider.name == "SFXSlider")
        {
            slider.value = settings.getSFX();
        }
    }

    void Update()
    {
        if(slider.name == "MasterSlider")
        {
            slider.onValueChanged.AddListener(MasterChange);
            slider.value = settings.getMaster();
        }
        else if (slider.name == "MusicSlider")
        {
            slider.onValueChanged.AddListener(MusicChange);
            slider.value = settings.getMusic();
        }
        else if (slider.name == "SFXSlider")
        {
            slider.onValueChanged.AddListener(SFXChange);
            slider.value = settings.getSFX();
        }
    }

    void MasterChange(float sliderValue)
    {
        settings.setMasterVolume(sliderValue);
    }

    void MusicChange(float sliderValue)
    {
        settings.setMusicVolume(sliderValue);
    }

    void SFXChange(float sliderValue)
    {
        settings.setSFXVolume(sliderValue);
        //SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.TestSFX);
    }


    public void PlaySFX()
    {
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.TestSFX);
    }
}
