﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGSound2 : MonoBehaviour
{
    public AudioClip clips;
    private AudioSource audioSrc;

    void Start()
    {
        //player = GetComponent<NewPlayerScript>();
        audioSrc = GetComponent<AudioSource>();
        audioSrc.clip = clips;
        audioSrc.volume = SettingsController.Instance.getMusicVolume();

    }

    private static BGSound2 instance = null;
    public static BGSound2 Instance
    {
        get { return instance; }
    }



    void Awake()
    {
        if (instance != null && instance != this) //If this object is on but not this scene   
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);

    }


    void Update()
    {
        //audioSrc.volume = VolumeManager.Instance.getMusicVolume();
        audioSrc.volume = SettingsController.Instance.getMusicVolume();
        //Debug.Log("audioSrc.BG.volume : "+ audioSrc.volume);
    }
}
