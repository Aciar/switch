﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VolumeManager : MonoBehaviour
{
    // Start is called before the first frame update

    public static VolumeManager Instance { get; set; }

    public float masterVol = 1f;
    public float musicVol = 1f;
    public float sfxVol = 1f;

    public  Slider _MasterSlider;
    public Slider MasterSlider
    {
        get
        {
            if(_MasterSlider == null)
            {
                _MasterSlider = GameObject.FindGameObjectWithTag("MasterSlider").GetComponent<Slider>();
                Debug.Log("Finding master");
            }
            return _MasterSlider;
        }
    }
    public  Slider _MusicSlider;
    public Slider MusicSlider
    {
        get
        {
            if (_MusicSlider == null)
            {
                _MusicSlider = GameObject.FindGameObjectWithTag("MusicSlider").GetComponent<Slider>();
                Debug.Log("Finding music");
            }
            return _MusicSlider;
        }
    }
    public  Slider _SFXSlider;
    public Slider SFXSlider
    {
        get
        {
            if (_SFXSlider == null)
            {
                _SFXSlider = GameObject.FindGameObjectWithTag("SFXSlider").GetComponent<Slider>();
                Debug.Log("Finding sfx");
            }
            return _SFXSlider;
        }
    }

    bool isSceneLoaded = false;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        Debug.Log("Start finding slider");
        _MasterSlider = GameObject.FindGameObjectWithTag("MasterSlider").GetComponent<Slider>();
        _MusicSlider = GameObject.FindGameObjectWithTag("MusicSlider").GetComponent<Slider>();
        _SFXSlider = GameObject.FindGameObjectWithTag("SFXSlider").GetComponent<Slider>();
        Debug.Log("Done start");
    }

    public void FindSlider()
    {
        Debug.Log("Finding slider");
        _MasterSlider = GameObject.FindGameObjectWithTag("MasterSlider").GetComponent<Slider>();
        _MusicSlider = GameObject.FindGameObjectWithTag("MusicSlider").GetComponent<Slider>();
        _SFXSlider = GameObject.FindGameObjectWithTag("SFXSlider").GetComponent<Slider>();
        Debug.Log("Done finding");
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        isSceneLoaded = true;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void Update()
    {
        Debug.Log(MasterSlider);
        Debug.Log(MusicSlider);
        Debug.Log(SFXSlider);
        UpdateSlider();
        if (isSceneLoaded)
        {
            Debug.Log("ChangeScene+FindSlider");
            FindSlider();
            isSceneLoaded = false;
        }
    }
    void UpdateSlider()
    {
        float valueMaster = PlayerPrefs.GetFloat("MasterVolume");
        float valueMusic = PlayerPrefs.GetFloat("MusicVolume");
        float valueSFX = PlayerPrefs.GetFloat("SFXVolume");

        MasterSlider.value = valueMaster;
        MusicSlider.value = valueMusic;
        SFXSlider.value = valueSFX;
    }

    public float getSFXVolume()
    {
        return masterVol * sfxVol;
    }

    public float getMusicVolume()
    {
        return masterVol * musicVol;
    }

    public void setMasterVolume(float value)
    {
        masterVol = value;
        PlayerPrefs.SetFloat("MasterVolume", value);
        PlayerPrefs.Save();
    }

    public void setMusicVolume(float value)
    {
        musicVol = value;
        PlayerPrefs.SetFloat("MusicVolume", value);
        PlayerPrefs.Save();
    }

    public void setSFXVolume(float value)
    {
        sfxVol = value;
        PlayerPrefs.SetFloat("SFXVolume", value);
        PlayerPrefs.Save();
    }
}
