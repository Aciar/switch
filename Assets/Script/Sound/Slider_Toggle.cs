﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slider_Toggle : MonoBehaviour
{
    public GameObject parent;
    public Toggle mute;
    public MuteController muteControl;
    public Slider slider;
    public FindSettings sliderControl;
    public GameObject soundSettings;
    public SettingsController settings;
    // Start is called before the first frame update
    void Start()
    {
        soundSettings = GameObject.Find("SoundVolume");
        settings = soundSettings.GetComponent<SettingsController>();
        if (parent.name == "Master")
        {
            mute = GameObject.Find("MasterMute").GetComponent<Toggle>();
            muteControl = mute.GetComponent<MuteController>();
            slider = GameObject.Find("MasterSlider").GetComponent<Slider>();
            sliderControl = slider.GetComponent<FindSettings>();
        }
        else if (parent.name == "Music")
        {
            mute = GameObject.Find("MusicMute").GetComponent<Toggle>();
            muteControl = mute.GetComponent<MuteController>();
            slider = GameObject.Find("MusicSlider").GetComponent<Slider>();
            sliderControl = slider.GetComponent<FindSettings>();
        }
        else if (parent.name == "SFX")
        {
            mute = GameObject.Find("SFXMute").GetComponent<Toggle>();
            muteControl = mute.GetComponent<MuteController>();
            slider = GameObject.Find("SFXSlider").GetComponent<Slider>();
            sliderControl = slider.GetComponent<FindSettings>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (parent.name == "Master")
        {
            mute.onValueChanged.AddListener(MasterMuteChange);
            slider.onValueChanged.AddListener(MasterChange);
        }
        else if (parent.name == "Music")
        {
            mute.onValueChanged.AddListener(MusicMuteChange);
            slider.onValueChanged.AddListener(MusicChange);
        }
        else if (parent.name == "SFX")
        {
            mute.onValueChanged.AddListener(SFXMuteChange);
            slider.onValueChanged.AddListener(SFXChange);
        }

        setValue();
    }

    void MasterMuteChange(bool value)
    {
        mute.isOn = value;
        settings.setMasterMute(value);
    }

    void MusicMuteChange(bool value)
    {
        mute.isOn = value;
        settings.setMusicMute(value);
    }

    void SFXMuteChange(bool value)
    {
        mute.isOn = value;
        settings.setSFXMute(value);
    }

    void AllMuteChange(bool value)
    {
        mute.isOn = value;
        settings.setAllMute(value);
    }

    void MasterChange(float sliderValue)
    {
        settings.setMasterVolume(sliderValue);
    }

    void MusicChange(float sliderValue)
    {
        settings.setMusicVolume(sliderValue);
    }

    void SFXChange(float sliderValue)
    {
        settings.setSFXVolume(sliderValue);
    }

    public void setValue()
    {
        if (slider.name == "MasterSlider")
        {
            slider.value = settings.getMaster();
        }
        else if (slider.name == "MusicSlider")
        {
            slider.value = settings.getMusic();
        }
        else if (slider.name == "SFXSlider")
        {
            slider.value = settings.getSFX();
        }
    }
}
