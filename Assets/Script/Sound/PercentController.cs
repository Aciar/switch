﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PercentController : MonoBehaviour
{
    public Text PercentText;
    public GameObject soundSettings;
    public SettingsController settings;

    // Start is called before the first frame update
    void Start()
    {
        soundSettings = GameObject.Find("SoundVolume");
        settings = soundSettings.GetComponent<SettingsController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PercentText.name == "MasterPer")
        {
            PercentText.text = SettingsController.getMasterPer().ToString() + "%";
        }
        else if (PercentText.name == "MusicPer")
        {
            PercentText.text = SettingsController.getMusicPer().ToString() + "%";
        }
        else if (PercentText.name == "SFXPer")
        {
            PercentText.text = SettingsController.getSFXPer().ToString() + "%";
        }
    }
}
