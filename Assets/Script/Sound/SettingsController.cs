﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    public static SettingsController _instance;

    public static float masterVol = 0.5f;
    public static float musicVol = 0.5f;
    public static float sfxVol = 1f;
    public static float MaxMaster = 0.5f;
    public static float MaxMusic = 0.5f;
    public static float MaxSFX = 1f;
    public static float unmuteMaster;
    public static float unmuteMusic;
    public static float unmuteSFX;
    public static bool MasterMute = false;
    public static bool MusicMute = false;
    public static bool SFXMute = false;
    public static bool AllMute = false;
    public static float MasterValue = 1.0f;
    public static float MusicValue = 1.0f;
    public static float SFXValue = 1.0f;
    public static float AllValue = 1.0f;

    public static SettingsController Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new SettingsController();
            }

            return _instance;
        }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
        if(PlayerPrefs.GetInt("savedFirstRun") == 1)
        {
            SetAll();
        }
        unmuteMaster = masterVol;
        unmuteMusic = musicVol;
        unmuteSFX = sfxVol;
    }

    void Update()
    {
        if (AllMute == true)
        {
            AllValue = 0.0f;
        }
        else if (AllMute == false)
        {
            AllValue = 1.0f;
        }

        if (MasterMute == true)
        {
            MasterValue = 0.0f;
        }
        else if(MasterMute == false)
        {
            MasterValue = 1.0f;
        }

        if (MusicMute == true)
        {
            MusicValue = 0.0f;
        }
        else if (MusicMute == false)
        {
            MusicValue = 1.0f;
        }

        if (SFXMute == true)
        {
            SFXValue = 0.0f;
        }
        else if (SFXMute == false)
        {
            SFXValue = 1.0f;
        }
    }

    public float getSFXVolume()
    {
        return masterVol * sfxVol * SFXValue * AllValue;
    }

    public float getMusicVolume()
    {
        return masterVol * musicVol * MusicValue * AllValue;
    }

    public float getMaster()
    {
        return masterVol * MasterValue * AllValue;
    }

    public float getMusic()
    {
        return  musicVol * MusicValue * AllValue;
    }

    public float getSFX()
    {
        return sfxVol * SFXValue * AllValue;
    }

    public static int getMasterPer()
    {
        return (int)(((masterVol * MasterValue * AllValue) /MaxMaster)*100);
    }

    public static int getMusicPer()
    {
        return (int)(((musicVol * MusicValue * AllValue) / MaxMusic) * 100);
    }

    public static int getSFXPer()
    {
        return (int)(((sfxVol * SFXValue * AllValue) / MaxSFX) * 100);
    }

    public void setMasterVolume(float value)
    {
        masterVol = value;
        if (value >= 0.001)
        {
            AllMute = false;
            MasterMute = false;
        }
        else if (value == 0.000)
        {
            MasterMute = true;
        }
    }

    public void setMusicVolume(float value)
    {
        musicVol = value;
        if (value >= 0.001)
        {
            AllMute = false;
            MusicMute = false;
        }
        else if(value == 0.000)
        {
            MusicMute = true;
        }
    }

    public void setSFXVolume(float value)
    {
        sfxVol = value;
        if(value >= 0.001)
        {
            AllMute = false;
            SFXMute = false;
        }
        else if (value == 0.000)
        {
            SFXMute = true;
        }
    }

    public void muteMaster(bool value)
    {
        if (value == true && AllMute == false)
        {
            unmuteMaster = masterVol;
        }
        else if(value == false)
        {
            AllMute = false;
            masterVol = unmuteMaster;
        }
        MasterMute = value;
    }

    public void muteMusic(bool value)
    {
        if (value == true && AllMute == false)
        {
            unmuteMusic = musicVol;
        }
        else if (value == false)
        {
            AllMute = false;
            musicVol = unmuteMusic;
        }
        MusicMute = value;
    }

    public void muteSFX(bool value)
    {
        if (value == true && AllMute == false)
        {
            unmuteSFX = sfxVol;
        }
        else if (value == false)
        {
            AllMute = false;
            sfxVol = unmuteSFX;
        }
        SFXMute = value;
    }

    public void muteAll(bool value)
    {
        muteMaster(value);
        muteSFX(value);
        muteMusic(value);
    }

    public bool getMasterMute()
    {
        return MasterMute;
    }

    public bool getMusicMute()
    {
        return MusicMute;
    }

    public bool getSFXMute()
    {
        return SFXMute;
    }

    public bool getAllMute()
    {
        return AllMute;
    }

    public void setMasterMute(bool value)
    {
        MasterMute = value;
        muteMaster(value);
    }

    public void setMusicMute(bool value)
    {
        MusicMute = value;
        muteMusic(value);
    }

    public void setSFXMute(bool value)
    {
        SFXMute = value;
        muteSFX(value);
    }

    public void setAllMute(bool value)
    {
        muteAll(value);
        AllMute = value;
    }

    public void SetAll()
    {
        masterVol = PlayerPrefs.GetFloat("MasterVol");
        musicVol = PlayerPrefs.GetFloat("MusicVol");
        sfxVol = PlayerPrefs.GetFloat("SfxVol");
        if (PlayerPrefs.GetFloat("MasterMuteV") == 1.0f)
        {
            MasterMute = true;
        }
        else if (PlayerPrefs.GetFloat("MasterMuteV") == 0.0f)
        {
            MasterMute = false;
        }
        if (PlayerPrefs.GetFloat("MusicMuteV") == 1.0f)
        {
            MusicMute = true;
        }
        else if (PlayerPrefs.GetFloat("MusicMuteV") == 0.0f)
        {
            MusicMute = false;
        }
        if (PlayerPrefs.GetFloat("SFXMuteV") == 1.0f)
        {
            SFXMute = true;
        }
        else if (PlayerPrefs.GetFloat("SFXMuteV") == 0.0f)
        {
            SFXMute = false;
        }
        if (PlayerPrefs.GetFloat("AllMuteV") == 1.0f)
        {
            AllMute = true;
        }
        else if (PlayerPrefs.GetFloat("AllMuteV") == 0.0f)
        {
            AllMute = false;
        }
    }

    public static void GetAll()
    {
        if (MasterMute == false)
        {
            PlayerPrefs.SetFloat("MasterVol", masterVol);
            PlayerPrefs.SetFloat("MusicVol", musicVol);
            PlayerPrefs.SetFloat("SfxVol", sfxVol);
        }
        else if(MasterMute == true)
        {
            PlayerPrefs.SetFloat("MasterVol", unmuteMaster);
            PlayerPrefs.SetFloat("MusicVol", unmuteMusic);
            PlayerPrefs.SetFloat("SfxVol", unmuteSFX);
        }
        if(MasterMute == true)
        {
            PlayerPrefs.SetFloat("MasterMuteV", 1.0f);
        }
        else if(MasterMute == false)
        {
            PlayerPrefs.SetFloat("MasterMuteV", 0.0f);
        }
        if (MusicMute == true)
        {
            PlayerPrefs.SetFloat("MusicMuteV", 1.0f);
        }
        else if (MusicMute == false)
        {
            PlayerPrefs.SetFloat("MusicMuteV", 0.0f);
        }
        if (SFXMute == true)
        {
            PlayerPrefs.SetFloat("SFXMuteV", 1.0f);
        }
        else if (SFXMute == false)
        {
            PlayerPrefs.SetFloat("SFXMuteV", 0.0f);
        }
        if (AllMute == true)
        {
            PlayerPrefs.SetFloat("AllMuteV", 1.0f);
        }
        else if (AllMute == false)
        {
            PlayerPrefs.SetFloat("AllMuteV", 0.0f);
        }
        PlayerPrefs.Save();
    }
}
