﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetActiveBG : MonoBehaviour
{
    public GameObject BG;
    public bool setActive = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(BG == null)
        {
            BG = GameObject.FindGameObjectWithTag("BGsound");
        }

        /*if (Input.GetKeyDown("space"))
        {
            if (setActive == true)
            {
                setActive = false;
            }
            else if (setActive == false)
            {
                setActive = true;
            }
        }*/

        if(setActive == true)
        {
            Debug.Log("BG true");
            BG.SetActive(true);
        }
        else if(setActive == false)
        {
            Debug.Log("BG false");
            BG.SetActive(false);
        }
    }

    public void SetBool()
    {
        if (setActive == true)
        {
            setActive = false;
        }
        else if (setActive == false)
        {
            setActive = true;
        }
    }
}
