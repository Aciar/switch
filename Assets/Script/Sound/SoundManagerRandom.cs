﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerRandom : MonoBehaviour
{
    public enum Sound
    {
        Walk,
        Run,
        Jump,
        Landing,
        Break,
        DiSwitch,
        Dead,
        PickUpCard,
        OpenDoor,
        MenuButton,
        Fire,
        Gravity,
        AxisDisortion,
        TemperatureDrop,
        StructureFailure,
        Neutron,
        Hurricane,
        TestSFX,
    }

    //perfect for storing timers
    private static Dictionary<Sound, float> soundTimerDictionary;
    private static GameObject oneShotGameObject;
    private static AudioSource oneShotAudioSource;

    public static void Initialize()
    {
        soundTimerDictionary = new Dictionary<Sound, float>();
        soundTimerDictionary[Sound.Walk] = 0f;
        soundTimerDictionary[Sound.Run] = 0f;
    }

    public static void PlaySound(Sound sound, Vector3 position)
    {
        if (CanPlaySound(sound))
        {
            GameObject soundGameObject = new GameObject("Sound");
            soundGameObject.transform.position = position;
            AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
            audioSource.clip = GetAudioClip(sound);
            //audioSource.volume = SFXAssetArray.i.getSFXVolume();
            //audioSource.volume = VolumeManager.Instance.getSFXVolume();
            audioSource.volume = SettingsController.Instance.getSFXVolume();
            audioSource.Play();
            Object.Destroy(soundGameObject, audioSource.clip.length);
        }
    }

    public static void PlaySound(Sound sound)
    {
        if (CanPlaySound(sound))
        {
            if (oneShotGameObject == null)
            {
                oneShotGameObject = new GameObject("Sound");
                oneShotAudioSource = oneShotGameObject.AddComponent<AudioSource>();
            }
            //oneShotAudioSource.volume = VolumeManager.Instance.getSFXVolume();
            oneShotAudioSource.volume = SettingsController.Instance.getSFXVolume();
            if (sound == Sound.PickUpCard)
            {
                oneShotAudioSource.volume += 0.2f;
            }
            if (sound == Sound.StructureFailure)
            {
                oneShotAudioSource.volume *= 0.2f;
            }
            oneShotAudioSource.PlayOneShot(GetAudioClip(sound));
        }
    }

    private static bool CanPlaySound(Sound sound)
    {
        switch (sound)
        {
            default:
                return true;
            case Sound.Walk:
                if (soundTimerDictionary.ContainsKey(sound))
                {
                    float lastTimePlayed = soundTimerDictionary[sound];
                    float playerMoveTimerMax = 0.25f;
                    if ((lastTimePlayed + playerMoveTimerMax) < Time.time)
                    {
                        soundTimerDictionary[sound] = Time.time;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            //break;
            case Sound.Run:
                if (soundTimerDictionary.ContainsKey(sound))
                {
                    float lastTimePlayed = soundTimerDictionary[sound];
                    float playerMoveTimerMax = 0.25f;
                    if ((lastTimePlayed + playerMoveTimerMax) < Time.time)
                    {
                        soundTimerDictionary[sound] = Time.time;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
                //break;
        }
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        foreach (SFXAssetArray.SoundAudioClip soundAudioClipArray in SFXAssetArray.i.soundAudioClipArray)
        {
            if (soundAudioClipArray.sound == sound)
            {
                return soundAudioClipArray.GetRandomClip(soundAudioClipArray.audioClipArray);
            }
        }
        Debug.LogError("Sound " + sound + "not found");
        return null;

    }
}
