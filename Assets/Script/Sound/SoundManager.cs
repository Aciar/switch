﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    //public Slider[] volumnSliders;
    public AudioMixer mixer;


    public void SetMasterVolumn(float value)
    {
        //AudioManager.instance.SetVolume(value, AudioManager.Audiochannel.Master);
        mixer.SetFloat("MasterVolume", Mathf.Log10(value) * 20);
        ButtonSoundControl.volume = Mathf.Log10(value) * 20;
    }

    public void SetMusicVolumn(float value)
    {
        mixer.SetFloat("MusicVolume", Mathf.Log10(value) * 20);
    }

    public void SetSFXVolumn(float value)
    {
        mixer.SetFloat("SFXmaster", Mathf.Log10(value) * 20);
        //mixer.SetFloat("SFXvolume", Mathf.Log10(value) * 20);
    }

    public void SetSFX(float value)
    {
        mixer.SetFloat("SFXvolume", Mathf.Log10(value) * 20);
    }

    public void SetSFXVolumn1(float value)
    {
        mixer.SetFloat("Sound1volume", Mathf.Log10(value) * 20);
    }

    public void SetSFXVolumn2(float value)
    {
        mixer.SetFloat("Sound2volume", Mathf.Log10(value) * 20);
    }

    /*public float GetVolume(float vol)
    {
        vol = mixer.volume;
    }*/
}
