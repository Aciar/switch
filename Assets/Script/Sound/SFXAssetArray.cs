﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXAssetArray : MonoBehaviour
{
    private static SFXAssetArray _i;

    public static SFXAssetArray i
    {
        get
        {
            //if (_i == null) _i = (Instantiate(Resources.Load("sfxasset")) as GameObject).GetComponent<SFXasset>();
            if (_i == null) //_i = Instantiate(Resources.Load<SFXasset>("SFXasset"));
                _i = Instantiate(Resources.Load("SFXAssetArray") as GameObject).GetComponent<SFXAssetArray>();
            return _i;
        }
    }
    //public SoundVolume soundVol;

    //[System.Serializable]
    /*public class SoundVolume
    {
        public float masterVol = 1f;
        public float musicVol = 1f;
        public float sfxVol = 1f;


        public float getSFXVolume()
        {
            return masterVol * sfxVol;
        }

        public float getMusicVolume()
        {
            return masterVol * musicVol;
        }

    }*/
    /*public float masterVol = 1f;
    public float musicVol = 1f;
    public float sfxVol = 1f;

    public float getSFXVolume()
    {
        return masterVol * sfxVol;
    }

    public float getMusicVolume()
    {
        return masterVol * musicVol;
    }

    public void setMasterVolume(float value)
    {
        //Debug.Log("value : " + value);
        masterVol = value;
        //Debug.Log("masterVol : " + masterVol);
    }

    public void setMusicVolume(float value)
    {
        musicVol = value;
    }

    public void setSFXVolume(float value)
    {
        sfxVol = value;
    }*/


    public SoundAudioClip[] soundAudioClipArray;
    
    [System.Serializable]
    public class SoundAudioClip
    {
        public SoundManagerRandom.Sound sound;
        public AudioClip[] audioClipArray;
        public AudioClip GetRandomClip(AudioClip[] audioArray)
        {
            return audioArray[Random.Range(0, audioArray.Length)];
        }
    }

}
