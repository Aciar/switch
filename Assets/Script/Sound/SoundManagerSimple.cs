﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerSimple : MonoBehaviour
{
    public static AudioClip walkSound, jumpSound, breakSound;
    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        //walkSound = Resources.Load<AudioClip>("Walk");
        jumpSound = Resources.Load<AudioClip>("Jump");
        breakSound = Resources.Load<AudioClip>("Break");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            /*case "Walk":
                audioSrc.PlayOneShot(walkSound);
                break;*/
            case "Jump":
                audioSrc.PlayOneShot(jumpSound);
                break;
            case "Break":
                audioSrc.PlayOneShot(breakSound);
                break;

        }
    }
}
