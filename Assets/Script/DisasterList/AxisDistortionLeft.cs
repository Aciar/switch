﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisDistortionLeft : MonoBehaviour
{
    public static AxisDistortionLeft instance; // singleton

    public Transform cameraObj;
    public float shiftAngle = -10f;
    public float shiftTime = 1f;
    public float timer = 6f;

    public float forceMag = 150f;
    public float forceAng = 180f;

    Vector3 originAngle;
    Vector3 desiredAngle;
    Vector3 smoothShifting;
    
    float calAngle;
    float angleShift;

    bool startShiftLeft = false;
    bool returning = false;

    public bool isActive = false;

    public GameObject axisDistort;
    GameObject cam;
    GameObject distort;

    private void Awake()
    {
        if (instance != null) {
            Destroy(this);
        }
        instance = this;

        originAngle = new Vector3(0, 0, 0);
        desiredAngle = new Vector3(0, 0, shiftAngle);
    }

    private void Start()
    {
        cameraObj = GameObject.FindWithTag("MainCamera").GetComponent<Transform>();
        cam = GameObject.FindWithTag("MainCamera");
    }

    void Update()
    {
        if (isActive == true) 
        {
            isActive = false;
            PlayerEffector.instance.StartEffect(forceMag, forceAng, timer);
            ShiftingLeft();
        }
    }

    private void FixedUpdate()
    {
        if (startShiftLeft)
        {
            calAngle = Mathf.SmoothDamp(calAngle, shiftAngle, ref angleShift, shiftTime); // apply the SmoothDamp to x axis when acceleration
            //print(calAngle);
            cameraObj.transform.eulerAngles = new Vector3(0, 0, calAngle);
            
        }

        if (returning) 
        {
            calAngle = Mathf.SmoothDamp(calAngle, 0, ref angleShift, shiftTime); // apply the SmoothDamp to x axis when acceleration
            //print(calAngle);
            cameraObj.transform.eulerAngles = new Vector3(0, 0, calAngle);
            
        }
    }

    void ShiftingLeft() {
        distort = Instantiate(axisDistort) as GameObject;
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.AxisDisortion);
        distort.transform.parent = cam.transform;
        distort.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, distort.transform.position.z);
        startShiftLeft = true;
        //print("ShiftStart");
        StartCoroutine(ReturnOrigin());
    }

    IEnumerator ReturnOrigin() {
        yield return new WaitForSeconds(timer);
        startShiftLeft = false;
        //print("ShiftEnd");
        yield return new WaitForEndOfFrame();
        returning = true;
        Destroy(distort);
        //print("returnStart");
        yield return new WaitForSeconds(shiftTime);
        yield return new WaitForSeconds(5);
        returning = false;
        //print("returnEnd");
    }
}
