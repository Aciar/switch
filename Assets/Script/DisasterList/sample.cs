﻿using UnityEngine;

public class sample : MonoBehaviour
{
    public Transform cameraObj;
    public float shiftAngle = 30f;
    public float shiftSpeed = 0.125f;

    Vector3 currentAngle;
    Vector3 originAngle;
    Vector3 desiredAngle;
    Vector3 smoothShifting;

    bool isOccur;

    void Start()
    {
        originAngle = new Vector3(0, 0, 0);
        desiredAngle = new Vector3(0, 0, shiftAngle);

        currentAngle = originAngle;

    }

    void FixedUpdate()
    {
        ShiftingCheck();
    }

    void ShiftingCheck() {
        if (Input.GetKey(KeyCode.D))
        {
            FieldShiftingRight();
        }
        else if (Input.GetKey(KeyCode.A))
        {
            FieldShiftingLeft();
        }
        else {
            ReturnOrigin();
        }
    }

    void FieldShiftingRight() 
    {
        smoothShifting = Vector3.Lerp(currentAngle, desiredAngle, shiftSpeed);
        currentAngle = smoothShifting;
        cameraObj.transform.eulerAngles = smoothShifting;
    }

    void FieldShiftingLeft()
    {
        smoothShifting = Vector3.Lerp(currentAngle, -desiredAngle, shiftSpeed);
        currentAngle = smoothShifting;
        cameraObj.transform.eulerAngles = smoothShifting;
    }

    void ReturnOrigin() 
    {
        smoothShifting = Vector3.Lerp(currentAngle, originAngle, shiftSpeed);
        currentAngle = smoothShifting;
        cameraObj.transform.eulerAngles = smoothShifting;
    }
}
