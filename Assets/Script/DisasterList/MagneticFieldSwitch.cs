﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticFieldSwitch : MonoBehaviour
{
    public static MagneticFieldSwitch instance;

    [HideInInspector]
    public NewPlayerScript player;
    private AnimationScript anim;
    public float duration = 5f;

    public bool isActive = false;

    public GameObject dizzy;
    GameObject point;
    GameObject diz;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }

        instance = this;
    }

    void Start()
    {
        player = GameObject.Find("PlayerNew").GetComponent<NewPlayerScript>();
        anim = GetComponentInChildren<AnimationScript>();
        point = GameObject.FindWithTag("player");
    }

    private void Update()
    {
        if (isActive)
        {
            isActive = false;
            //player.side *= -1;
            StartCoroutine(SwitchAxis());
        }
    }

    IEnumerator SwitchAxis() 
    {
        print("Start");
        diz = Instantiate(dizzy) as GameObject;
        diz.transform.parent = point.transform;
        diz.transform.position = new Vector3(point.transform.position.x, point.transform.position.y, diz.transform.position.z);
        NewPlayerScript.isMagneticField = true;
        //player.moveSpeed *= -1; // invert
        player.side *= -1;
        player.isInvert = true;
        yield return new WaitForSeconds(duration);
        NewPlayerScript.isMagneticField = false;
        //player.moveSpeed *= -1; // revert
        player.side *= -1;
        player.isInvert = false;
        Destroy(diz);
        print("end");
    }

    private void OnDisable()
    {
        if (player.isInvert) {
            NewPlayerScript.isMagneticField = false;
            player.side *= -1;
            player.isInvert = false;
            Destroy(diz);
            print("reset magnetic");
        }
    }
}
