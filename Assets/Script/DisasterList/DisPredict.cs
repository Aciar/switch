﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class DisPredict : MonoBehaviour
{
    // UI timer
    public Text countdownText;
    public bool IsTimeTrigger = false;
    public float period = 0.0f;
    float currentTime;

    // UI slot
    public Image slotOn;
    public Image[] slot = new Image[4];
    public Sprite leftArrow;
    public Sprite rightArrow;
    
    // Event(platform)
    public float platformSpeed = 0.0f;
    int randomRange = 2;
    int[] predictSlot = new int[4];
    int onGoing = -1;

    void Start()
    {
        InitialRandom();
        currentTime = period;  
    }

    
    void FixedUpdate()
    {
        Countdown();
        CheckTrigger();
        SetSlot();
        movePlatform();
    }

    void Countdown() {
        currentTime -= 1 * Time.deltaTime;
        countdownText.text = currentTime.ToString("F2");
        if (currentTime <= 0)
        {
            currentTime = period;
            Debug.Log("Trigger!");
            IsTimeTrigger = true;
        }
    }

    void CheckTrigger()
    {
        if (IsTimeTrigger == true)
        {
            IsTimeTrigger = false;
            onGoing = predictSlot[0];
            Debug.Log(onGoing);
            for (int i = 0; i < 3; i++)
            {
                predictSlot[i] = predictSlot[i+1];
            }
            predictSlot[3] = Random.Range(0, randomRange);   
        }
    }

    void SetSlot()
    {
        if (onGoing == 0)
        {
            slotOn.GetComponent<Image>().sprite = leftArrow;
        }
        else if (onGoing == 1) {
            slotOn.GetComponent<Image>().sprite = rightArrow;
        }

        for (int i = 0; i < 4; i++) {
            if (predictSlot[i] == 0)
            {
                slot[i].GetComponent<Image>().sprite = leftArrow;
            }
            else if (predictSlot[i] == 1)
            {
                slot[i].GetComponent<Image>().sprite = rightArrow;
            }
        }
    }

    void movePlatform() {
        if (onGoing == 0) {
            transform.position += new Vector3(-platformSpeed * Time.deltaTime, 0, 0); // left
        } else if (onGoing == 1) {
            transform.position += new Vector3(platformSpeed * Time.deltaTime, 0, 0); // right
        } 
    }

    void InitialRandom()
    {
        for (int i = 0; i < 4; i++)
        {
            predictSlot[i] = Random.Range(0, randomRange);
        }
    }
}
