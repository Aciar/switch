﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hurricane : MonoBehaviour
{
    public static Hurricane instance; // singleton

    [Header("Instantiated object")]
    [Tooltip("object that the script going to instantiate")]
    public GameObject windObj;
    //public GameObject thunderEffect;

    [Header("Delay Timer")]
    [Tooltip("number of fire object per disaster")]
    public int windCount = 3;

    Vector2 screenBoundsMax; // Max bound of screen 
    Vector2 screenBoundsMin; // Min bound of screen

    public bool isActive = false;
    public LayerMask CollisionMask;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        instance = this;
    }

    private void Update()
    {
        if (isActive == true)
        {
            isActive = false;
            SpawnWind();
        }
    }

    void SpawnWind()
    {
        screenBoundsMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z)); // update Max by transfer pixel to world position screen bound in case if camera moved
        screenBoundsMin = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)); // update Min by transfer pixel to world position screen bound in case if camera moved
        for (int i = 0; i < windCount; i++)
        {
            bool isPointCollide = true;
            float randXPos = 0;
            float randYPos = 0;
            while (isPointCollide)
            {
                randXPos = Random.Range(screenBoundsMin.x, screenBoundsMax.x);
                randYPos = Random.Range(screenBoundsMin.y, screenBoundsMax.y);
                Vector3 checkSpawnPos = new Vector3(randXPos, randYPos, transform.position.z);
                if (Physics2D.CircleCast(checkSpawnPos, 1, transform.up, 0, CollisionMask) == false)
                {
                    isPointCollide = false;
                }
            }
            GameObject fire = Instantiate(windObj) as GameObject; // instantiate thunder object
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Hurricane);
            fire.transform.position = new Vector2(randXPos, randYPos); // spawn position inside screen bounds
        }
    }
}
