﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructureFailure : MonoBehaviour
{
    public static StructureFailure instance; // singleton

    [Header("Instantiated object")]
    [Tooltip("object that the script going to instantiate")]
    public GameObject structureObj;

    [Header("Delay Timer")]
    [Tooltip("number of fire object per disaster")]
    public int structureCount = 6;
    [Tooltip("minimum delay between instantiation")]
    public float RespawnTimeMin = 1f;
    [Tooltip("maximum delay between instantiation")]
    public float RespawnTimeMax = 1f;

    public float spawnHeight = 6f;

    Vector2 screenBoundsMax; // Max bound of screen 
    Vector2 screenBoundsMin; // Min bound of screen

    public bool isActive = false;

    public GameObject structureFailure;
    GameObject cam;
    GameObject structure;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        instance = this;
    }

    private void Start()
    {
        cam = GameObject.FindWithTag("MainCamera");
    }

    private void Update()
    {
        if (isActive == true)
        {
            isActive = false;
            StartCoroutine(StructureFail());
        }
    }

    IEnumerator StructureFail()
    {
        structure = Instantiate(structureFailure) as GameObject;
        structure.transform.parent = cam.transform;
        structure.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, structure.transform.position.z);
        for (int i = 0; i < structureCount; i++)
        {
            yield return new WaitForSeconds(Random.Range(RespawnTimeMin, RespawnTimeMax));
            SpawnFire();
        }
        Destroy(structure);
    }

    void SpawnFire()
    {
        screenBoundsMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z)); // update Max by transfer pixel to world position screen bound in case if camera moved
        screenBoundsMin = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)); // update Min by transfer pixel to world position screen bound in case if camera moved
        GameObject structure = Instantiate(structureObj) as GameObject; // instantiate thunder object
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.StructureFailure);
        structure.transform.position = new Vector2(Random.Range(screenBoundsMin.x, screenBoundsMax.x), spawnHeight); // spawn position inside screen bounds
    }
}