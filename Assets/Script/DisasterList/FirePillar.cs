﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePillar : MonoBehaviour
{
    public static FirePillar instance; // singleton

    [Header("Instantiated object")]
    [Tooltip("object that the script going to instantiate")]
    public GameObject fireObj;
    //public GameObject thunderEffect;

    [Header("Delay Timer")]
    [Tooltip("number of fire object per disaster")]
    public int fireCount;
    [Tooltip("minimum delay between instantiation")]
    public float RespawnTimeMin = 1f;
    [Tooltip("maximum delay between instantiation")]
    public float RespawnTimeMax = 1f;

    Vector2 screenBoundsMax; // Max bound of screen 
    Vector2 screenBoundsMin; // Min bound of screen
    public LayerMask CollisionMask;

    public bool isActive = false;

    private void Awake()
    {
        if (instance != null) {
            Destroy(this);
        }
        instance = this;
    }

    private void Update()
    {
        if (isActive == true) {
            isActive = false;
            StartCoroutine(StartFire());
        }
    }

    IEnumerator StartFire() 
    {
        for (int i = 0; i < fireCount; i++) 
        {
            yield return new WaitForSeconds(Random.Range(RespawnTimeMin, RespawnTimeMax));
            SpawnFire();
        }
    }

    void SpawnFire() 
    {
        screenBoundsMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z)); // update Max by transfer pixel to world position screen bound in case if camera moved
        screenBoundsMin = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)); // update Min by transfer pixel to world position screen bound in case if camera moved
        bool isPointCollide = true;
        float randXPos = 0;
        float randYPos = 0;
        int n = 0;
        while (isPointCollide)
        {
            Debug.Log(n);
            n++;
            randXPos = Random.Range(screenBoundsMin.x, screenBoundsMax.x);
            randYPos = Random.Range(screenBoundsMin.y, screenBoundsMax.y);
            Vector3 checkSpawnPos = new Vector3(randXPos, randYPos, transform.position.z);
            if (Physics2D.CircleCast(checkSpawnPos, 1, transform.up, 0, CollisionMask) == false)
            {
                isPointCollide = false;
            }
        }
        GameObject fire = Instantiate(fireObj) as GameObject; // instantiate thunder object
        //SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Fire);
        fire.transform.position = new Vector2(randXPos, randYPos); // spawn position inside screen bounds
    }
}
