﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEffector : MonoBehaviour
{
    GameObject player;
    Transform effectObj;
    AreaEffector2D effector;
    float timer;

    public static PlayerEffector instance;

    public bool isActive = false;
    public bool isFollow = false;

    private void Awake()
    {
        if (instance != null) {
            Destroy(this);
        }
        instance = this;
    }

    void Start()
    {
        effectObj = this.GetComponent<Transform>();
        effector = this.GetComponent<AreaEffector2D>();
        player = GameObject.FindWithTag("player");

        effector.forceMagnitude = 0;
        effector.forceAngle = 0;
    }

    
    void Update()
    {
        if (isActive == true) {
            isActive = false;
            StartCoroutine(TimeUp());
        }

        if (isFollow) {
            effectObj.position = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        }
    }

    public void StartEffect(float magnitude, float angle, float time) {
        isActive = true;
        isFollow = true;
        effector.forceMagnitude = magnitude;
        effector.forceAngle = angle;
        timer = time;
    }

    IEnumerator TimeUp() {
        yield return new WaitForSeconds(timer);
        effector.forceMagnitude = 0;
        effector.forceAngle = 0;
        timer = 0;
        isFollow = false;
    }

}
