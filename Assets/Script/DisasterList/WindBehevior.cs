﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindBehevior : MonoBehaviour
{
    [Header("WindValue")]
    [Tooltip("the radius of effect area")]
    public float effectRadius = .3f;
    [Tooltip("delay before the thunder strike")]
    public float pullForce = 100f;
    [Tooltip("duration of the effect")]
    public float activeTime = 6f;

    public Transform spawnPoint; // position for spawning raycast or effect
    public LayerMask collisionMask; // collision check 

    PointEffector2D pointEffect;

    bool isAtGround = false; // check if the raycast hit ground

    private void Start()
    {
        spawnPoint = this.GetComponent<Transform>(); // get the position of this object 
        pointEffect = this.GetComponent<PointEffector2D>();
        MoveToGround();
    }

    private void Update()
    {
        if (isAtGround) {
            pointEffect.forceMagnitude = -pullForce;
            Destroy(this.gameObject, activeTime);
        }
    }

    void MoveToGround() // function for moving object to ground
    {

        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, -spawnPoint.up, float.PositiveInfinity, collisionMask); // cast ray downward and state condition for hit
        if (hitInfo) // if hit
        {
            isAtGround = true;  // set the object at ground is true
            transform.position = hitInfo.point; // move object to the hit position
            spawnPoint = this.GetComponent<Transform>(); // Update spawn point;
        }
        else // if not hit
        {
            Destroy(this.gameObject); // destroy the object 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isAtGround && collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Destroy(this.gameObject);
        }
    }
}
