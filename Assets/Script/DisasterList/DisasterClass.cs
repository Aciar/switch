﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DisasterClass 
{
    public string name;
    public Sprite Icon;
    public bool isEnabled;
}
