﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderBehevior : MonoBehaviour
{
    [Header("ThunderValue")]
    [Tooltip("the radius of the sphereCast")]
    public float sphereRadius = .3f;
    [Tooltip("delay before the thunder strike")]
    public float warningDelay = 2f;
    [Tooltip("duration of thunder that will hit player")]
    public float activeTime = 5f;
    [Tooltip("speed value that will affect player when hit")]
    public float speedModifier = 0.75f;
    [Tooltip("duration of effect")]
    public float effectTime = 3f;

    public Transform spawnPoint; // position for spawning raycast or effect
    public LayerMask collisionMask; // collision check 
    public LayerMask playerMask; // collision check for player
    public GameObject trailObj; // [temp] object for thunder trail
    public GameObject lightningObj; // [temp] object for thunder object
    public GameObject lightningEff;

    GameObject trail; // object for controlling instantiated trail
    //GameObject lightning; // object for controlling instantiated thunder
    GameObject thunderEf;

    bool isAtGround = false; // check if the raycast hit ground
    bool isThunderActive = false; // check if thunder is on
    bool isAlreadyHit = false; // for not take the value twice

    IEnumerator Start()
    {
        spawnPoint = this.GetComponent<Transform>(); // get the position of this object 

        yield return new WaitForSeconds(1f); // delay for the function to check ontrigger

        MoveToGround(); // function for moving object to ground

        yield return new WaitForSeconds(warningDelay);

        thunderEf = Instantiate(lightningEff) as GameObject;
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Neutron);
        thunderEf.transform.localScale *= 2;
        thunderEf.transform.position = new Vector2(transform.position.x, transform.position.y + 6);
        print(lightningEff.transform.localScale.y);
        //Destroy(trail); // destory  trail
        isThunderActive = true; // set active thunder
        //lightning = Instantiate(lightningObj) as GameObject;
        //thunderEf = Instantiate(lightningEff) as GameObject;
        //lightning.transform.position = new Vector2(transform.position.x, transform.position.y + (lightningObj.transform.localScale.y * 2.8f));
        //thunderEf.transform.position = new Vector2(transform.position.x, transform.position.y + (lightningEff.transform.localScale.y * 2.8f));

        yield return new WaitForSeconds(activeTime);

        isThunderActive = false; // set unactive thunder
        //Destroy(lightning); // [temp] destroy lightning
        //Debug.Log(thunderEf.name);
        Destroy(thunderEf);
        //.Log(trail.name);
        Destroy(trail);
        Destroy(this.gameObject);// destroy this gameObject
    }

    void MoveToGround() // function for moving object to ground
    {
        
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, -spawnPoint.up, float.PositiveInfinity, collisionMask); // cast ray downward and state condition for hit
        if (hitInfo) // if hit
        {
            isAtGround = true;  // set the object at ground is true
            transform.position = hitInfo.point; // move object to the hit position
            spawnPoint = this.GetComponent<Transform>(); // Update spawn point;

            trail = Instantiate(trailObj) as GameObject; // [temp] instant the trail
            trail.transform.position = new Vector2(transform.position.x, transform.position.y + (trailObj.transform.localScale.y * 2.8f)); // set trail position
        }
        else // if not hit
        {
            Destroy(this.gameObject); // destroy the object 
        }
    }

    private void Update()
    {
        if (isThunderActive)
        {
            Vector3 raySpawn = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z); // position to spawn ray
            RaycastHit2D playerHitInfo = Physics2D.CircleCast(raySpawn, sphereRadius, transform.up, float.PositiveInfinity, playerMask); // declare and cast ray

            if (playerHitInfo && !isAlreadyHit) // if hit player
            {
                isAlreadyHit = true;
                NewPlayerScript player = playerHitInfo.transform.GetComponent<NewPlayerScript>();
                if (player != null)
                {
                    player.velocityX /= 2;
                    player.gotShocked();        //Shock Effect
                    player.HitByDisaster(speedModifier, 1, effectTime);
                }
            }
        }
    }

    private void OnDrawGizmos() // show to check in development
    {
        if (isThunderActive) {
            Vector3 raySpawn = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z); // position to spawn ray
            RaycastHit2D playerHitInfo = Physics2D.CircleCast(raySpawn, sphereRadius, transform.up, float.PositiveInfinity, playerMask); // declare and cast ray
            
            if (playerHitInfo) // if hit player
            {
                Gizmos.color = Color.red;
                Gizmos.DrawRay(transform.position, transform.up * playerHitInfo.distance);
                Gizmos.DrawWireSphere(transform.position + transform.up * playerHitInfo.distance, sphereRadius);
            }
            else 
            {
                Gizmos.color = Color.green;
                Gizmos.DrawRay(transform.position, transform.up * float.PositiveInfinity);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isAtGround && collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Destroy(this.gameObject);
        }
    }
}
