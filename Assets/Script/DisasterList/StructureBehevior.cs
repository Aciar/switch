﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructureBehevior : MonoBehaviour
{
    Rigidbody2D rb2D;
    void Start()
    {
        rb2D = this.GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("player")) {
            MagneticFieldSwitch.instance.isActive = true;
        }
        Destroy(this.gameObject);
    }
}
