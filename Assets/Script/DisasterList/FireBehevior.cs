﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBehevior : MonoBehaviour
{
    [Header("ThunderValue")]
    [Tooltip("the radius of the sphereCast")]
    public float sphereRadius = .3f;
    [Tooltip("delay before the thunder strike")]
    public float warningDelay = 2f;
    [Tooltip("duration of thunder that will hit player")]
    public float activeTime = 0.3f;

    public GameObject fireTrail; // trail prefab
    public GameObject fireObj;

    GameObject trail;
    

    public Transform spawnPoint; // position for spawning raycast or effect
    public LayerMask collisionMask; // collision check 
    public LayerMask playerMask; // collision check for player

    bool isAtGround = false; // check if the raycast hit ground
    bool isActive = false; 

    IEnumerator Start()
    {
        spawnPoint = this.GetComponent<Transform>(); // get the position of this object 

        yield return new WaitForEndOfFrame(); // delay for the function to check ontrigger
        MoveToGround(); // function for moving object to ground

        yield return new WaitForSeconds(warningDelay);

        Destroy(trail);
        GameObject fire = Instantiate(fireObj) as GameObject;
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Fire);
        fire.transform.position = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        yield return new WaitForSeconds(0.2f);
        Vector3 raySpawn = new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z); // position to spawn ray
        RaycastHit2D playerHitInfo = Physics2D.CircleCast(raySpawn, sphereRadius, transform.up, sphereRadius + 1, playerMask); // declare and cast ray

        if (playerHitInfo) // if hit player
        {
            NewPlayerScript player = playerHitInfo.transform.GetComponent<NewPlayerScript>();
            if (player != null)
            {
                player.DeadByDis();
            }
        }
        isActive = false;
        yield return new WaitForSeconds(activeTime);
        yield return new WaitForSeconds(1);
        Destroy(fire);
        Destroy(this.gameObject);// destroy this gameObject
    }

    private void OnDrawGizmos() // show to check in development
    {
        if (isActive)
        {
            Vector3 raySpawn = new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z); // position to spawn ray
            RaycastHit2D playerHitInfo = Physics2D.CircleCast(raySpawn, sphereRadius, transform.up, sphereRadius + 1, playerMask); // declare and cast ray

            if (playerHitInfo) // if hit player
            {
                Gizmos.color = Color.red;
                Gizmos.DrawRay(transform.position, transform.up * playerHitInfo.distance);
                Gizmos.DrawWireSphere(transform.position + transform.up * playerHitInfo.distance, sphereRadius);
            }
            else
            {
                Gizmos.color = Color.green;
                Gizmos.DrawRay(transform.position, transform.up * sphereRadius);
                Gizmos.DrawWireSphere(transform.position + transform.up * sphereRadius, sphereRadius);
            }
        }
    }

    void MoveToGround() // function for moving object to ground
    {

        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, -spawnPoint.up, float.PositiveInfinity, collisionMask); // cast ray downward and state condition for hit
        if (hitInfo) // if hit
        {
            isAtGround = true;  // set the object at ground is true
            transform.position = hitInfo.point; // move object to the hit position
            spawnPoint = this.GetComponent<Transform>(); // Update spawn point;

            trail = Instantiate(fireTrail) as GameObject; // [temp] instant the trail
            trail.transform.position = new Vector2(transform.position.x, transform.position.y); // set trail position
            isActive = true;
        }
        else // if not hit
        {
            Destroy(this.gameObject); // destroy the object 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isAtGround && collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Destroy(this.gameObject);
        }
    }
}
