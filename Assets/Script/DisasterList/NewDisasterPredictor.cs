﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewDisasterPredictor : MonoBehaviour
{
    // dis predict control
    public float disPeriod = 10f;
    List<int> disEvent = new List<int>();
    float currentTime;

    //dis List
    [Header("DisasterList")]
    public List<DisasterClass> disList = new List<DisasterClass>();
    List<int> disUse = new List<int>();

    // dis UI
    public Image[] slot = new Image[5];
    public Slider barMeter;

    int slotNum = 6;

    private void Awake()
    {
        currentTime = disPeriod;
        foreach (DisasterClass dis in disList) {
            if (dis.isEnabled) {
                disUse.Add(disList.IndexOf(dis)+1);
            }
        }
    }

    void Start()
    {
        FillSlot();
        SetSlot();
    }

    private void FixedUpdate()
    {
        CountDown();
    }

    void CountDown()
    {
        currentTime -= 1 * Time.deltaTime;
        float progress = currentTime / disPeriod;
        barMeter.value = progress;
        if (currentTime <= 0)
        {
            currentTime = disPeriod;
            UpdateSlot();
            DisExecute();
            SetSlot();
        }
    }

    void FillSlot() {
        disEvent.Add(0);
        for (int i = 0; i < slotNum-1; i++) {
            disEvent.Add(disUse[Random.Range(0,disUse.Count-1)]);
            if (i >= 2)
            {
                while (disEvent[i] == disEvent[i - 1] && disEvent[i] == disEvent[i - 2]) // rng bullshit prevention
                { 
                    disEvent[i] = disUse[Random.Range(0, disUse.Count - 1)]; // random agian
                }
            }
        }
    }

    void UpdateSlot() {
        disEvent.Remove(disEvent[0]);
        disEvent.Add(disUse[Random.Range(0, disUse.Count - 1)]);
        while (disEvent[slotNum - 1] == disEvent[slotNum - 2] && disEvent[slotNum - 1] == disEvent[slotNum - 3]) // rng bullshit prevention
        {
            disEvent[slotNum - 1] = disUse[Random.Range(0, disUse.Count - 1)]; // random agian
        }
    }

    void DisExecute() {
        switch (disEvent[0])
        {
            case 1:
                NeutronStorm.instance.isActive = true;
                break;
            case 2:
                FirePillar.instance.isActive = true;
                break;
            case 3:
                AxisDistortionRight.instance.isActive = true;
                break;
            case 4:
                AxisDistortionLeft.instance.isActive = true;
                break;
            case 5:
                StructureFailure.instance.isActive = true;
                break;
            case 6:
                TemperatureDrop.instance.isActive = true;
                break;
            case 7:
                Hurricane.instance.isActive = true;
                break;
            case 8:
                ExtremeGravitationPull.instance.isActive = true;
                break;
        }
    }

    void SetSlot()
    {
        for (int i = 0; i < slotNum-1; i++)
        {
            switch (disEvent[i])
            {
                case 1:
                    slot[i].sprite = disList[0].Icon;
                    break;
                case 2:
                    slot[i].sprite = disList[1].Icon; ;
                    break;
                case 3:
                    slot[i].sprite = disList[2].Icon; ;
                    break;
                case 4:
                    slot[i].sprite = disList[3].Icon; ;
                    break;
                case 5:
                    slot[i].sprite = disList[4].Icon; ;
                    break;
                case 6:
                    slot[i].sprite = disList[5].Icon; ;
                    break;
                case 7:
                    slot[i].sprite = disList[6].Icon; ;
                    break;
                case 8:
                    slot[i].sprite = disList[7].Icon; ;
                    break;
            }
        }
    }
}
