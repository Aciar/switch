﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtremeGravitationPull : MonoBehaviour
{
    public static ExtremeGravitationPull instance;

    [HideInInspector]
    public NewPlayerScript player;
    public float gravityModValue;
    public float duration = 10f;

    public bool isActive = false;
    public GameObject gravityPull;
    GameObject point;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }

        instance = this;
    }

    void Start()
    {
        player = GameObject.Find("PlayerNew").GetComponent<NewPlayerScript>();
        point = GameObject.Find("PlayerNew");
    }

    void Update()
    {
        if (isActive) 
        {
            isActive = false;
            GameObject pull = Instantiate(gravityPull) as GameObject;
            SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Gravity);
            pull.transform.parent = point.transform;
            pull.transform.position = point.transform.position;
            player.HitByDisaster(1, gravityModValue, duration);
            Destroy(pull, 6f);
        }
    }
}
