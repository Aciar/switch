﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeutronStorm : MonoBehaviour
{
    public static NeutronStorm instance;

    [Header("Instantiated object")]
    [Tooltip("object that the script going to instantiate")]
    public GameObject thunderCall;
    //public GameObject thunderEffect;

    [Header("Delay Timer")]
    [Tooltip("minimum delay between instantiation")]
    public float RespawnTimeMin = 1f; 
    [Tooltip("maximum delay between instantiation")]
    public float RespawnTimeMax = 1f;

    [Header("Thunder Count")]
    [Tooltip("minimum number of an instantiation in one period")]
    public int thunderCountMin = 1; 
    [Tooltip("maximum number of an instantiation in one period")]
    public int thunderCountMax = 1;

    public float round = 10;
    public bool isActive = false;
    public LayerMask CollisionMask;


    int thunderCount; // number of thunder calls
    Vector2 screenBoundsMax; // Max bound of screen 
    Vector2 screenBoundsMin; // Min bound of screen

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }

        instance = this;
    }

    private void Update()
    {
        if (isActive) {
            isActive = false;
            StartCoroutine(ThunderCal()); // start function
        }
    }



    IEnumerator ThunderCal() { // calculate thunder related value
        for (int i = 0; i < round; i++)
        {
            yield return new WaitForSeconds(Random.Range(RespawnTimeMin, RespawnTimeMax)); // random and wait for respawn time
            thunderCount = Random.Range(thunderCountMin, thunderCountMax); // random the number of thunder
            SpawnThunder(); // call function SpawnThunder
        }
            
    }

    void SpawnThunder() { //set spawn 
        screenBoundsMax = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z)); // update Max by transfer pixel to world position screen bound in case if camera moved
        screenBoundsMin = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)); // update Min by transfer pixel to world position screen bound in case if camera moved
        bool isPointCollide = true;
        float randXPos = 0;
        float randYPos = 0;
        while (isPointCollide)
        {
            randXPos = Random.Range(screenBoundsMin.x, screenBoundsMax.x);
            randYPos = Random.Range(screenBoundsMin.y, screenBoundsMax.y);
            Vector3 checkSpawnPos = new Vector3(randXPos, randYPos, transform.position.z);
            if (Physics2D.CircleCast(checkSpawnPos, 1, transform.up, 0, CollisionMask) == false)
            {
                isPointCollide = false;
            }
        }
        GameObject thund = Instantiate(thunderCall) as GameObject; // instantiate thunder object
        //SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.Neutron);
        //GameObject thunderEf = Instantiate(thunderEffect) as GameObject;
        thund.transform.position = new Vector2(randXPos, randYPos); // spawn position inside screen bounds
            //thunderEf.transform.position = new Vector2(Random.Range(screenBoundsMin.x, screenBoundsMax.x), Random.Range(screenBoundsMin.y, screenBoundsMax.y));
        
    }
}
