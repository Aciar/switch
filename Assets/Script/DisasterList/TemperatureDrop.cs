﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureDrop : MonoBehaviour
{
    public float addTimeToMax = .5f;
    public float addTimeToStop = .5f;
    public float duration = 6f;
    public bool isActive = false;
    float originMax, originStop;

    public static TemperatureDrop instance;
    public GameObject tempDrop;
    GameObject cam;

    private void Awake()
    {
        if (instance != null) {
            Destroy(this);
        }
        instance = this;
    }

    private void Start()
    {
        originMax = NewPlayerScript.instance.timeToMax;
        originStop = NewPlayerScript.instance.timeToStop;
        cam = GameObject.Find("Main Camera");
    }

    private void Update()
    {
        if (isActive) {
            isActive = false;
            StartCoroutine(Freeze());
        }
    }

    IEnumerator Freeze() {
        GameObject snow = Instantiate(tempDrop) as GameObject;
        SoundManagerRandom.PlaySound(SoundManagerRandom.Sound.TemperatureDrop);
        snow.transform.parent = cam.transform;
        snow.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, snow.transform.position.z);
        NewPlayerScript.instance.timeToMax += addTimeToMax;
        NewPlayerScript.instance.timeToStop += addTimeToStop;
        yield return new WaitForSeconds(duration);
        Destroy(snow);
        NewPlayerScript.instance.timeToMax -= addTimeToMax;
        NewPlayerScript.instance.timeToStop -= addTimeToStop;
    }

    private void OnDisable()
    {
        NewPlayerScript.instance.timeToMax = originMax;
        NewPlayerScript.instance.timeToStop = originStop;
        print("Temp reset");
    }
}
