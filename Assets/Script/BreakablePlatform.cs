﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakablePlatform : MonoBehaviour
{
    public float timeToBreak = 1f;
    public float timeToRespawn = 2f;

    public float shakeSpeed = 7f; 
    public float intensity = .15f;

    bool isBreaking = false;
    bool isBreak = false;
    bool inProcess = false;

    float elapsed = 0f;
    //int breakingCount = 0;
    int breakCount = 0;

    Vector2 startingPos;
    Collider2D platformCollider;
    Renderer platformRenderer;

    void Awake()
    {
        platformCollider = GetComponent<Collider2D>();
        platformRenderer = GetComponent<Renderer>();

        startingPos.x = transform.position.x;
        startingPos.y = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (isBreaking)
        {
            transform.localPosition = intensity * new Vector3(
            Mathf.PerlinNoise(shakeSpeed * Time.time, 1),
            Mathf.PerlinNoise(shakeSpeed * Time.time, 2),
            Mathf.PerlinNoise(shakeSpeed * Time.time, 3)
            );
        }

        elapsed += Time.deltaTime; // update every second
        if (elapsed >= 1f)
        {
            elapsed = elapsed % 1f;

            if (isBreak)
            {
                breakCount++;
                if (breakCount > timeToRespawn)
                {
                    breakCount = 0;
                    isBreak = false;
                    Respawn();
                }
            }

            /*if (isBreaking)
            {
                breakingCount++;
                if (breakingCount > timeToBreak)
                {
                    isBreaking = false;
                    StartCoroutine(Break());
                }
            }
            else if (isBreak)
            {
                breakCount++;
                if (breakCount > timeToRespawn)
                {
                    isBreak = false;
                    Respawn();
                }
            }
            else if (!inProcess && (breakingCount != 0 || breakCount != 0)){
                breakingCount = breakCount = 0;
            }*/
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!inProcess)
        {
            inProcess = true;
            StartCoroutine(Breaking());
        }
    }

    IEnumerator Breaking() {
        isBreaking = true;
        yield return new WaitForSeconds(timeToBreak);
        isBreaking = false;
        StartCoroutine(Break());
    }

    IEnumerator Break() {
        isBreak = true;
        platformCollider.enabled = false;
        platformRenderer.enabled = false;
        isBreaking = false;
        yield return new WaitForSeconds(timeToRespawn);
        breakCount = 0;
        isBreak = false;
        Respawn();
    }

    void Respawn() {
        platformCollider.enabled = true;
        platformRenderer.enabled = true;
        inProcess = false;
    }

    private void OnDisable()
    {
        if (inProcess) {
            isBreak = true;
            platformCollider.enabled = false;
            platformRenderer.enabled = false;
            isBreaking = false;
        }
    }
}
