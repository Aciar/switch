﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlatformScript : MonoBehaviour
{
    [Header("CollisionChecking")]
    [Tooltip("collider of the checking object.")]
    public Transform collideCheck;
    [Tooltip("layer that the collision will detect (player).")]
    public LayerMask player;

    [Header("RotatePositioning")]
    public Transform tilemapControl;
    [Tooltip("all of each angle that the platform will rotate to")]
    public List<float> rotateAngle = new List<float>();

    bool isBackAtOrigin = true; // check if the tile is in it's origin rotation
    bool isPlayerColiide = false; // store value if the player hit the check collider
    bool playerOverlap = false; // if player stay in overlap
    bool rotating = false; // check if rotate
    int sizeOfAngleList; // number of angle in a list

    bool overlapState = false; // hotfix 
    

    private void Start()
    {
        tilemapControl = GetComponent<Transform>(); // get reference of the rotate tile
        sizeOfAngleList = rotateAngle.Count; // find size of the list
    }

    void Update()
    {
        PlayerOverlapChecking();

        if (overlapState && !rotating) { // if according to condition, start rotating
            rotating = true;
            RotateTile();
            Debug.Log("Rotate: TRUE");
        }
    }

    void RotateTile()
    {
        if (rotating && isBackAtOrigin)
        {
            StartCoroutine(RotateForth());
        }
        else if(rotating && !isBackAtOrigin)
        {
            StartCoroutine(RotateForth());
        }
    }

    void PlayerOverlapChecking() // check if player overlapping the collider or out
    {
        isPlayerColiide = Physics2D.OverlapBox(collideCheck.position, collideCheck.localScale, 0, player); // check if player touch the collider

        if (isPlayerColiide && !playerOverlap)
        {
            playerOverlap = true;
            overlapState = true;
            Debug.Log("Overlap: TRUE");
        }
        else if (!isPlayerColiide && playerOverlap)
        {
            playerOverlap = false;
            Debug.Log("Overlap: FALSE");
        }
    }

    IEnumerator RotateForth()
    {
        for (int i = 0; i < sizeOfAngleList - 1; i++)
        {
            float rotateValueY = rotateAngle[i + 1] - rotateAngle[i]; // find the next rotate value
            tilemapControl.Rotate(0, rotateValueY, 0); // rotate
            yield return new WaitForFixedUpdate();
            if (i == sizeOfAngleList - 2)
            {
                isBackAtOrigin = false; // set rotate position
                rotating = false; // stop rotate
                overlapState = false;
                Debug.Log("Oigin: False");
                Debug.Log("Rotate: False");
            }
        }
    }

    IEnumerator RotateBack()
    {
        for (int i = sizeOfAngleList; i > 0; i--)
        {
            float rotateValueY = rotateAngle[i - 1] - rotateAngle[i]; // find the next rotate value
            tilemapControl.Rotate(0, rotateValueY, 0); // rotate
            yield return new WaitForFixedUpdate();
            if (i == 1)
            {
                isBackAtOrigin = true; // set rotate position
                rotating = false; // stop rotate
                overlapState = false;
                Debug.Log("Origin: True");
                Debug.Log("Rotate: False");
            }
        }
    }
}
