﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause_Play : MonoBehaviour
{
    public GameObject soundButton;

    // Start is called before the first frame update
    void Start()
    {
        /*if(AudioListener.pause == true)
        {
            
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SoundControl()
    {
        if(AudioListener.pause == true)
        {
            AudioListener.pause = false;
        }
        else
        {
            AudioListener.pause = true;
        }
        //ButtonSoundControl.Stop();
    }
}
