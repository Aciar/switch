﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;

    public KeyCode left { get; set; }
    public KeyCode right { get; set; }
    public KeyCode jump { get; set; }
    public KeyCode diSwitch { get; set; }
    public KeyCode attachDev { get; set; }
    public KeyCode pushBox { get; set; }


    private void Awake()
    {
        if (GameObject.FindGameObjectsWithTag("Input").Length > 1) {
            Destroy(this.gameObject);
        }

        if (instance != null) {
            Destroy(this);
        }
        instance = this;

        DontDestroyOnLoad(this.gameObject);

        left = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("leftKey", "LeftArrow"));
        right = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("rightKey", "RightArrow"));
        jump = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("jumpKey", "Z"));
        diSwitch = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("diSwitchKey", "C"));
        attachDev = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("attachDevKey", "X"));
        pushBox = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("pushBoxKey", "Space"));

    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
