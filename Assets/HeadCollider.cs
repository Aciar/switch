﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadCollider : MonoBehaviour
{
    public int gotPlayer = 0;
    // Start is called before the first frame update
    void Start()
    {
        gotPlayer = 0;
        EnemyRaycastState.playerGotStun = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            gotPlayer++;
            Debug.Log("Got player on head");
            
            if (gotPlayer <= 1)
            {
                other.GetComponent<NewPlayerScript>().Stunning();
                EnemyRaycastState.playerGotStun = false;
            }
            else
            {
                EnemyRaycastState.playerGotStun = true;
            }
        }
    }
}
